import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity } from 'react-native';
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import NavigationService from "./services/NavigationService";
import { AuthContainer, UnAuthContainer } from "./navigation";
import { NETWORK_INTERFACE, NETWORK_INTERFACE_LINK } from './constants/config';
import { store, persistor } from './redux/store';
import OneSignal from 'react-native-onesignal';
import NetInfo from "@react-native-community/netinfo";
import { image } from './constants/image';
import { colors, dimensions } from './theme';
import * as StoreReview from 'react-native-store-review';
import { DarkModeProvider } from 'react-native-dark-mode'
import { ViewCuston } from './components/CustonView';
import { CustomText } from './components/CustomText';




// Subscribe
const unsubscribe = NetInfo.addEventListener(state => {
  console.log("Connection type", state.type);
  console.log("Is connected?", state.isConnected);
  
    });
// Unsubscribe
unsubscribe();

const httpLink = createHttpLink({
  uri: NETWORK_INTERFACE,
});

const authLink = setContext(async (_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = await AsyncStorage.getItem('token');
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? token : "",
    }
  }
});

const apolloClient = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
})

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "fetching",
      device: '',
      Isconected: false

    };
    OneSignal.init("d7778157-666e-4d2b-a840-e9f8d1423251", { kOSSettingsKeyAutoPrompt: true });
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }

  

  componentWillUnmount() {

    if (StoreReview.isAvailable) {
      StoreReview.requestReview();
    }
    
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds = async (device) => {
    const id = await AsyncStorage.getItem('id')
    const Userid = device && device.userId 

    if (Userid && id) {
      fetch(`${NETWORK_INTERFACE_LINK}/save-userid-notification?UserId=${Userid}&id=${id}`)
      .catch(err => console.log(err))
    } else {
      return null
    }
    
  }

  
  async componentDidMount() {

    try {
      const value = await AsyncStorage.getItem('token')
      console.log(value)
      if (value !== null) {
        this.setState({ token: value })
        global.token = value
      }
      else {
        this.setState({ token: false })
      }
    } catch (e) {
      // error reading value
      this.setState({ token: false })
    }
    this.onIds()

    NetInfo.fetch().then(state => {
      this.setState({
        Isconected: state.isConnected
      })

    });
  }

  refeshButton = () =>{
    NetInfo.fetch().then(state => {
      this.setState({
        Isconected: state.isConnected
      })
    });
  }
  render() {
    if (this.state.token == 'fetching') {
      return null
    } if (!this.state.Isconected){
      return <ViewCuston light={colors.white} dark={colors.black} style={{justifyContent: 'center', height: dimensions.Height(100)}} containers={
              <View>
                <Image source={image.Nointernet} style={{width: dimensions.Width(100), height: dimensions.Height(40)}}/>
                <CustomText light={colors.green_main} dark={colors.white} style={{textAlign: 'center', fontSize: dimensions.FontSize(18), fontWeight: '300'}}>Hay un problema con tu conexión a internet.</CustomText>
                <TouchableOpacity onPress={()=> this.refeshButton()} style={{alignSelf: 'center', backgroundColor: colors.green_main, padding: 15, borderRadius: 40, marginTop: dimensions.Height(5)}}><Text style={{color: colors.white, fontSize: dimensions.FontSize(16), paddingHorizontal: 40}}>Volver a intentarlo</Text></TouchableOpacity>
            </View>} />
    } else
    return (
      <DarkModeProvider>
      <Provider store={store}>
        <PersistGate loading={true} persistor={persistor}>
          {/* <ProcessIndicator ref={ref=>global.processRef = ref}/> */}
          <ApolloProvider client={apolloClient}>
            {this.state.token == false ? <UnAuthContainer
              ref={navigatorRef => {
                NavigationService.setContainer(navigatorRef);
              }}
            /> : <AuthContainer
                ref={navigatorRef => {
                  NavigationService.setContainer(navigatorRef);
                }}
              />}
          </ApolloProvider>
        </PersistGate>
      </Provider>
      </DarkModeProvider>
    );
  }
}
