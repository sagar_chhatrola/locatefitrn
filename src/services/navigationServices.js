import {NavigationActions} from 'react-navigation';

let _navigator;

/**
 * set top level navigator ref
 */
function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

/**
 * move to another screen
 * @param {string} routeName
 * @param {object} params
 */
function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
}

export default {setTopLevelNavigator, navigate};