import React, { Component } from "react"
import {
  AppRegistry,
  Button,
  StyleSheet,
  Text,
  View,
  Alert,
  NativeModules,
  TouchableOpacity } from "react-native"
import { dimensions } from "../theme"

const { RNTwitterSignIn } = NativeModules

const Constants = {
  //Dev Parse keys
  TWITTER_COMSUMER_KEY: "owgI95CXvidObdnCzoV9hySvo",
  TWITTER_CONSUMER_SECRET: "lcY38cZ9aDaSjhxlJbCED7MYveLLofnlIS8y3GDWhBoU2rOhaG"
}

export default class TwitterButton extends Component {
  state = {
    isLoggedIn: false
  }

  componentDidMount(){
    RNTwitterSignIn.init(Constants.TWITTER_COMSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET)

  }

  _twitterSignIn = () => {
    RNTwitterSignIn.logIn()
      .then(loginData => {
        let email = loginData.email
        let firstName = loginData.userName
        let lastName = ''
        let token = loginData.userID
        this.props.onSuccess({firstName,lastName,email,token})
        const { authToken, authTokenSecret } = loginData
        //alert(authToken)
        if (authToken && authTokenSecret) {
          this.setState({
            isLoggedIn: true
          })
        }
      })
      .catch(error => {
        console.log("error",error)
      }
    )
  }

  handleLogout = () => {
    console.log("logout")
    RNTwitterSignIn.logOut()
    this.setState({
      isLoggedIn: false
    })
  }

  render() {
    const { isLoggedIn } = this.state
    return (
      <View style={{flex:1,alignItems:'center'}}>
        {isLoggedIn
          ? <TouchableOpacity onPress={this.handleLogout}>
              <Text>Log out</Text>
            </TouchableOpacity>
          : <TouchableOpacity  style={styles.button} onPress={()=>this._twitterSignIn()}>
            <Text style={{color:'#FFF',alignSelf:'center',fontWeight:'bold',fontSize:dimensions.FontSize(13)}}>Twitter</Text>
            </TouchableOpacity>}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    flex:1,
    justifyContent:'center'
    
    
  
  }
})