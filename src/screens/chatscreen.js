import React from 'react';
import { StyleSheet, View, TouchableOpacity, SafeAreaView, TextInput, FlatList, Image, Text, RefreshControl } from 'react-native';
import { colors, dimensions } from '../theme';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { CustomText } from '../components/CustomText';
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { image } from '../constants/image'
import { Avatar } from 'react-native-elements';
import { SwipeAction, Badge } from '@ant-design/react-native';
import moment from 'moment';
import { NETWORK_INTERFACE_LINK_AVATAR } from './../constants/config';
import Iconsverifiel from 'react-native-vector-icons/Octicons';
import Header from './../components/Header';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';


const GET_CONVERSATION = gql`
  query getConversation($profeional: ID!) {
    getConversation(profeional: $profeional) {
      message
      success
      conversation{
        _id
      prfsnal{
        id
        nombre
        apellidos
        Verified
        UserID
        foto_del_perfil
      }
      client{
        id
        nombre
        apellidos
        Verified
        UserID
        foto_del_perfil
      }
      messagechat{
        _id
    		text
    		read
    		createdAt
        user{
          _id
          name
          avatar
        }
      }
      }
    }
  }
`;

const GET_MESSAGE_NO_READ = gql`
  query getMessageNoRead($conversationID: ID! $profeional: ID!) {
    getMessageNoRead(conversationID: $conversationID profeional: $profeional) {
      message
    success
    conversation{
      _id
      count
    }
    }
  }`

const READ_MESSAGE = gql`
mutation readMessage($conversationID: ID!) {
  readMessage(conversationID: $conversationID) {
    success
    message
  }
}
`;

const DELETE_CONVERSATION = gql`
mutation deleteConversation($id: ID!) {
  deleteConversation(id: $id) {
    success
    message
  }
}
`;

class Chat extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      refreshing: false,
    };
  }


  refetch = null


  componentDidMount = async () => {
    const userId = await AsyncStorage.getItem('id');
    this.setState({ userId })
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.setState({
        refreshing: false
      })
    }, 2000)
  }


  ///component
  _renderItem({ item }, refetch) {
    const { navigation } = this.props;
    const datos = item ? item.prfsnal[0] : ''
    if (datos.id === this.state.userId) {
      const datos = item ? item.client[0] : ''
      const mgs = item ? item.messagechat[0] : ''
      const { navigation } = this.props;
      const dataCOUNT = navigation.getParam('data')
      return (
        <Mutation mutation={DELETE_CONVERSATION} variables={{ conversationID: item._id }}>
          {(deleteConversation, data) => {
            return (
              <Mutation mutation={READ_MESSAGE} variables={{ conversationID: item._id }}>
                {(readMessage, data) => {
                  return (
                    <SwipeAction
                      autoClose
                      style={{ backgroundColor: 'transparent' }}
                      right={[
                        {
                          text: 'Eliminar',
                          style: { backgroundColor: colors.ERROR, color: 'white' },
                          onPress: () => {
                            deleteConversation({ variables: { id: item._id } }).then(() => {
                              Toast.showWithGravity('Conversación eliminada con éxito', Toast.LONG, Toast.TOP)
                              refetch()
                            })
                          }
                        },
                      ]}
                      left={[{
                        text: 'Marcar como leído',
                        style: { backgroundColor: colors.twitter_color, color: 'white' },
                        onPress: () => {
                          readMessage({ variables: { conversationID: item._id } }).then(() => {
                            refetch()
                            Toast.showWithGravity('Mensajes marcado como leído', Toast.LONG, Toast.TOP)
                          }
                          );
                        },
                      },]}>
                      <Query query={GET_MESSAGE_NO_READ} variables={{ conversationID: item._id, profeional: dataCOUNT }}>
                        {(response, error,) => {
                          if (error) {
                            return console.log('Response Error-------', error);
                          }
                          if (response) {
                            const dat = response && response.data && response.data.getMessageNoRead ? response.data.getMessageNoRead.conversation[0] : ''
                            return (
                              <TouchableOpacity style={{ minHeight: dimensions.Height(9) }}
                                onPress={() => {
                                  readMessage({ variables: { conversationID: item._id } }).then(() => {
                                    refetch()
                                    navigation.navigate('MessagechatList', { data: item })
                                  }
                                  );
                                }}>
                                <View style={{ borderBottomColor: colors.rgb_235, borderBottomWidth: 0.5 }}>
                                  <View style={{ flexDirection: 'row', padding: 20 }}>
                                  <Badge text={dat ? dat.count : ''} overflowCount={10}>
                                    <Avatar
                                      rounded
                                      size="medium"
                                      source={{ uri: NETWORK_INTERFACE_LINK_AVATAR + datos.foto_del_perfil }}
                                    />
                                    </Badge>
                                    <View style={{ marginLeft: 20 }}>
                                      <View style={{ flexDirection: 'row' }}>
                                        <CustomText light={colors.black} dark={colors.white} numberOfLines={1} style={{ fontSize: dimensions.FontSize(20) }}>{datos.nombre} {datos.apellidos}</CustomText>
                                        {
                                          datos.Verified ?
                                            <Iconsverifiel name="verified" style={{ alignSelf: 'center', marginLeft: 5, marginTop: 3 }} size={12} color={colors.twitter_color} /> : null
                                        }
                                      </View>
                                      <Text numberOfLines={1} style={{ color: colors.rgb_153, fontWeight: '300', fontSize: dimensions.FontSize(14), marginTop: 5, width: dimensions.Width(40) }}>{mgs.text}</Text>
                                    </View>
                                    <Text numberOfLines={1} style={{ color: colors.rgb_153, fontWeight: '200', marginTop: 5, alignSelf: 'center', marginLeft: 'auto' }}>
                                      {moment(mgs.createdAt).fromNow('LT')}</Text>
                                    <AntdIcons name="right" size={14} color={colors.rgb_153} style={{ marginLeft: 'auto', alignSelf: 'center', }} />
                                  </View>
                                </View>
                              </TouchableOpacity>
                            )
                          }
                        }}
                      </Query>
                    </SwipeAction>
                  );
                }}
              </Mutation>
            );
          }}
        </Mutation>
      )
    } else {
      const datos = item ? item.prfsnal[0] : ''
      const mgs = item ? item.messagechat[0] : ''
      const { navigation } = this.props;
      const dataCOUNT = navigation.getParam('data')
      return (
        <Mutation mutation={DELETE_CONVERSATION} variables={{ conversationID: item._id }}>
          {(deleteConversation, data) => {
            return (
              <Mutation mutation={READ_MESSAGE} variables={{ conversationID: item._id }}>
                {(readMessage, data) => {
                  return (
                    <SwipeAction
                      autoClose
                      style={{ backgroundColor: 'transparent' }}
                      right={[
                        {
                          text: 'Eliminar',
                          style: { backgroundColor: colors.ERROR, color: 'white' },
                          onPress: () => {
                            deleteConversation({ variables: { id: item._id } }).then(() => {
                              Toast.showWithGravity('Conversación eliminada con éxito', Toast.LONG, Toast.TOP)
                              refetch()
                            })
                          },
                        },
                      ]}
                      left={[{
                        text: 'Marcar como leído',
                        style: { backgroundColor: colors.twitter_color, color: 'white' },
                        onPress: () => {
                          readMessage({ variables: { conversationID: item._id } }).then(() => {
                            refetch()
                            Toast.showWithGravity('Mensajes marcado como leído', Toast.LONG, Toast.TOP)
                          }
                          );
                        },
                      },]}
                    >
                      <Query query={GET_MESSAGE_NO_READ} variables={{ conversationID: item._id, profeional: dataCOUNT }}>
                        {(response, error,) => {
                          if (error) {
                            return console.log('Response Error-------', error);
                          }
                          if (response) {
                            const dat = response && response.data && response.data.getMessageNoRead ? response.data.getMessageNoRead.conversation[0] : ''
                            return (
                              <TouchableOpacity style={{ minHeight: dimensions.Height(9) }}
                                onPress={() => {
                                  readMessage({ variables: { conversationID: item._id } }).then(() => {
                                    refetch()
                                    navigation.navigate('MessagechatList', { data: item })
                                  }
                                  );
                                }}>
                                <View style={{ borderBottomColor: colors.rgb_235, borderBottomWidth: 0.5 }}>
                                  <View style={{ flexDirection: 'row', padding: 20 }}>
                                  <Badge text={dat ? dat.count : ''} overflowCount={10}>
                                    <Avatar
                                      rounded
                                      size="medium"
                                      source={{ uri: NETWORK_INTERFACE_LINK_AVATAR + datos.foto_del_perfil }}
                                    />
                                    </Badge>
                                    <View style={{ marginLeft: 20 }}>
                                      <View style={{ flexDirection: 'row' }}>
                                        <CustomText light={colors.black} dark={colors.white} numberOfLines={1} style={{ fontSize: dimensions.FontSize(20) }}>{datos.nombre} {datos.apellidos}
                                        </CustomText>
                                        {
                                          datos.Verified ?
                                            <Iconsverifiel name="verified" style={{ alignSelf: 'center', marginLeft: 5, marginTop: 3 }} size={12} color={colors.twitter_color} /> : null
                                        }
                                      </View>
                                      <Text numberOfLines={1} style={{ color: colors.rgb_153, fontWeight: '300', fontSize: dimensions.FontSize(14), marginTop: 5, width: dimensions.Width(40) }}>{mgs.text}</Text>
                                    </View>
                                    <Text numberOfLines={1} style={{ color: colors.rgb_153, fontWeight: '200', marginTop: 5, alignSelf: 'center', marginLeft: 'auto' }}>
                                      {moment(mgs.createdAt).fromNow('LT')}</Text>
                                    <AntdIcons name="right" size={14} color={colors.rgb_153} style={{ marginLeft: 'auto', alignSelf: 'center', }} />
                                  </View>
                                </View>
                              </TouchableOpacity>
                            )
                          }
                        }}
                      </Query>
                    </SwipeAction>
                  );
                }}
              </Mutation>
            );
          }}
        </Mutation>
      )
    }
  }


  render() {
    const { navigation } = this.props;
    const datart = navigation.getParam('data')
    return (
      <ViewCuston light={colors.white} dark={colors.black} containers={
      <View style={{height: dimensions.Height(100)}}>
        <View style={{ paddingHorizontal: dimensions.Width(4)}}>
          <Header navigation={navigation} />
          <View style={{ marginTop: 20, marginBottom: dimensions.Height(3) }}>
            <CustomText light={colors.blue_main} dark={colors.white} style={{ fontSize: 18, fontWeight: '500' }}>
              <IconsD name="message1" size={18}  light={colors.blue_main} dark={colors.white} />  Chats
            </CustomText>
            <CustomText light={colors.blue_main} dark={colors.rgb_235} style={{ fontWeight: '300', fontSize: dimensions.FontSize(14), marginLeft: 25 }}>
              Aquí tienes todas tus conversaciones.
            </CustomText>
          </View>
        </View>

        <Query query={GET_CONVERSATION} variables={{ profeional: datart }}>
          {(response, error, refetch) => {
            this.refetch = refetch;
            if (error) {
              return console.log('Response Error-------', error);
            }
            if (response) {
              const Conversation = response && response.data && response.data.getConversation ? response.data.getConversation.conversation : ''
              return (
                <FlatList
                  data={Conversation}
                  showsHorizontalScrollIndicator={false}
                  renderItem={(item) => this._renderItem(item, response.refetch)}
                  keyExtractor={item => item._id}
                  ListEmptyComponent={
                    <View style={{ alignSelf: 'center', padding: 20 }}>
                      <Image source={image.Vacia} style={{ width: dimensions.Width(34), alignSelf: 'center', height: dimensions.Height(13) }} />
                      <CustomText light={colors.blue_main} dark={colors.white} style={{ textAlign: 'center', fontSize: dimensions.FontSize(20), fontWeight: '200' }}>Aún no tienes conversaciones</CustomText>
                    </View>
                  }
                  refreshControl={
                    <RefreshControl
                      refreshing={this.state.refreshing}
                      onRefresh={this._onRefresh}
                    />
                  }
                />
              )
            }
          }}
        </Query>
      </View>} />
    )
  }
}

export default Chat;



