import React, { Component } from 'react';
import { View, Image, ImageBackground, StyleSheet, Alert, Linking, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { colors, dimensions } from './../theme';
import { Button } from './../components/Button';
import { CustomText } from './../components/CustomText';
import { image } from './../constants';
import { SliderBox } from "react-native-image-slider-box";
import { ViewCuston } from '../components/CustonView';

class LandingScreen extends Component {
  constructor(props){
    super(props)
    this.state = {
      images: [
        require('./../assets/images/barner1.png'),
        require('./../assets/images/Barner2png.png'),
        require('./../assets/images/barner3.png'),          // Local image
      ]
    };
  }


  render() {
    return (
      <ViewCuston light={colors.white} dark={colors.black} containers={
      <View style={styles.container}>
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always">
        <View style={{marginTop: dimensions.Height(10)}}>
          <SliderBox
          images={this.state.images}
          sliderBoxHeight={300}
          autoplay
          disableOnPress={false}
          circleLoop={true}
          dotColor={colors.green_main}
          imageLoadingColor={colors.green_main}
          dotStyle={{
            width: 50,
            height: 10,
            borderRadius: 5,
            marginHorizontal: 0,
            padding: 0,
            margin: 0,
          }}
        />
        </View>
       
          <ImageBackground source={image.img_map} resizeMode="contain" style={{justifyContent:'center',alignItems:'center', height:dimensions.Height(30)}}> 
              
              <CustomText light={colors.blue_main} dark={colors.white} style={{fontSize:dimensions.FontSize(28), fontWeight: 'bold'}}>Los mejores profesionales</CustomText>
              <CustomText style={{marginTop:dimensions.Height(1),fontSize:dimensions.FontSize(20),color:colors.slight_black,alignSelf:'center'}}>
                  Tu eliges la fecha, hora y lugar
              </CustomText>
              <CustomText style={{marginTop:dimensions.Height(1),fontSize:dimensions.FontSize(16),color:colors.rgb_153,alignSelf:'center', paddingHorizontal: 20, textAlign: 'center',}}>
                En Locatefit nuestro principal objetivo es ofrecerte los mejores profesionales al menor precio, para que te ayuden con las tareas del día a día.
              </CustomText>
          </ImageBackground>
          <View style={styles.formView}>         
            <View style={styles.signupButtonContainer}>
              <Button light={colors.white} dark={colors.white} containerStyle={styles.buttonView} 
                        onPress={()=>this.props.navigation.navigate('Home')} title="Explorar" 
                        titleStyle={styles.buttonTitle}/>
            </View>
            <View style={styles.signupButtonContainer}>
              <Button light={colors.white} dark={colors.white} containerStyle={styles.buttonView} 
                        onPress={()=>this.props.navigation.navigate('Login')} title="Iniciar sesión" 
                        titleStyle={styles.buttonTitle}/>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>}
      />
    );
  }
}

export default connect(
  null,
  {  }
)(LandingScreen);

const styles = StyleSheet.create({
  container:{
    backgroundColor: 'transparent'
  },
  formView:{
    marginHorizontal:dimensions.Width(8),
    flex:1,
    marginTop:dimensions.Height(0),
    marginBottom: dimensions.Height(10)
  },
  rememberMeView:{
    marginTop:dimensions.Height(2),
    flexDirection:'row'
  },
  rememberText:{
    fontSize:dimensions.FontSize(18)
  },
  signupButtonContainer:{
    marginTop:dimensions.Height(2),
    alignSelf:'center'
  },
  buttonView:{
    backgroundColor:colors.light_blue,
    width:dimensions.Width(84),
    borderRadius:dimensions.Width(8),
  },
  buttonTitle:{
    alignSelf:'center',
    paddingVertical:dimensions.Height(2),
    paddingHorizontal:dimensions.Width(5),
    color:colors.white,
    fontWeight:'bold',
    fontSize:dimensions.FontSize(17),
  }
})