import React from 'react';
import { View, Image, Text, StyleSheet, Alert, Linking, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { colors, dimensions } from './../theme';
import { CustomTextInput } from './../components/CustomTextInput';
import { CustomText } from './../components/CustomText';
import { Button } from './../components/Button';
import { Avatar } from 'react-native-elements';
import { NETWORK_INTERFACE_LINK_AVATAR } from './../constants/config';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import MaterialCommunityIcons from 'react-native-vector-icons/Feather';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconsverifiel from 'react-native-vector-icons/Octicons';
import moment from 'moment';
import Link from '../components/Link2';

const USER_DETAIL = gql`
  query getUsuario($id: ID!) {
    getUsuario(id: $id) {
      email
      nombre
      apellidos
      ciudad
      telefono
      foto_del_perfil
      fotos_tu_dni
      profesion
      descripcion
      fecha_de_nacimiento
      notificacion
      grado
      estudios
      formularios_de_impuesto
      fb_enlazar
      twitter_enlazar
      instagram_enlazar
      youtube_enlazar
      propia_web_enlazar
      created_at
    }
  }
`;


class DrawerScreen extends React.PureComponent {

  state = { loggedIn: false, userId: '' }

  async componentDidMount() {
    const token = await AsyncStorage.getItem('token');
    const id = await AsyncStorage.getItem('id');
    if (token) {
      this.setState({ loggedIn: true, userId: id })
    }
  }
  async componentDidUpdate() {
    const token = await AsyncStorage.getItem('token');
    const id = await AsyncStorage.getItem('id');
    if (token) {
      this.setState({ loggedIn: true, userId: id })
    } else {
      this.setState({ loggedIn: false })
    }
  }

   logout = async () => {
      await AsyncStorage.removeItem('token')
      await  AsyncStorage.removeItem('id')
    this.props.navigation.navigate('Landing')
  }
  
  render() {
    const { loggedIn, userId } = this.state;
    const { navigation } = this.props;
    if (!loggedIn) {
      return (
        <View>
        <View style={styles.formView}>
      </View>
      <CustomText style={{ marginLeft: 30, fontSize: dimensions.FontSize(18), marginTop: 30, color: colors.blue_main }}>Bienvenido a Locatefit' </CustomText>
      <View style={styles.signupButtonContainer}>
        <Button containerStyle={styles.buttonView} 
                onPress={() => this.props.navigation.navigate('Login')}
               title="Iniciar sesión" 
               titleStyle={styles.buttonTitle}/>
        <Button containerStyle={styles.buttonView11} 
                onPress={() => this.props.navigation.navigate('Register')}
               title="Regístrarme" 
               titleStyle={styles.buttonTitle}/>
    </View>
   </View>
       )
    }
    return (
      <Query query={USER_DETAIL} variables={{ id: userId }}>
        {(response, error, ) => {
          if (error) {
            return console.log('Response Error-------', error);
          }
          if (response) {
            const imageName = response && response.data && response.data.getUsuario.foto_del_perfil;
            const profileLink = NETWORK_INTERFACE_LINK_AVATAR + imageName;
            const FechaIni = response && response.data && response.data.getUsuario.created_at;
            return (
              <View style={styles.container}>
                <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
                  <View style={styles.formView}>
                    <Image source={{uri: profileLink}} style={{width: 50, height: 50, borderRadius: 33, borderWidth: 3, borderColor: colors.rgb_235}} />
                    <View>
                    <View style={{flexDirection: 'row'}}>
                      <CustomText style={{ marginLeft: 10, fontSize: dimensions.FontSize(25), color: colors.black }}>Hola' {response && response.data && response.data.getUsuario.nombre}</CustomText>
                      <Iconsverifiel name="verified" style={{alignSelf: 'center', marginLeft: 5, marginTop: 3}} size={12} color={colors.twitter_color}/>
                    </View>
                    <CustomText style={{ marginLeft: 10, fontSize: dimensions.FontSize(12), color: colors.rgb_102, fontWeight: '200', marginTop: 3 }}>Miembro desde  {moment(Number(FechaIni)).format('ll')}</CustomText>
                    </View>
                  </View>
                  <View style={{ marginHorizontal: dimensions.Width(2), marginTop: dimensions.Height(8), paddingBottom: dimensions.Height(10) }}>
                  
                  <TouchableOpacity onPress={()=>this.props.navigation.navigate('MyAccount')} style={{flexDirection: 'row', width: dimensions.Width(67), marginBottom: 20, borderBottomWidth: 0.5, borderBottomColor: colors.rgb_235, paddingBottom: 10}}>
                    <AntdIcons name="user" style={{alignSelf: 'center' }} size={30} color={colors.blue_main}/>
                      <View style={{marginLeft: 10, alignSelf: 'center'}}>
                        <Text style={{fontSize: dimensions.FontSize(16), color: colors.black}}>
                        Mi cuenta
                        </Text>
                        <Text style={{fontSize: dimensions.FontSize(14), color: colors.slight_black, fontWeight: '200'}}>
                        Aquí podrás editar tu información.
                        </Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={()=>this.props.navigation.navigate('Address')} style={{flexDirection: 'row', width: dimensions.Width(67), marginBottom: 20, borderBottomWidth: 0.5, borderBottomColor: colors.rgb_235, paddingBottom: 10}}>
                    <MaterialCommunityIcons name="map-pin" style={{alignSelf: 'center' }} size={30} color={colors.blue_main}/>
                      <View style={{marginLeft: 10, alignSelf: 'center'}}>
                        <Text style={{fontSize: dimensions.FontSize(16), color: colors.black}}>
                        Dirección
                        </Text>
                        <Text style={{fontSize: dimensions.FontSize(14), color: colors.slight_black, fontWeight: '200'}}>
                        Gestiona tu dirección de para servicio.
                        </Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={()=>this.props.navigation.navigate('Mypayment')} style={{flexDirection: 'row', width: dimensions.Width(67), marginBottom: 20, borderBottomWidth: 0.5, borderBottomColor: colors.rgb_235, paddingBottom: 10}}>
                    <Icons name="currency-eur" style={{alignSelf: 'center' }} size={30} color={colors.blue_main}/>
                      <View style={{marginLeft: 10, alignSelf: 'center'}}>
                        <Text style={{fontSize: dimensions.FontSize(16), color: colors.black}}>
                          Pago
                        </Text>
                        <Text style={{fontSize: dimensions.FontSize(14), color: colors.slight_black, fontWeight: '200'}}>
                        Gestiona tus métodos de pago
                        </Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={()=>this.props.navigation.navigate('Coments')} style={{flexDirection: 'row', width: dimensions.Width(67), marginBottom: 20, borderBottomWidth: 0.5, borderBottomColor: colors.rgb_235, paddingBottom: 10}}>
                  <AntdIcons name="staro" style={{alignSelf: 'center' }} size={30} color={colors.blue_main}/>
                    <View style={{marginLeft: 10, alignSelf: 'center'}}>
                      <Text style={{fontSize: dimensions.FontSize(16), color: colors.black}}>
                      Opiniones
                      </Text>
                      <Text style={{fontSize: dimensions.FontSize(14), color: colors.slight_black, fontWeight: '200'}}>
                        Mira lo que opinan los clientes de ti.
                      </Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.props.navigation.navigate('Chatscreen')} style={{flexDirection: 'row', width: dimensions.Width(67), marginBottom: 20, borderBottomWidth: 0.5, borderBottomColor: colors.rgb_235, paddingBottom: 10}}>
                <AntdIcons name="message1" style={{alignSelf: 'center' }} size={30} color={colors.blue_main}/>
                  <View style={{marginLeft: 10, alignSelf: 'center'}}>
                    <Text style={{fontSize: dimensions.FontSize(16), color: colors.black}}>
                    Mensajes
                    </Text>
                    <Text style={{fontSize: dimensions.FontSize(14), color: colors.slight_black, fontWeight: '200'}}>
                    Aqui tienes todas tus conversaciones.
                    </Text>
                </View>
              </TouchableOpacity>

              <View  style={{flexDirection: 'row', width: dimensions.Width(67), marginBottom: 20, borderBottomWidth: 0.5, borderBottomColor: colors.rgb_235, paddingBottom: 10}}>
                <AntdIcons name="plus" style={{alignSelf: 'center' }} size={30} color={colors.blue_main}/>
                  <View style={{marginLeft: 10, alignSelf: 'center'}}>
                  <View><Link   url="https://locatefit.es/profile" text="Publicar un servicio"/></View>
                    <Text style={{fontSize: dimensions.FontSize(14), color: colors.slight_black, fontWeight: '200'}}>
                    Convierte en un gran profesional.
                    </Text>
                </View>
              </View>

              <TouchableOpacity onPress={()=>this.props.navigation.navigate('Pedido')} style={{flexDirection: 'row', width: dimensions.Width(67), marginBottom: 20, borderBottomWidth: 0.5, borderBottomColor: colors.rgb_235, paddingBottom: 10}}>
                <AntdIcons name="file1" style={{alignSelf: 'center' }} size={30} color={colors.blue_main}/>
                  <View style={{marginLeft: 10, alignSelf: 'center'}}>
                    <Text style={{fontSize: dimensions.FontSize(16), color: colors.black}}>
                    Pedidos
                    </Text>
                    <Text style={{fontSize: dimensions.FontSize(14), color: colors.slight_black, fontWeight: '200'}}>
                    Gestiona y ve detalles de tu pedido.
                    </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={()=>this.props.navigation.navigate('Orders')} style={{flexDirection: 'row', width: dimensions.Width(67), marginBottom: 20, borderBottomWidth: 0.5, borderBottomColor: colors.rgb_235, paddingBottom: 10}}>
                <AntdIcons name="calendar" style={{alignSelf: 'center' }} size={30} color={colors.blue_main}/>
                  <View style={{marginLeft: 10, alignSelf: 'center'}}>
                    <Text style={{fontSize: dimensions.FontSize(16), color: colors.black}}>
                    Ordenes
                    </Text>
                    <Text style={{fontSize: dimensions.FontSize(14), color: colors.slight_black, fontWeight: '200'}}>
                    Gestiona y procesa tus ordenes.
                    </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={()=>this.props.navigation.navigate('Configuration')} style={{flexDirection: 'row', width: dimensions.Width(67), marginBottom: 20, borderBottomWidth: 0.5, borderBottomColor: colors.rgb_235, paddingBottom: 10}}>
                <AntdIcons name="setting" style={{alignSelf: 'center' }} size={30} color={colors.blue_main}/>
                  <View style={{marginLeft: 10, alignSelf: 'center'}}>
                    <Text style={{fontSize: dimensions.FontSize(16), color: colors.black}}>
                    Configuración y privacidad
                    </Text>
                    <Text style={{fontSize: dimensions.FontSize(14), color: colors.slight_black, fontWeight: '200'}}>
                    Gestiona tu privacidad y contacta con nosotros.
                    </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity  onPress={() => Alert.alert(
                'Cerrar sesión',
                '¿Seguro que quieres cerrar sesión?',
                [
                  {
                    text: 'Cancelar',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  { text: 'Sí', onPress: () => this.logout() },
                ],
                { cancelable: false },)} style={{flexDirection: 'row', width: dimensions.Width(67), marginBottom: 20, borderBottomWidth: 0.5, borderBottomColor: colors.rgb_235, paddingBottom: 10}}>
                <AntdIcons name="logout" style={{alignSelf: 'center' }} size={30} color={colors.ERROR}/>
                  <View style={{marginLeft: 10, alignSelf: 'center'}}>
                    <Text style={{fontSize: dimensions.FontSize(16), color: colors.black}}>
                    Cerrar sesión
                    </Text>
                    <Text style={{fontSize: dimensions.FontSize(14), color: colors.slight_black, fontWeight: '200'}}>
                    Cierre de sesion en la app.
                    </Text>
                </View>
              </TouchableOpacity>
                  </View>
                </KeyboardAwareScrollView>
              </View>

            )
          }
        }}
      </Query>
    );
  }
}


 
const mapStateToProps = (state) => {
  return {
    user: state.user.user
  }
}

export default connect(
  mapStateToProps,
  {}
)(DrawerScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.light_white,
  },
  formView: {
    marginHorizontal: dimensions.Width(6),
    flex: 1,
    marginTop: dimensions.Height(8),
    flexDirection: 'row',
  },
  rememberMeView: {
    marginTop: dimensions.Height(2),
    flexDirection: 'row'
  },
  rememberText: {
    fontSize: dimensions.FontSize(18)
  },
  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center'

  },
  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(60),
    borderRadius: dimensions.Width(8),
    marginHorizontal: dimensions.Width(6),
    marginTop: dimensions.Height(2),
  
  },

  buttonView11: {
    backgroundColor: colors.green_main,
    width: dimensions.Width(60),
    borderRadius: dimensions.Width(8),
    marginHorizontal: dimensions.Width(6),
    marginTop: dimensions.Height(2),
  
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1.5),
    paddingHorizontal: dimensions.Width(3),
    color: colors.white,
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(17),
  }
})