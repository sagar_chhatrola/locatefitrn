import React from 'react';
import { View, ScrollView, StyleSheet, Text, Image } from 'react-native';
import { colors, dimensions } from '../theme';
import { image } from './../constants/image';
import { CustomText } from '../components/CustomText'
import AntdIcons from 'react-native-vector-icons/AntDesign';
import Header from './../components/Header';
import Link from './../components/Link3';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';
import {Logo} from '../constants/logo';

const Datalist = [
  {
    text: "Seguir en twitter",
    icon: 'twitter',
    icon1: "right",
    url: "https://twitter.com/locatefites/"
  },

  {
    text: "Seguir en facebook",
    icon: 'facebook-square',
    icon1: "right",
    url: 'https://www.facebook.com/locatefit/'
  },

  {
    text: "Seguir en instagram",
    icon: 'instagram',
    icon1: "right",
    url: "https://www.instagram.com/locatefit/"
  },

  {
    text: "Seguir en Linkeding",
    icon: 'linkedin-square',
    icon1: "right",
    url: 'https://es.linkedin.com/company/locatefit?trk=public_profile_topcard_current_company'
  },

  {
    text: 'Blog',
    icon: 'aliwangwang-o1',
    icon1: "right",
    url: 'https://blog.locatefit.es'
  },
]

export default class Configuration extends React.Component {
  render() {
    const { navigation } = this.props;
    return (
      <ViewCuston light={colors.white} dark={colors.black} containers={
        <View style={styles.container}>
        <Header navigation={navigation}/>
        <View style={{ marginBottom: 25, marginTop: 5 }}>
          <CustomText light={colors.blue_main} dark={colors.white} style={{ fontSize: 20, fontWeight: '300', marginBottom: 10 }}>
            <AntdIcons name="customerservice" style={{ marginLeft: 'auto' }} size={20} color={colors.green_main} /> Ayuda
          </CustomText>
        </View>

        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={{color: colors.rgb_153,fontWeight: '300', marginBottom: 20}}>Social</Text>
          {
            Datalist.map((dat, i) =>(
                <ViewCuston key={dat.url} light={colors.light_white} dark={colors.back_dark} style={{flexDirection: 'row', height: dimensions.Height(5), borderRadius: 8, alignItems: 'center', marginBottom: 20}} containers={
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Text style={{marginLeft: 15}}>
                  <IconsD name={dat.icon} style={{marginLeft: 10 }} size={14} light={colors.black} dark={colors.white} />
                  </Text>
                  <View style={{marginLeft: 10 }}><Link dark={colors.black} light={colors.white} url={dat.url} text={dat.text}/></View>
                </View>
                } />
              )
            )
          }

          <Text style={{color: colors.rgb_153,fontWeight: '300', marginBottom: 20}}>Soporte</Text>

          <ViewCuston  light={colors.light_white} dark={colors.back_dark} style={{flexDirection: 'row', height: dimensions.Height(5), borderRadius: 8, alignItems: 'center', marginBottom: 20}} containers={
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={{marginLeft: 15}}>
            <IconsD name='questioncircleo' style={{marginLeft: 10 }} size={14} light={colors.black} dark={colors.white} />
            </Text>
            <View style={{marginLeft: 10 }}><Link dark={colors.black} light={colors.white} url='https://about.locatefit.es/preguntas-frecuentes' text="Preguntas frecuentes"/></View>
          </View>
          } />
          <ViewCuston  light={colors.light_white} dark={colors.back_dark} style={{flexDirection: 'row', height: dimensions.Height(5), borderRadius: 8, alignItems: 'center', marginBottom: 20}} containers={
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={{marginLeft: 15}}>
            <IconsD name='customerservice' style={{marginLeft: 10 }} size={14} light={colors.black} dark={colors.white} />
            </Text>
            <View style={{marginLeft: 10 }}><Link dark={colors.black} light={colors.white} url="https://about.locatefit.es/contacto" text='Contacto'/></View>
          </View>
          } />

          <Text style={{color: colors.rgb_153,fontWeight: '300', marginBottom: 20}}>Empleo</Text>

          <ViewCuston  light={colors.light_white} dark={colors.back_dark} style={{flexDirection: 'row', height: dimensions.Height(5), borderRadius: 8, alignItems: 'center', marginBottom: 20}} containers={
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={{marginLeft: 15}}>
            <IconsD name='team' style={{marginLeft: 10 }} size={14} light={colors.black} dark={colors.white} />
            </Text>
            <View style={{marginLeft: 10 }}><Link dark={colors.black} light={colors.white} url="https://about.locatefit.es/empleo" text='Trabaja con nosotros'/></View>
          </View>
          } />

          <Text style={{color: colors.rgb_153,fontWeight: '300', marginBottom: 20}}>Legal</Text>

          <ViewCuston  light={colors.light_white} dark={colors.back_dark} style={{flexDirection: 'row', height: dimensions.Height(5), borderRadius: 8, alignItems: 'center', marginBottom: 20}} containers={
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={{marginLeft: 15}}>
            <IconsD name='folderopen' style={{marginLeft: 10 }} size={14} light={colors.black} dark={colors.white} />
            </Text>
            <View style={{marginLeft: 10 }}><Link dark={colors.black} light={colors.white} url="https://about.locatefit.es/condiciones-de-uso" text='Términos y condiciones'/></View>
          </View>
          } />

          <ViewCuston  light={colors.light_white} dark={colors.back_dark} style={{flexDirection: 'row', height: dimensions.Height(5), borderRadius: 8, alignItems: 'center', marginBottom: 20}} containers={
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={{marginLeft: 15}}>
            <IconsD name='Safety' style={{marginLeft: 10 }} size={14} light={colors.black} dark={colors.white} />
            </Text>
            <View style={{marginLeft: 10 }}><Link dark={colors.black} light={colors.white} url="https://about.locatefit.es/privacidad" text='Póliticas de privacidad'/></View>
          </View>
          } />

          

    <View style={{alignSelf: 'center', marginTop: dimensions.Height(3), marginBottom: dimensions.Height(8)}}>
      <Logo />
      <Text style={{color: colors.rgb_153,fontWeight: '300', marginTop: 10, textAlign: 'center', fontSize: dimensions.FontSize(18)}}>Versión: 1.0.0</Text>
    </View>

  </ScrollView>
        
      </View>}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    paddingHorizontal: dimensions.Width(4),
    height: dimensions.Height(100)
  },

});