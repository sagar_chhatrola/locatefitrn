import React, { Component } from 'react';
import { View, Image, ImageBackground, StyleSheet, Alert, FlatList, TouchableOpacity, SafeAreaView, TextInput, ScrollView, RefreshControl, ActivityIndicator, Text } from 'react-native';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { connect } from 'react-redux';
import { colors, dimensions } from './../theme';
import { CustomText } from './../components/CustomText';
import { image } from './../constants';
import Category from '../components/category';
import moment from "moment";
import 'moment/locale/es';
import PlaceholderLoading from './placeholderloading';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { NETWORK_INTERFACE_LINK_AVATAR } from '../constants/config';
import MaterialCommunityIcons from 'react-native-vector-icons/Feather';
import { Badge } from '@ant-design/react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Geolocation from '@react-native-community/geolocation';
import RNLocation from 'react-native-location';
import LinearGradient from 'react-native-linear-gradient';
import { Avatar } from 'react-native-elements';
import Iconsverifiel from 'react-native-vector-icons/Octicons';
import * as StoreReview from 'react-native-store-review';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';

const PRODUCTO_CITY = gql`
query  getProductoCity($city: String){
    getProductoCity(city: $city) {
      success
      message
      data{
        id
          city
          category_id
          currency
          description
          domingo
          domingo_from
          domingo_to
          jueves
          jueves_from
          jueves_to
          lunes
          lunes_from
          lunes_to
          martes
          martes_from
          martes_to
          miercoles
          miercoles_from
          miercoles_to
          number
          sabado
          sabado_from
          sabado_to
          time
          title
          viernes
          viernes_from
          viernes_to
          visitas
          fileList
          anadidoFavorito
          created_by
          category {
            id
            title
          }
          ordenes{
            id
            cantidad
          }
        creator {
            id
            usuario
            email
            nombre
            Verified
            apellidos
            ciudad
            telefono
            tipo
            foto_del_perfil
            fotos_tu_dni
            profesion
            descripcion
            fecha_de_nacimiento
            notificacion
            grado
            estudios
            formularios_de_impuesto
            fb_enlazar
            twitter_enlazar
            instagram_enlazar
            youtube_enlazar
            propia_web_enlazar
            UserID
          }
        	professionalRating {
            id
            coment
            rate
            updated_at
            customer {
              foto_del_perfil
              nombre
              apellidos
              profesion
              ciudad
              }
           }
      }
    }
  }
`

const GET_NOTIFICATIONS = gql`
query getNotifications($userId: ID!) {
  getNotifications(userId: $userId) {
      success
      message
      notifications{
        _id
        user{
          id
          nombre
          apellidos
          foto_del_perfil
        }
        profesional{
          id
          nombre
          apellidos
          foto_del_perfil
        }
        cliente{
          id
          nombre
          apellidos
          foto_del_perfil
        }
        orden{
          id
        }
        type
        read
        createdAt
      }
  }
}
`;

const USUARIO_FAVORITO_PRODUCTS = gql`
    query {
          getUsuarioFavoritoProductos {
            success
            message
            list {
              id
              productoId
              producto {
                  id
                  city
                  category_id
                  currency
                  description
                  domingo
                  domingo_from
                  domingo_to
                  jueves
                  jueves_from
                  jueves_to
                  lunes
                  lunes_from
                  lunes_to
                  martes
                  martes_from
                  martes_to
                  miercoles
                  miercoles_from
                  miercoles_to
                  number
                  sabado
                  sabado_from
                  sabado_to
                  time
                  title
                  visitas
                  viernes
                  viernes_from
                  viernes_to
                  anadidoFavorito
                  fileList
                  created_by
                  category {
                    id
                    title
                  }
                  ordenes{
                    id
                       }
                  professionalRating {
                    id
                    coment
                    rate
                    updated_at
                    customer {
                      foto_del_perfil
                      nombre
                      apellidos
                      profesion
                      ciudad
                    }
                  }
                  creator {
                    id
                    usuario
                    email
                    nombre
                    UserID
                    Verified
                    apellidos
                    ciudad
                    telefono
                    tipo
                    foto_del_perfil
                    fotos_tu_dni
                    profesion
                    descripcion
                    fecha_de_nacimiento
                    notificacion
                    grado
                    estudios
                    formularios_de_impuesto
                    fb_enlazar
                    twitter_enlazar
                    instagram_enlazar
                    youtube_enlazar
                    propia_web_enlazar
                  }
                }
                }
              }
            }
          `;


const INSPIRATION_QUERY = gql`{
  getInspiration{
    id
    title
    image
    image1
    image2
    description
    created_at
    anadidoFavoritoinspiration
  }
}
`;


class ProfileScreen extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      currentDate: new Date(),
      markedDate: moment(new Date()).locale('es'),
      refreshing: false,
      Loading: true,
      city: null,
      user: '',
      lat: null,
      lng: null

    };
  }

  async componentDidMount() {

    if (StoreReview.isAvailable) {
      StoreReview.requestReview();
    }

    const id = await AsyncStorage.getItem('id');
    if (id) {
      this.setState({
        user: id
      })
    }
    RNLocation.configure({ allowsBackgroundLocationUpdates: false });
    RNLocation.requestPermission({
      ios: 'whenInUse', // or 'always'
      android: {
        detail: 'coarse', // or 'fine'
        rationale: {
          title: "Locatefit necesita usar tu ubicación",
          message: "Necesitamos tu ubicación para mostrarte profesionales cerca de ti",
          buttonPositive: "OK",
          buttonNegative: "Cancelar"
        }
      }
    });
    setTimeout(() => {
      this.setState({
        Loading: false,
      })
    }, 2000);

    Geolocation.getCurrentPosition(info => {
      this.setState({
        lat: info.coords.latitude,
        lng: info.coords.longitude
      })
    });
  }


  componentDidUpdate() {
    let apiUrlWithParams = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${this.state.lat},${this.state.lng}&key=AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg`;
    fetch(apiUrlWithParams)
      .then(response => response.json())
      .then(data => {

        let cityFound = false;

        for (let index = 0; index < data.results.length; index++) {

          for (let i = 0; i < data.results[index].address_components.length; i++) {

            if (data.results[index].address_components[i].types.includes('locality')) {

              this.setState({ city: data.results[index].address_components[i].long_name });
              cityFound = true;
            }

            if (cityFound) break;
          }

          if (cityFound) break;
        }
      })
      .catch(error => {
        // this.setState({ locationFilterChecked: false });
        console.log('error in product-plan getting lat, lng: ', error);

      })
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.setState({
        refreshing: false
      })
    }, 2000)
  }

  _renderItem({ item }) {
    let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
    item.professionalRating.forEach(start => {
      if (start.rate == 1) rating['1'] += 1;
      else if (start.rate == 2) rating['2'] += 1;
      else if (start.rate == 3) rating['3'] += 1;
      else if (start.rate == 4) rating['4'] += 1;
      else if (start.rate == 5) rating['5'] += 1;
    });

    const ar = (5 * rating['5'] + 4 * rating['4'] + 3 * rating['3'] + 2 * rating['2'] + 1 * rating['1']) / item.professionalRating.length;
    let averageRating = 0;
    if (item.professionalRating.length) {
      averageRating = ar.toFixed(1);
    }

    console.log('verificado', item.creator.id, item.creator.Verified)
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetails', { data: item })} style={{ marginBottom: 30 }}>
        {item.fileList.length > 0 ?
          <ImageBackground
            source={{ uri: item.fileList.length > 0 ? NETWORK_INTERFACE_LINK_AVATAR + item.fileList[0] : "" }}
            style={[
              styles.imageServices,
              { width: dimensions.Width(100), height: 180 }
            ]}
            imageStyle={{
              width: dimensions.Width(100),
              height: 180
            }}
          >
            <LinearGradient colors={['transparent', 'rgba(0, 0, 0, 0.2)', 'rgba(0, 0, 0, 0.5)']} style={{ width: dimensions.Width(100), height: 180, flexDirection: 'column-reverse' }}>
              <View style={{ flexDirection: 'row' }}>
                <Avatar
                  size='medium'
                  containerStyle={{ borderWidth: 0.2, borderColor: colors.rgb_235, marginBottom: 10, marginLeft: 10 }}
                  rounded
                  source={{ uri: NETWORK_INTERFACE_LINK_AVATAR + item.creator.foto_del_perfil }}
                />
                <View style={{ marginLeft: 10, marginTop: 5 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: 18, color: colors.white, fontWeight: '500' }}>{item.creator.nombre} {item.creator.apellidos}</Text>
                    {
                      item.creator.Verified ?
                        <Iconsverifiel name="verified" style={{ alignSelf: 'center', marginLeft: 5, marginTop: 2 }} size={12} color={colors.white} /> : null
                    }
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 3 }}>
                    <Text style={{ paddingRight: 10, color: colors.white }}>
                      <AntdIcons name="star" size={14} color={colors.orange} />
                      <AntdIcons name="star" size={14} color={colors.orange} />
                      <AntdIcons name="star" size={14} color={colors.orange} />
                      <AntdIcons name="star" size={14} color={colors.orange} />
                      <AntdIcons name="star" size={14} color={colors.orange} />
                    ({averageRating}) Valoraciones
                </Text>
                  </View>
                </View>
              </View>

            </LinearGradient>
          </ImageBackground> : null
        }
        <View style={{ flexDirection: 'row' }}>
          <View>
            <View style={{ margin: 10, width: dimensions.Width(88) }}>
              <CustomText light={colors.blue_main} dark={colors.white} numberOfLines={1} style={{ fontSize: 20, fontWeight: '500' }}>{item.title}</CustomText>
            </View>
            <View style={{ flexDirection: 'row', marginLeft: 10 }}>
              <CustomText light={colors.blue_main} dark={colors.rgb_235}>{item.number}€<CustomText light={colors.blue_main} dark={colors.rgb_235}>{item.currency}</CustomText></CustomText>
              <CustomText light={colors.blue_main} dark={colors.rgb_235} style={{ marginLeft: 20, fontWeight: '300', fontSize: dimensions.FontSize(12) }}><MaterialCommunityIcons name="map-pin" size={16} color={colors.green_main} /> {item.city}</CustomText>
            </View>
          </View>
          <View style={{ marginLeft: 'auto', marginRight: 10, marginTop: 10 }}>
            {item.anadidoFavorito ?
              <View>
                <AntdIcons name="heart" size={25} color={colors.ERROR} />
              </View>
              :
              <View>
                <AntdIcons name="hearto" size={25} color={colors.rgb_153} />
              </View>
            }
          </View>
        </View>

        <View style={{ flexDirection: 'row', margin: 5 }}>
          <View style={{ marginTop: 8 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('ProductDetails', { data: item })}
              style={{
                width: dimensions.Width(46),
                marginRight: 15,
                height: 40,
                marginLeft: 5,
                backgroundColor: 'transparent',
                borderRadius: 5,
                borderWidth: 1,
                borderColor: colors.green_main,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text style={{ fontSize: 14, alignItems: "center", color: colors.green_main, justifyContent: "center", }}>VER DETALLES</Text>
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 8 }}>
            <TouchableOpacity
              onPress={async () => {
                const id = await AsyncStorage.getItem('id');
                const { navigation } = this.props;
                const data = await navigation.getParam('data');
                if (id === null) {
                  Alert.alert(
                    'Upps debes iniciar sesión',
                    'Para iniciar una conversación debes iniciar sesión o regístrarte',
                    [
                      {
                        text: 'Iniciar sesión', onPress: () => this.props.navigation.navigate('Login'),
                      },
                      {
                        text: 'Regístrarme', onPress: () => this.props.navigation.navigate('Register')
                      },
                    ],
                    { cancelable: false },
                  )
                  return null

                } if (id === item.creator.id) {
                  Alert.alert(
                    'Upps álgo va mal',
                    'no puedes entablar una conversación con tigo mismo',
                    [
                      {
                        text: 'OK', onPress: () => console.log('ok'),
                      },
                    ],
                    { cancelable: false },
                  )
                  return null

                } else {
                  navigation.navigate('Message', { data: item })
                }
              }}
              style={{
                width: dimensions.Width(46),
                marginRight: 5,
                height: 40,
                backgroundColor: colors.green_main,
                borderRadius: 5,
                borderWidth: 1,
                borderColor: colors.green_main,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text style={{ fontSize: 14, alignItems: "center", color: colors.white, justifyContent: "center", }}>CHAT</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    )
  }


  render() {
    const today = this.state.currentDate;
    const day = moment(today).format("dddd");
    const date = moment(today).format("D, MMMM YYYY");
    const { navigation } = this.props
    return (
      <ViewCuston light={colors.white} dark={colors.black} containers={
        <View style={{ paddingBottom: dimensions.Height(40) }}>
          <SafeAreaView style={styles.headers}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ alignItems: 'flex-start', marginLeft: 20, marginTop: 10 }}>
                <TouchableOpacity onPress={() => navigation.navigate('Myprofile', { data: this.state.user })}>
                  <CustomText>
                    <IconsD name="user" size={25} light={colors.blue_main} dark={colors.white} />
                  </CustomText>
                </TouchableOpacity>
              </View>
              <View style={{ marginLeft: 'auto', flexDirection: 'row', marginTop: 15 }}>
                <Query query={GET_NOTIFICATIONS} variables={{ userId: this.state.user }}>
                  {(response, error, loading, refetch) => {
                    this.refetch = refetch;
                    if (loading) {
                      return null
                    }
                    if (error) {
                      return console.log('Response Error-------', error);
                    }
                    if (response) {
                      const datoscouter = response && response.data && response.data.getNotifications ? response.data.getNotifications.notifications.length : ''
                      return (
                        <Badge text={datoscouter} overflowCount={9} style={{ marginRight: 20 }}>
                          <TouchableOpacity onPress={() => navigation.navigate('Notification')}>
                            <CustomText>
                              <IconsD name="bells" size={25} light={colors.blue_main} dark={colors.white} />
                            </CustomText>
                          </TouchableOpacity>
                        </Badge>
                      )
                    }
                  }}
                </Query>
                <Query query={USUARIO_FAVORITO_PRODUCTS}>
                  {(response, error, loading, refetch) => {
                    this.refetch = refetch;
                    if (loading) {
                      return null
                    }
                    if (error) {
                      return console.log('Response Error-------', error);
                    }
                    if (response) {
                      const datos = response && response.data ? response.data.getUsuarioFavoritoProductos.list.length : ''
                      return (
                        <Badge text={datos} overflowCount={9} style={{ marginRight: 20 }}>
                          <TouchableOpacity onPress={() => navigation.navigate('Favourites')}>
                            <CustomText>
                              <IconsD name="hearto" size={25} light={colors.blue_main} dark={colors.white} />
                            </CustomText>
                          </TouchableOpacity>
                        </Badge>
                      )
                    }
                  }}
                </Query>
              </View>
            </View>
            <View style={{ marginTop: dimensions.Height(4) }}>
              <TextInput
                color={colors.rgb_153}
                inlineImageLeft='search_icon'
                keyboardAppearance='dark'
                returnKeyType='search'
                placeholder='¿Qué estás buscando?'
                clearButtonMode='always'
                onFocus={() => navigation.navigate('Search')}
                backgroundColor={'trasparent'}
                placeholderTextColor={colors.rgb_153}
                style={{
                  height: 40, marginLeft: 10, marginRight: 10, borderRadius: 50, paddingLeft: 20, paddingRight: 20, color: colors.blue_main,
                  borderColor: colors.rgb_153,
                  borderWidth: 0.5
                }}
              />
            </View>
          </SafeAreaView>
          <ScrollView showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          >
            <View>
              <View style={{ marginLeft: 15, marginTop: 20 }}>
                <Category navigation={this.props.navigation} />
              </View>
              <View style={{ marginTop: dimensions.Height(4), marginLeft: 20, flexDirection: 'row' }}>
                <View>
                  <CustomText light={colors.blue_main} dark={colors.white} style={{ fontWeight: '500', fontSize: dimensions.FontSize(20) }}>
                    Encuentra tu inspiración
                 </CustomText>
                  <View style={{ width: dimensions.Width(95), marginTop: 5 }}>
                    <CustomText style={{ color: colors.rgb_153 }}>{day} {date}</CustomText>
                  </View>
                </View>
              </View>
              <Query query={INSPIRATION_QUERY}>
                {(response, error, loading) => {
                  if (loading) {
                    return (<CustomText>
                      Loading.....
                    </CustomText>)
                  }
                  if (error) {
                    return console.log('Response Error-------', error);
                  }
                  if (response) {
                    const dataIns = response && response.data ? response.data.getInspiration[0] : ''
                    return (
                      <TouchableOpacity onPress={() => navigation.navigate('Inspiration')} style={{ marginTop: dimensions.Height(4), alignSelf: 'center' }}>
                        <ImageBackground
                          source={{ uri: dataIns.image }}
                          style={[
                            styles.imageBlock,
                            { width: dimensions.Width(95), height: 252 }
                          ]}
                          imageStyle={{
                            width: dimensions.Width(95),
                            height: 252
                          }}
                        >
                          <View style={styles.categoryTitle}>
                            <Text numberOfLines={2} style={{ color: colors.white, fontWeight: '500', fontSize: dimensions.FontSize(20), textAlign: 'center' }}>
                              {dataIns.title}
                            </Text>
                            <Text style={{ color: colors.white, fontWeight: '300', fontSize: dimensions.FontSize(16), marginTop: 5, }}>
                              Ver todos
                              </Text>
                          </View>
                        </ImageBackground>
                      </TouchableOpacity>
                    );
                  }
                }}
              </Query>
              <Query query={PRODUCTO_CITY} variables={{ city: this.state.city }}>
                {(response, error, loading) => {
                  if (loading) {
                    return <ActivityIndicator size="large" color={colors.green_main} />
                  }
                  if (error) {
                    return console.log('Response Error-------', error);
                  }
                  if (response) {
                    return (
                      <View>
                        <View style={{ marginTop: dimensions.Height(4), marginLeft: 20, flexDirection: 'row' }}>
                          <View style={{ width: 5, height: 30, backgroundColor: colors.green_main, marginRight: 10, borderRadius: 50 }}>
                          </View>
                          <CustomText light={colors.blue_main} dark={colors.white} numberOfLines={1} style={{ fontWeight: '200', fontSize: dimensions.FontSize(20), width: dimensions.Width(60) }}>
                            Cerca de ti en {this.state.city}
                          </CustomText>
                          <CustomText onPress={() => this.props.navigation.navigate('Search')} style={{ color: colors.green_main, fontWeight: '200', fontSize: dimensions.FontSize(16), marginTop: 3, marginLeft: 'auto', marginRight: 20 }}>
                            Explorar  <AntdIcons name="search1" size={16} color={colors.green_main} />
                          </CustomText>
                        </View>
                        <View style={{ marginTop: dimensions.Height(6), alignSelf: 'center', marginBottom: 50 }}>
                          <FlatList
                            keyExtractor={item => item.id}
                            data={response && response.data ? response.data.getProductoCity.data : ''}
                            renderItem={(item) => this._renderItem(item)}
                            ListEmptyComponent={
                              <View style={{ alignSelf: 'center', padding: 20 }}>
                                <Image source={image.Vacia} style={{ width: dimensions.Width(34), alignSelf: 'center', height: dimensions.Height(13) }} />
                                <CustomText light={colors.blue_main} dark={colors.white} style={{ textAlign: 'center', fontSize: dimensions.FontSize(20), fontWeight: '200' }}>Aún no hay profesionales cerca de ti.</CustomText>
                              </View>
                            }
                          />
                        </View>
                      </View>
                    )

                  }
                }}
              </Query>
            </View>
          </ScrollView>

        </View>
      } />
    );
  }
}

export default connect(
  null,
  {}
)(ProfileScreen);

const styles = StyleSheet.create({
  headers: {
    height: dimensions.Height(20),
    backgroundColor: "transparent",
  },
  formView: {
    marginHorizontal: dimensions.Width(8),
    flex: 1,
    marginTop: dimensions.Height(4)
  },
  rememberMeView: {
    marginTop: dimensions.Height(2),
    flexDirection: 'row'
  },
  rememberText: {
    fontSize: dimensions.FontSize(18)
  },
  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center'
  },
  buttonView: {
    backgroundColor: colors.white,
    width: dimensions.Width(54),
    borderRadius: dimensions.Width(1),
    shadowColor: colors.white,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 10,
    shadowOpacity: 0.5,
    elevation: 2,
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(2),
    color: colors.black,
    fontWeight: '200',
    fontSize: dimensions.FontSize(17),
  },
  notify: {
    backgroundColor: colors.ERROR,
    borderRadius: 4,
    height: dimensions.Height(1),
    width: dimensions.Height(1),
    position: 'absolute',
    right: 15,
  },

  categoryTitle: {
    height: "100%",
    width: dimensions.Width(95),
    paddingHorizontal: 10,
    backgroundColor: "rgba(0, 0, 0, 0.2)",
    justifyContent: "center",
    alignItems: "center"
  },

  imageBlock: {
    overflow: "hidden",
    borderRadius: 4
  },

  imageServices: {
    overflow: "hidden",
  },
  card: {
    width: dimensions.Width(90),
    height: dimensions.Height(16),
    backgroundColor: colors.white,
    margin: dimensions.Width(1),
    flexDirection: 'row',
    alignSelf: 'center',
    marginBottom: 30,
  },
  img: {
    width: dimensions.Width(22),
    height: dimensions.Height(10),
    backgroundColor: colors.white,
    borderRadius: 7,
    marginLeft: 'auto',
  }
});