import React, { Component } from 'react';
import { View, Image, ImageBackground, StyleSheet } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { colors, dimensions } from './../theme';
import { Button } from './../components/Button';
import { CustomText } from './../components/CustomText';
import { image } from './../constants';
import { ViewCuston } from '../components/CustonView';

class ConfirmEmail extends Component {
  render() {
    return (
      <ViewCuston light={colors.white} dark={colors.black} containers={
      <View style={styles.container}>
        <KeyboardAwareScrollView  keyboardShouldPersistTaps="always">
          <ImageBackground source={image.img_map} resizeMode="contain" style={{justifyContent:'center',alignItems:'center',height:dimensions.Height(75)}}> 
            <Image source={image.landing} style={{width: 300, height: 260, marginBottom: 40}} />
              <CustomText light={colors.blue_main} dark={colors.white} style={{fontSize:dimensions.FontSize(28), fontWeight: 'bold'}}>Confirma tu Email</CustomText>
              <CustomText style={{marginTop:dimensions.Height(1),fontSize:dimensions.FontSize(16),color:colors.rgb_153,alignSelf:'center', paddingHorizontal: 20, textAlign: 'center',}}>
                Ve a tu bandeja de entrada allí te hemos dejado un enlace magíco para que confirme tu email, estas a punto de empezara contratar profesionales o ofrecer tus sevicios.
              </CustomText>
          </ImageBackground>
          <View style={styles.formView}>         
            <View style={styles.signupButtonContainer}>
              <Button light={colors.white} dark={colors.white} containerStyle={styles.buttonView} 
                        onPress={()=>this.props.navigation.navigate('Login')} title="Iniciar sesión" 
                        titleStyle={styles.buttonTitle}/>
            </View>    
          </View>
        </KeyboardAwareScrollView>
      </View>}
      />
    );
  }
}

export default connect(
  null,
  {  }
)(ConfirmEmail);

const styles = StyleSheet.create({
  container:{
    backgroundColor: 'transparent',
    height: dimensions.Height(100)
  },
  formView:{
    marginHorizontal:dimensions.Width(8),
    flex:1,
    marginTop:dimensions.Height(0)
  },
  rememberMeView:{
    marginTop:dimensions.Height(2),
    flexDirection:'row'
  },
  rememberText:{
    fontSize:dimensions.FontSize(18)
  },
  signupButtonContainer:{
    marginTop:dimensions.Height(2),
    alignSelf:'center'
  },
  buttonView:{
    backgroundColor:colors.light_blue,
    width:dimensions.Width(84),
    borderRadius:dimensions.Width(8),
  },
  buttonTitle:{
    alignSelf:'center',
    paddingVertical:dimensions.Height(2),
    paddingHorizontal:dimensions.Width(5),
    color:colors.white,
    fontWeight:'bold',
    fontSize:dimensions.FontSize(17),
  }
})