import React from 'react';
import { View, ScrollView, StyleSheet, RefreshControl } from 'react-native';
import { colors, dimensions } from '../theme';
import { CustomText } from '../components/CustomText';
import CardServicesFavourites from '../components/CardServicesfavourites';
import LoadingPlaceHolders from './placeholderloadingor';
import Header from './../components/Header';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';
import {Logo} from '../constants/logo';


export default class Favourites extends React.Component {

  constructor() {
    super()
    this.state = {
      Loading: true,
      refreshing: false
    }
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({
        Loading: false
      })
    }, 2000)
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.setState({
        refreshing: false
      })
    }, 2000)
  }

  renderArticles = () => {
    const { navigation } = this.props;
    return (
      <ViewCuston light={colors.white} dark={colors.black} containers={
      <View style={styles.container}>
      <Header navigation={navigation} />
        <View style={{ marginBottom: 25, marginTop: 5 }}>
          <CustomText light={colors.blue_main} dark={colors.white} style={{ fontSize: 20, fontWeight: '500', marginBottom: 10 }}>
            <AntdIcons name="hearto" style={{ marginLeft: 'auto' }} size={20} color={colors.ERROR} />  Mi lista de deseos
          </CustomText>
          <CustomText style={{ color: colors.rgb_153, fontWeight: '300', fontSize: dimensions.FontSize(14) }}>
              Aquí te guardamos tu profesionales favoritos.
          </CustomText>

        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >
          {
            this.state.Loading ?
              <LoadingPlaceHolders />
              :
              <View style={{marginBottom: 300}}>
                <CardServicesFavourites navigation={navigation} />
              </View>
          }
        </ScrollView>
      </View>}
      />
    )
  }

  render() {
    return (
      <View>
        {this.renderArticles()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    paddingHorizontal: dimensions.Width(4),
    height: dimensions.Height(100)
  },

});