import React from 'react';
import { Text, StyleSheet, View, Image, ActivityIndicator, FlatList, TouchableOpacity, SafeAreaView, TextInput, ScrollView, RefreshControl, ImageBackground, Alert} from 'react-native';
import { colors, dimensions } from '../theme';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { image } from '../constants/image'
import MaterialCommunityIcons from 'react-native-vector-icons/Feather';
import { CustomText } from './../components/CustomText';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { NETWORK_INTERFACE_LINK_AVATAR } from '../constants/config';
import Header from '../components/Header';
import Geolocation from '@react-native-community/geolocation';
import RNLocation from 'react-native-location';
import LinearGradient from 'react-native-linear-gradient';
import { Avatar } from 'react-native-elements';
import Iconsverifiel from 'react-native-vector-icons/Octicons';
import AsyncStorage from '@react-native-community/async-storage';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';

const CATEGORY_QUERY = gql`
query getCategory($id: ID!,  $price: Int, $city: String) {
    getCategory(id: $id,  price: $price, city: $city) {
      success
      message
      data {
        id
        title
        productos {
          id
          city
          category_id
          currency
          description
          domingo
          domingo_from
          domingo_to
          jueves
          jueves_from
          jueves_to
          lunes
          lunes_from
          lunes_to
          martes
          martes_from
          martes_to
          miercoles
          miercoles_from
          miercoles_to
          number
          sabado
          sabado_from
          sabado_to
          time
          title
          viernes
          viernes_from
          viernes_to
          visitas
          fileList
          anadidoFavorito
          created_by
          category {
            id
            title
          }
          ordenes{
            id
          }
          creator {
            id
            usuario
            UserID
            Verified
            email
            nombre
            apellidos
            ciudad
            telefono
            tipo
            foto_del_perfil
            fotos_tu_dni
            profesion
            descripcion
            fecha_de_nacimiento
            notificacion
            grado
            estudios
            formularios_de_impuesto
            fb_enlazar
            twitter_enlazar
            instagram_enlazar
            youtube_enlazar
            propia_web_enlazar
          }
          professionalRating {
            id
            coment
            rate
            updated_at
            customer {
              foto_del_perfil
              nombre
              apellidos
              profesion
              ciudad
              }
           }
        }
      }
    }
  }
`


class Allproducts extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      city: null
    };
  }

  componentDidMount() {
    RNLocation.configure({ allowsBackgroundLocationUpdates: false });
    RNLocation.requestPermission({
      ios: 'whenInUse', // or 'always'
      android: {
        detail: 'coarse', // or 'fine'
        rationale: {
          title: "Locatefit necesita usar tu ubicación",
          message: "Necesitamos tu ubicación para mostrarte profesionales cerca de ti",
          buttonPositive: "OK",
          buttonNegative: "Cancelar"
        }
      }
    });

    Geolocation.getCurrentPosition(info => {
      this.setState({
        lat: info.coords.latitude,
        lng: info.coords.longitude
      })
    });
  }

  componentDidUpdate() {
    let apiUrlWithParams = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${this.state.lat},${this.state.lng}&key=AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg`;
    fetch(apiUrlWithParams)
      .then(response => response.json())
      .then(data => {

        let cityFound = false;

        for (let index = 0; index < data.results.length; index++) {

          for (let i = 0; i < data.results[index].address_components.length; i++) {

            if (data.results[index].address_components[i].types.includes('locality')) {

              this.setState({ city: data.results[index].address_components[i].long_name });
              cityFound = true;
            }

            if (cityFound) break;
          }

          if (cityFound) break;
        }

        console.log(this.state.city)

      })

      .catch(error => {
        console.log('error in product-plan getting lat, lng: ', error);

      })
  }


  _renderItem({ item }) {
    let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
    item.professionalRating.forEach(start => {
      if (start.rate == 1) rating['1'] += 1;
      else if (start.rate == 2) rating['2'] += 1;
      else if (start.rate == 3) rating['3'] += 1;
      else if (start.rate == 4) rating['4'] += 1;
      else if (start.rate == 5) rating['5'] += 1;
    });

    const ar = (5 * rating['5'] + 4 * rating['4'] + 3 * rating['3'] + 2 * rating['2'] + 1 * rating['1']) / item.professionalRating.length;
    let averageRating = 0;
    if (item.professionalRating.length) {
      averageRating = ar.toFixed(1);
    }
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetails', { data: item })} style={{ marginBottom: 30 }}>
        {item.fileList.length > 0 ?
          <ImageBackground
            source={{ uri: item.fileList.length > 0 ? NETWORK_INTERFACE_LINK_AVATAR + item.fileList[0] : "" }}
            style={[
              styles.imageServices,
              { width: dimensions.Width(100), height: 180 }
            ]}
            imageStyle={{
              width: dimensions.Width(100),
              height: 180
            }}
          >
            <LinearGradient colors={['transparent', 'rgba(0, 0, 0, 0.3)', 'rgba(0, 0, 0, 0.5)']} style={{ width: dimensions.Width(100), height: 180, flexDirection: 'column-reverse' }}>
              <View style={{ flexDirection: 'row' }}>
                <Avatar
                  size='medium'
                  containerStyle={{ borderWidth: 0.2, borderColor: colors.rgb_235, marginBottom: 10, marginLeft: 10 }}
                  rounded
                  source={{ uri: NETWORK_INTERFACE_LINK_AVATAR + item.creator.foto_del_perfil }}
                />
                <View style={{ marginLeft: 10, marginTop: 5 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: 18, color: colors.white, fontWeight: '500' }}>{item.creator.nombre} {item.creator.apellidos}</Text>
                    {
                      item.creator.Verified ?
                        <Iconsverifiel name="verified" style={{ alignSelf: 'center', marginLeft: 5, marginTop: 2 }} size={12} color={colors.white} /> : null
                    }
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 3 }}>
                    <Text style={{ paddingRight: 10, color: colors.white }}>
                      <AntdIcons name="star" size={14} color={colors.orange} />
                      <AntdIcons name="star" size={14} color={colors.orange} />
                      <AntdIcons name="star" size={14} color={colors.orange} />
                      <AntdIcons name="star" size={14} color={colors.orange} />
                      <AntdIcons name="star" size={14} color={colors.orange} />
                    ({averageRating}) Valoraciones
                </Text>
                  </View>
                </View>
              </View>

            </LinearGradient>
          </ImageBackground> : null
        }
        <View style={{flexDirection: 'row'}}>
        <View>
        <View style={{ margin: 10, width: dimensions.Width(88)}}>
          <CustomText light={colors.blue_main} dark={colors.white} numberOfLines={1} style={{ fontSize: 20, fontWeight: '500'}}>{item.title}</CustomText>
        </View>
        <View style={{ flexDirection: 'row', marginLeft: 10 }}>
          <CustomText light={colors.blue_main} dark={colors.rgb_235}>{item.number}€<CustomText light={colors.blue_main} dark={colors.rgb_235}>{item.currency}</CustomText></CustomText>
          <CustomText light={colors.blue_main} dark={colors.rgb_235} style={{ marginLeft: 20, fontWeight: '300', fontSize: dimensions.FontSize(12) }}><MaterialCommunityIcons name="map-pin" size={16} color={colors.green_main} /> {item.city}</CustomText>
        </View>
        </View>
        <View style={{ marginLeft: 'auto', marginRight: 10, marginTop: 10 }}>
          {item.anadidoFavorito ?
            <View>
              <AntdIcons name="heart" size={25} color={colors.ERROR} />
            </View>
            :
            <View>
              <AntdIcons name="hearto" size={25} color={colors.rgb_153} />
            </View>
          }
        </View>
        </View>

        <View style={{flexDirection: 'row', margin: 5}}>
        <View style={{ marginTop: 8 }}>
          <TouchableOpacity
              onPress={() => this.props.navigation.navigate('ProductDetails', { data: item })}
              style={{
                  width: dimensions.Width(46),
                  marginRight: 15,
                  height: 40,
                  marginLeft: 5,
                  backgroundColor: 'transparent',
                  borderRadius: 5,
                  borderWidth: 1,
                  borderColor: colors.green_main,
                  justifyContent: "center",
                  alignItems: "center",
              }}
              >
              <Text style={{ fontSize: 14, alignItems: "center", color: colors.green_main, justifyContent: "center", }}>VER DETALLES</Text>
          </TouchableOpacity>
      </View>
      <View style={{ marginTop: 8 }}>
          <TouchableOpacity
              onPress={ async () => {
                const id = await AsyncStorage.getItem('id');
                const { navigation } = this.props;
                const data = await navigation.getParam('data');
                if (id===null){
                    Alert.alert(
                        'Upps debes iniciar sesión',
                        'Para iniciar una conversación debes iniciar sesión o regístrarte',
                        [
                            {
                              text: 'Iniciar sesión', onPress: () => this.props.navigation.navigate('Login'),
                            },
                            { 
                              text: 'Regístrarme', onPress: () => this.props.navigation.navigate('Register') 
                            },
                        ],
                        { cancelable: false },
                    )
                    return null
        
                }if (id===item.creator.id){
                    Alert.alert(
                        'Upps álgo va mal',
                        'no puedes entablar una conversación con tigo mismo',
                        [
                            {
                              text: 'OK', onPress: () => console.log('ok'),
                            },
                        ],
                        { cancelable: false },
                    )
                    return null
        
                }else {
                navigation.navigate('Message', {data: item})  
                }     
            }}
              style={{
                  width: dimensions.Width(46),
                  marginRight: 5,
                  height: 40,
                  backgroundColor: colors.green_main,
                  borderRadius: 5,
                  borderWidth: 1,
                  borderColor: colors.green_main,
                  justifyContent: "center",
                  alignItems: "center",
              }}
              >
              <Text style={{ fontSize: 14, alignItems: "center", color: colors.white, justifyContent: "center", }}>CHAT</Text>
          </TouchableOpacity>
      </View>
        </View>
      </TouchableOpacity>
    )
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.setState({
        refreshing: false
      })
    }, 2000)
  }

  render() {
    const { navigation } = this.props;
    const data = navigation.getParam('data');
    return (
      <ViewCuston light={colors.white} dark={colors.black} containers={
      <View style={styles.container}>
            <View style={{paddingHorizontal: dimensions.Width(4)}}>
            <Header navigation={navigation} />
            </View>
            <View>
            <TextInput
              color={colors.rgb_153}
              inlineImageLeft='search_icon'
              keyboardAppearance='dark'
              returnKeyType='search'
              placeholder='¿Qué estás buscando?'
              clearButtonMode='always'
              onFocus={() => navigation.navigate('Search')}
              backgroundColor={'trasparent'}
              placeholderTextColor={colors.rgb_153}
              style={{
                height: 40, marginLeft: 10, marginRight: 10, borderRadius: 50, paddingLeft: 20, paddingRight: 20, color: colors.blue_main,
                borderColor: colors.rgb_153,
                borderWidth: 0.5
              }}
            />
          </View>
        <ScrollView showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >
          <Query query={CATEGORY_QUERY} variables={{ id: data.id, city: this.state.city }}>
            {(response, error, loading) => {
              if (loading) {
                return <ActivityIndicator size="large" color="#95ca3e" />
              }
              if (error) {
                return console.log('Response Error-------', error);
              }
              if (response) {
                return (
                  <View>
                    <View style={{ marginTop: dimensions.Height(4), marginLeft: 20, marginBottom: 30 }}>
                      <CustomText light={colors.blue_main} dark={colors.white} style={{fontWeight: '500', fontSize: dimensions.FontSize(24) }}>
                        {data.title} en {this.state.city}
                      </CustomText>
                      <View style={{ width: dimensions.Width(95), marginTop: 5 }}>
                        <CustomText style={{ color: colors.rgb_153 }}>{response && response.data ? response.data.getCategory.data.productos.length : ''}, resultado de la busqueda</CustomText>
                      </View>
                    </View>
                    <FlatList
                      keyExtractor={item => item.id}
                      data={response && response.data ? response.data.getCategory.data.productos : ''}
                      renderItem={(item) => this._renderItem(item)}
                      ListEmptyComponent={
                        <View style={{ alignSelf: 'center', padding: 20 }}>
                          <Image source={image.Vacia} style={{ width: dimensions.Width(34), alignSelf: 'center', height: dimensions.Height(13) }} />
                          <CustomText light={colors.blue_main} dark={colors.white} style={{ textAlign: 'center', fontSize: dimensions.FontSize(20), fontWeight: '200' }}>Aún no hay profesionales cerca de ti.</CustomText>
                        </View>
                      }
                    />
                  </View>
                )
              }
            }}
          </Query>
        </ScrollView>
      </View>}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    height: dimensions.Height(100)
  },
  headers: {
    height: dimensions.Height(18),
    backgroundColor: colors.white,
  },
  card: {
    width: dimensions.Width(90),
    height: dimensions.Height(16),
    borderRadius: 10,
    backgroundColor: colors.white,
    marginBottom: 30,
    flexDirection: 'row',
    alignSelf: 'center'
  },
  img: {
    width: dimensions.Width(22),
    height: dimensions.Height(10),
    backgroundColor: colors.white,
    borderRadius: 7,
    marginLeft: 'auto',
  },

  notify: {
    backgroundColor: colors.ERROR,
    borderRadius: 4,
    height: dimensions.Height(1),
    width: dimensions.Height(1),
    position: 'absolute',
    right: 15,
  },
})

export default Allproducts;