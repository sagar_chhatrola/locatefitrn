import React from 'react';
import { View, ScrollView, StyleSheet, RefreshControl, Text, FlatList, TouchableOpacity, Image, Alert, Share } from 'react-native';
import { colors, dimensions } from '../theme';
import { image } from './../constants/image';
import { CustomText } from '../components/CustomText';
import LoadingPlaceHolders from './placeholderloadinginspiration';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import Header from './../components/Header';
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import MaterialCommunityIcons from 'react-native-vector-icons/Feather';
import moment from 'moment';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';

const INSPIRATION_QUERY = gql`{
    getInspiration{
      id
      title
      image
      image1
      image2
      description
      created_at
      anadidoFavoritoinspiration
    }
  }
`;

export default class Favourites extends React.PureComponent {

    constructor() {
        super()
        this.state = {
            Loading: true,
            refreshing: false,
            data: null,
        }
    }
    componentDidMount() {
        setTimeout(() => {
            this.setState({
                Loading: false
            })
        }, 2000)
    }

    _onRefresh = () => {
        this.setState({ refreshing: true });
        setTimeout(() => {
            this.setState({
                refreshing: false
            })
        }, 2000)
    }
    onShare = async () => {
        try {
            const result = await Share.share({
                title: 'Locatefit',
                message:
                    'Locatefit | Los mejores profeisonales',

            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };


    renderItem = (item) => {
        const { navigation } = this.props;
        return (
            <View style={{ marginTop: dimensions.Height(2) }}>
                <TouchableOpacity style={styles.card} onPress={() => navigation.navigate('InspirationDetails', { data: item })}>
                    <Image source={{ uri: item.image }} style={{ width: dimensions.Width(100), height: 250, justifyContent: 'center', alignSelf: 'center' }} />
                    <View style={{borderBottomColor: colors.rgb_235, borderBottomWidth: 0.5, paddingBottom: 10}}>
                    <View style={{flexDirection: 'row'}}>
                      <CustomText light={colors.black} dark={colors.white} numberOfLines={1} style={{ width: dimensions.Width(70), fontSize: dimensions.FontSize(16), marginLeft: 10, fontWeight: '300', marginTop: 10}}>
                        {item.title}
                      </CustomText> 
                      <View style={{ marginLeft: 'auto', marginTop: 10 }}>
                                {item.anadidoFavoritoinspiration ?
                                    <View>
                                        <AntdIcons name="heart" size={25} color={colors.ERROR} />
                                    </View>
                                    :
                                    <View>
                                        <AntdIcons name="hearto" size={25} color={colors.rgb_153} />
                                    </View>
                                }
                            </View>
                    <TouchableOpacity style={{marginLeft: 10}} onPress={()=> this.onShare()}>
                      <MaterialCommunityIcons style={{marginTop: 10, marginLeft: 'auto', marginRight: 10 }} size={24} name="share" color={colors.rgb_153} />
                      </TouchableOpacity>     
                    </View>
                    <Text numberOfLines={1} style={{ width: dimensions.Width(70), fontSize: dimensions.FontSize(14), marginLeft: 10, fontWeight: '200', color: colors.rgb_153}}>
                    {moment(Number(item.created_at)).format('LL')}
                  </Text>
                  </View> 
                </TouchableOpacity>
            </View>
        )
    }


    render() {
        const { navigation } = this.props;
        return (
            <ViewCuston light={colors.white} dark={colors.black}containers={
            <View>
                <View style={styles.container} >
                    <Header navigation={navigation} />
                </View>

                {
                    this.state.Loading ?
                        <LoadingPlaceHolders />
                        :
                        <View>
                            <ScrollView
                                showsVerticalScrollIndicator={false}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this._onRefresh}
                                    />
                                }
                            >
                                <CustomText light={colors.black} dark={colors.white} style={{ fontWeight: '500', fontSize: dimensions.FontSize(34), fontFamily: 'Helvetica', marginLeft: 15, marginBottom: dimensions.Height(3)}}>Inspiraciones</CustomText>
                                <View style={{marginBottom: dimensions.Height(50)}}>
                                <Query query={INSPIRATION_QUERY}>
                                    {(response, error, loading) => {
                                        if (loading) {
                                            return <LoadingPlaceHolders />
                                        }
                                        if (error) {
                                            return console.log('Response Error-------', error);
                                        }
                                        if (response) {
                                            return (<FlatList
                                                data={response && response.data ? response.data.getInspiration : ''}
                                                horizontal={false}
                                                keyExtractor={(item) => item.id}
                                                showsHorizontalScrollIndicator={false}
                                                renderItem={({ item }) => this.renderItem(item)}
                                                ListEmptyComponent={
                                                    <View style={{ alignSelf: 'center', padding: 20}}>
                                                      <Image source={image.Vacia} style={{width: dimensions.Width(34), alignSelf: 'center', height: dimensions.Height(13)}} />
                                                      <CustomText light={colors.blue_main} dark={colors.white} style={{textAlign: 'center', fontSize: dimensions.FontSize(20),  fontWeight: '200'}}>Aún no tenemos inspiración para hoy</CustomText>
                                                  </View>
                                                  }
                                            />);
                                        }
                                    }}
                                </Query>
                            </View>
                            </ScrollView>
                        </View>
                }
            </View>}
            />
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        paddingHorizontal: dimensions.Width(4),
       

    },

});