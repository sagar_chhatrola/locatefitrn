import React from 'react';
import { View, ScrollView, StyleSheet, RefreshControl, Image, Text, TouchableOpacity } from 'react-native';
import { colors, dimensions } from '../theme';
import { CustomText } from '../components/CustomText';
import LoadingPlaceHolders from './placeholderloadingmy';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import Header from './../components/Header';
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { NETWORK_INTERFACE_LINK_AVATAR } from './../constants/config';
import { image } from '../constants/image'
import Link from './../components/Link4';
import { ActivityIndicator } from '@ant-design/react-native';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';

const PRODUCTS_QUERY = gql`
  {
    getProductos {
      success
      message
      list {
        id
                  city
                  category_id
                  currency
                  description
                  domingo
                  domingo_from
                  domingo_to
                  jueves
                  jueves_from
                  jueves_to
                  lunes
                  lunes_from
                  lunes_to
                  martes
                  martes_from
                  martes_to
                  miercoles
                  miercoles_from
                  miercoles_to
                  number
                  sabado
                  sabado_from
                  sabado_to
                  time
                  title
                  visitas
                  viernes
                  viernes_from
                  viernes_to
                  anadidoFavorito
                  fileList
                  created_by
                  category {
                    id
                    title
                  }
                  ordenes{
                    id
                       }
                  professionalRating {
                    id
                    coment
                    rate
                    updated_at
                    customer {
                      foto_del_perfil
                      nombre
                      apellidos
                      profesion
                      ciudad
                    }
                  }
                  creator {
                    id
                    usuario
                    email
                    nombre
                    UserID
                    apellidos
                    ciudad
                    telefono
                    tipo
                    foto_del_perfil
                    fotos_tu_dni
                    profesion
                    descripcion
                    fecha_de_nacimiento
                    notificacion
                    grado
                    estudios
                    formularios_de_impuesto
                    fb_enlazar
                    twitter_enlazar
                    instagram_enlazar
                    youtube_enlazar
                    propia_web_enlazar
                  }
            }
     }
  }
`;


const ELIMINAR_PRODUCT = gql`
  mutation eliminarProducto($id: ID!) {
    eliminarProducto(id: $id) {
      success
      message
    }
  }
`;


export default class Favourites extends React.Component {

    constructor() {
        super()
        this.state = {
            Loading: true,
            refreshing: false
        }
    }
    componentDidMount() {
        setTimeout(() => {
            this.setState({
                Loading: false
            })
        }, 200)
    }

    _onRefresh = () => {
        this.setState({ refreshing: true });
        setTimeout(() => {
            this.setState({
                refreshing: false
            })
        }, 2000)
    }

    renderArticles = () => {
        const { navigation, refetch } = this.props;
        return (
            <ViewCuston light={colors.light_white} dark={colors.black} containers={
            <View style={styles.container}>
                <Header navigation={navigation} />
                <View style={{ marginBottom: 25, marginTop: 5 }}>
                <CustomText light={colors.blue_main} dark={colors.white} style={{ fontSize: 20, fontWeight: '500',  marginBottom: 10 }}>
                        <AntdIcons name="notification" style={{ marginLeft: 'auto' }} size={20} color={colors.green_main} />  Mis anuncios
                        </CustomText>
                    <CustomText style={{ color: colors.rgb_153, fontWeight: '300', fontSize: dimensions.FontSize(14) }}>
                        Aquí podrás gestionar los servicios que ya tienes y destacarlos para venderlos antes.
                     </CustomText>
                </View>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        refreshControl={ <RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh} />}>

                        <Query query={PRODUCTS_QUERY}>
                            {({ loading, error, data, refetch }) => {
                                this.refetch = refetch;
                                if (loading) {
                                    return <LoadingPlaceHolders />
                                }
                                else {
                                    let response = { data: data }
                                    const item = response && response.data && response.data.getProductos ? response.data.getProductos.list : ''
                                    return (
                                        <View style={{ marginBottom: dimensions.Height(60), flexDirection: 'row', width: dimensions.Width(92), height: 'auto', flexWrap: 'wrap', justifyContent: 'center', marginTop: 30 }}>
                                            {
                                                item.map(
                                                    (pro, i) => (
                                                        <ViewCuston light={colors.white} dark={colors.back_dark} style={styles.cardMayadds} key={i} containers={
                                                        <View>
                                                            <Image source={pro.fileList.length > 0 ?
                                                                { uri: pro.fileList.length > 0 ? NETWORK_INTERFACE_LINK_AVATAR + pro.fileList[0] : "" } : null} style={{ width: dimensions.Width(41), height: dimensions.Height(15), borderTopLeftRadius: 12, borderTopRightRadius: 12 }} />
                                                            <View style={{ margin: 10 }}>
                                                            <CustomText light={colors.blue_main} dark={colors.white} style={{ fontSize: dimensions.FontSize(16), fontWeight: 'bold' }}>{pro.number}€ {pro.currency}</CustomText>
                                                            <CustomText light={colors.blue_main} dark={colors.white}  numberOfLines={2} style={{ fontSize: dimensions.FontSize(14), fontWeight: '300', marginTop: 5 }}>{pro.title}</CustomText>
                                                            </View>
                                                            <View style={{ alignSelf: 'center', marginTop: 10 }}>
                                                                <TouchableOpacity onPress={() => navigation.navigate('ProductDetails', { data: pro })} style={{ borderWidth: 1, borderColor: colors.green_main, padding: 10, borderRadius: 50 }}><Text style={{ color: colors.green_main }}>Ver detalles</Text></TouchableOpacity>
                                                            </View>
                                                        </View> 
                                                            }
                                                        />

                                                    )
                                                )

                                            }
                                            {
                                                (item.length === 0 || !item) ?
                                                    <View style={{ alignSelf: 'center', padding: 20 }}>
                                                        <Image source={image.Vacia} style={{ width: dimensions.Width(34), alignSelf: 'center', height: dimensions.Height(13) }} />
                                                        <Text style={{ textAlign: 'center', fontSize: dimensions.FontSize(20), color: colors.blue_main, fontWeight: '200' }}>Aún no has publicado ningún servicio</Text>

                                                        <View style={{ flexDirection: 'row', height: dimensions.Height(5), backgroundColor: colors.green_main, borderRadius: 30, marginBottom: 20, marginTop: 30, justifyContent: 'center', alignItems: 'center' }}>
                                                            <AntdIcons name="pluscircleo" style={{ marginLeft: 10 }} size={14} color={colors.white} />
                                                            <View style={{ marginLeft: 10 }}><Link url="https://locatefit.es/profile" text="Publicar un servicio" /></View>
                                                        </View>

                                                    </View> : null

                                            }
                                        </View>
                                    )
                                }
                            }}
                        </Query>
                     </ScrollView>
            </View>}
            />
        )
    }

    render() {
        return (
            <View>
                {this.renderArticles()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        paddingHorizontal: dimensions.Width(4),
        height: dimensions.Height(100)

    },

    cardMayadds: {
        width: dimensions.Width(41),
        height: 280,
        margin: dimensions.Width(2),
        borderRadius: 12,
    }

});