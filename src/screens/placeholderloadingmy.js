import React from "react";
import { SafeAreaView, View } from "react-native";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { dimensions } from './../theme';
import { useDynamicValue } from 'react-native-dark-mode'
import { colors } from '../theme/colors';


export default function LoadingMy(){
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark)
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030')
  return(
    <SafeAreaView style={{alignSelf: 'center', flexDirection: 'row', flexWrap: 'wrap'}}>
      <SkeletonPlaceholder backgroundColor={placeholderColor} highlightColor={placeholderColor1}>
      <View style={{marginBottom: 30, marginRight: 20}}>
      <View style={{ width: dimensions.Width(41), height: 150, borderTopEndRadius: 10, borderTopStartRadius: 10}} />
      <View style={{flexDirection: 'row'}}>
        <View>
          <View style={{ width: dimensions.Width(35), height: 15, borderRadius: 3, marginTop: 15}} />
          <View style={{ width: dimensions.Width(25), height: 15, borderRadius: 3, marginTop: 15}} />
        </View>
      </View>
      <View style={{ width: dimensions.Width(30), height: 45, borderRadius: 30, marginTop: 15, alignSelf: 'center'}} />
      </View>
      
      <View style={{marginBottom: 30}}>
      <View style={{ width: dimensions.Width(40), height: 150, borderTopEndRadius: 10, borderTopStartRadius: 10}} />
      <View style={{flexDirection: 'row'}}>
        <View>
          <View style={{ width: dimensions.Width(35), height: 15, borderRadius: 3, marginTop: 15}} />
          <View style={{ width: dimensions.Width(25), height: 15, borderRadius: 3, marginTop: 15}} />
        </View>
      </View>
      <View style={{ width: dimensions.Width(30), height: 45, borderRadius: 30, marginTop: 15, alignSelf: 'center'}} />
      </View>      
      </SkeletonPlaceholder>
    </SafeAreaView>
  )
}
