import React from "react";
import { SafeAreaView, View } from "react-native";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { dimensions } from './../theme';
import { useDynamicValue } from 'react-native-dark-mode'
import { colors } from '../theme/colors';
 

export default function LoadingOr(){
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark)
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030')
  return(
    <SafeAreaView style={{alignSelf: 'center'}}>
      <SkeletonPlaceholder backgroundColor={placeholderColor} highlightColor={placeholderColor1}>
      <View style={{flexDirection: 'row', width: dimensions.Width(90), marginBottom: 40 }}>
        <View>
          <View style={{ width: dimensions.Width(60), height: 20, borderRadius: 3}} />
          <View style={{width: dimensions.Width(50), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(55), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(48), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{flexDirection: 'row'}}>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
        </View>
        </View>
        <View style={{marginLeft: 'auto' }}>
          <View style={{ width: 100, height: 100, borderRadius: 10}} />
        </View>
      </View>

      <View style={{flexDirection: 'row', width: dimensions.Width(90), marginBottom: 40 }}>
        <View>
          <View style={{ width: dimensions.Width(60), height: 20, borderRadius: 3}} />
          <View style={{width: dimensions.Width(50), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(55), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(48), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{flexDirection: 'row'}}>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
        </View>
        </View>
        <View style={{marginLeft: 'auto' }}>
          <View style={{ width: 100, height: 100, borderRadius: 10}} />
        </View>
      </View>

      <View style={{flexDirection: 'row', width: dimensions.Width(90), marginBottom: 40 }}>
        <View>
          <View style={{ width: dimensions.Width(60), height: 20, borderRadius: 3}} />
          <View style={{width: dimensions.Width(50), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(55), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(48), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{flexDirection: 'row'}}>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
        </View>
        </View>
        <View style={{marginLeft: 'auto' }}>
          <View style={{ width: 100, height: 100, borderRadius: 10}} />
        </View>
      </View>

      <View style={{flexDirection: 'row', width: dimensions.Width(90), marginBottom: 40 }}>
        <View>
          <View style={{ width: dimensions.Width(60), height: 20, borderRadius: 3}} />
          <View style={{width: dimensions.Width(50), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(55), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(48), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{flexDirection: 'row'}}>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
        </View>
        </View>
        <View style={{marginLeft: 'auto' }}>
          <View style={{ width: 100, height: 100, borderRadius: 10}} />
        </View>
      </View>

      <View style={{flexDirection: 'row', width: dimensions.Width(90), marginBottom: 40 }}>
        <View>
          <View style={{ width: dimensions.Width(60), height: 20, borderRadius: 3}} />
          <View style={{width: dimensions.Width(50), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(55), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(48), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{flexDirection: 'row'}}>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
        </View>
        </View>
        <View style={{marginLeft: 'auto' }}>
          <View style={{ width: 100, height: 100, borderRadius: 10}} />
        </View>
      </View>

      <View style={{flexDirection: 'row', width: dimensions.Width(90), marginBottom: 40 }}>
        <View>
          <View style={{ width: dimensions.Width(60), height: 20, borderRadius: 3}} />
          <View style={{width: dimensions.Width(50), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(55), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(48), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{flexDirection: 'row'}}>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
        </View>
        </View>
        <View style={{marginLeft: 'auto' }}>
          <View style={{ width: 100, height: 100, borderRadius: 10}} />
        </View>
      </View>

      <View style={{flexDirection: 'row', width: dimensions.Width(90), marginBottom: 40 }}>
        <View>
          <View style={{ width: dimensions.Width(60), height: 20, borderRadius: 3}} />
          <View style={{width: dimensions.Width(50), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(55), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(48), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{flexDirection: 'row'}}>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
        </View>
        </View>
        <View style={{marginLeft: 'auto' }}>
          <View style={{ width: 100, height: 100, borderRadius: 10}} />
        </View>
      </View>

      <View style={{flexDirection: 'row', width: dimensions.Width(90), marginBottom: 40 }}>
        <View>
          <View style={{ width: dimensions.Width(60), height: 20, borderRadius: 3}} />
          <View style={{width: dimensions.Width(50), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(55), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{width: dimensions.Width(48), height: 5, marginTop: 12, borderRadius: 3}}/>
          <View style={{flexDirection: 'row'}}>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
            <View style={{width: dimensions.Width(10), height: 10, marginTop: 12, marginRight: 20, borderRadius: 3}}/>
        </View>
        </View>
        <View style={{marginLeft: 'auto' }}>
          <View style={{ width: 100, height: 100, borderRadius: 10}} />
        </View>
      </View>
      </SkeletonPlaceholder>
    </SafeAreaView>
  )
}
