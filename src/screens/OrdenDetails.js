import React from 'react';
import { StyleSheet, View, Image, Alert,} from 'react-native';
import { colors, dimensions } from '../theme';
import {Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { NETWORK_INTERFACE_LINK_AVATAR, NETWORK_INTERFACE_LINK} from '../constants/config';
import { CustomText } from '../components/CustomText';
import { ScrollView } from 'react-native-gesture-handler';
import OpenURLButton from './../components/LinkOut';
import moment from 'moment';
import { Button } from './../components/Button';
import { withApollo } from 'react-apollo';
import Header from './../components/Header';
import Toast from 'react-native-simple-toast';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';


const PROFESSIONAL_ORDEN_PROCEED = gql`
  mutation ordenProceed($ordenId: ID!, $estado: String!, $progreso: String!, $status: String!, $nota: String) {
    ordenProceed(ordenId: $ordenId, estado: $estado, progreso: $progreso, status: $status, nota: $nota) {
      success
      message
    }
  }
`;

const CREATE_NOTIFICATION = gql`
  mutation createNotification($input: NotificationInput) {
    createNotification(input: $input) {
      success
      message
    }
  }
`;

const USER_DETAIL = gql`
  query getUsuario($id: ID!) {
    getUsuario(id: $id) {
      email
      nombre
      apellidos
      ciudad
      telefono
      foto_del_perfil
      fotos_tu_dni
      profesion
      descripcion
      fecha_de_nacimiento
      notificacion
      grado
      estudios
      formularios_de_impuesto
      fb_enlazar
      twitter_enlazar
      instagram_enlazar
      youtube_enlazar
      propia_web_enlazar
      UserID
    }
  }
`;

class OrdenDetails extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null,
            dataLoading: true,
            recipient: '',
            useridpush: '',
            messageTexte: 'Locatefit S.L., tu profeisonal de Locatefit a aceptado la orden contratada',
            messageTexteFinish: 'Locatefit S.L., La orden de Locatefit ha sido finalizada con éxito',
            messageTexteRechazar: 'Locatefit S.L., el profesional ha rechazado la orden lamentamos los incovenientes.',
        };
      }

      async componentDidMount(){
        const { navigation } = this.props;
         const data = await navigation.getParam('data');
         console.log(data)
        this.setState({
            recipient: data.client.telefono,
            data
          });
          this.props.client
            .query({
                    query: USER_DETAIL,
                    variables: { id: this.state.data.cliente }
                    })
                .then(async (results)=> {
                    const { loading, error, data } = results
                    if (data) {
                        this.setState({
                            useridpush: data && data.getUsuario ? data.getUsuario.UserID : ''
                        })
                    }
                    console.log(this.state.useridpush)
                })
            }


    refetch = null;

    SendTextSMS = () => {
        fetch(`${NETWORK_INTERFACE_LINK}/send-message?recipient=${this.state.recipient}&textmessage=${this.state.messageTexte}`)
          .catch(err => console.log(err))
      };

      SendTextSMSFinish = () => {
        fetch(`${NETWORK_INTERFACE_LINK}/send-message?recipient=${this.state.recipient}&textmessage=${this.state.messageTexteFinish}`)
          .catch(err => console.log(err))
      };

      SendTextSMSRechazar = () => {
        fetch(`${NETWORK_INTERFACE_LINK}/send-message?recipient=${this.state.recipient}&textmessage=${this.state.messageTexteRechazar}`)
          .catch(err => console.log(err))
          console.log('sending', this.state.messageTexteRechazar, this.state.recipient)
      };

      /////////Send Push Notification

      SendPushNotificationAceptar = () => {
        fetch(`${NETWORK_INTERFACE_LINK}/send-push-notification?IdOnesignal=${this.state.useridpush}&textmessage=${this.state.messageTexte}`)
          .catch(err => console.log(err))

          console.log(this.state.useridpush, this.state.messageTexte)
      };

      SendPushNotificationFinish = () => {
        fetch(`${NETWORK_INTERFACE_LINK}/send-push-notification?IdOnesignal=${this.state.useridpush}&textmessage=${this.state.messageTexteFinish}`)
          .catch(err => console.log(err))
          console.log(this.state.useridpush, this.state.messageTexteFinish)
      };

      SendPushNotificationRechazar = () => {
        fetch(`${NETWORK_INTERFACE_LINK}/send-push-notification?IdOnesignal=${this.state.useridpush}&textmessage=${this.state.messageTexteRechazar}`)
          .catch(err => console.log(err))
          console.log(this.state.useridpush, this.state.messageTexteRechazar)
      };

    
     handleAcepta(e, ordenProceed, data) {
        this.setState();
        const formData = {
            ordenId: data.id,
            estado: 'Aceptado',
            progreso: '75',
            status: 'active',
        }
         ordenProceed({ variables: formData }).then(async res => {
            if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
                Toast.showWithGravity('Orden aceptada éxitosamente', Toast.LONG, Toast.TOP)
                console.log(res && res.data && res.data.ordenProceed && res.data.ordenProceed.success)
                this.setState(prevState => ({
                    data: { ...prevState.data, estado: 'Aceptado' }
                }))
                const Notificacion = {
                    orden: data.id,
                    user: data.cliente,
                    cliente: data.cliente,
                    profesional: data.profesional,
                    type:'accept_order'
                }
                this.props.client
                        .mutate({
                          mutation: CREATE_NOTIFICATION,
                          variables: {input: Notificacion}
                        })
                        .then(async (results)=> {
                          console.log("results",results)
                        }).catch(err=>{
                          console.log("err",err)
                        })
                        this.SendTextSMS()
                        this.SendPushNotificationAceptar()
                if (this.refetch) {
                    this.refetch().then(res => {
                        if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
                            this.setState({ ordenes: res.data.getOrdenesByProfessional.list });
                        }
                    });
                }
            }
        }).catch(err => {
            console.log('Algo salió mal. Por favor intente nuevamente en un momento.', err)
        })
    }

    handleCancelar(e, ordenProceed, data) {
        this.setState();
        const formData = {
            ordenId: data.id,
            estado: 'Rechazado',
            progreso: '0',
            status: 'exception',
        }
         ordenProceed({ variables: formData }).then(async res => {
            if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
                Toast.showWithGravity('Orden rechazada éxitosamente', Toast.LONG, Toast.TOP)
                this.setState(prevState => ({
                    data: { ...prevState.data, estado: 'Rechazado' }
                }))
                const Notificacion = {
                    orden: data.id,
                    user: data.cliente,
                    cliente: data.cliente,
                    profesional: data.profesional,
                    type:'reject_order'
                }
                this.props.client.mutate({
                        mutation: CREATE_NOTIFICATION,
                        variables: {input: Notificacion}
                    })
                    .then(async (results)=> {
                        console.log("results",results)
                    }).catch(err=>{
                        console.log("err",err)
                    })
                    this.SendPushNotificationRechazar()
                    this.SendTextSMSRechazar()
                console.log(res.data.ordenProceed.message);
                if (this.refetch) {
                    this.refetch().then(res => {
                        if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
                            this.setState({ ordenes: res.data.getOrdenesByProfessional.list });
                        }
                    });
                }
            }
        }).catch(err => {
            console.log(err)
            console.log('Algo salió mal. Por favor intente nuevamente en un momento.')
        })
    }

      

    finalizar(e, ordenProceed, data) {
        this.setState();
        const formData = {
            ordenId: data.id,
            estado: 'Finalizada',
            progreso: '100',
            status: 'success',
        }
         ordenProceed({ variables: formData }).then(async res => {
            if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
                Toast.showWithGravity('Orden Finalizada éxitosamente', Toast.LONG, Toast.TOP)
                this.setState(prevState => ({
                    data: { ...prevState.data, estado: 'Finalizada' }
                }))
                this.setState({formLoading: false });
                const Notificacion = {
                    orden: data.id,
                    user:  data.cliente,
                    cliente: data.cliente,
                    profesional: data.profesional,
                    type:'finish_order'
                }
                this.props.client
                .mutate({
                    mutation: CREATE_NOTIFICATION,
                    variables: {input: Notificacion}
                    })
                    .then(async (results)=> {
                    console.log("results",results)
                    }).catch(err=>{
                    console.log("err",err)
                    })
                    this.SendPushNotificationFinish()
                    this.SendTextSMSFinish()
                console.log('La orden ha sido marcada como finalizada éxitosamente');
                if (this.refetch) {
                    this.refetch().then(res => {
                        if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
                            this.setState({ ordenes: res.data.getOrdenesByProfessional.list });
                        }
                    });
                }
            }
        }).catch(err => {
            this.setState({formLoading: false });
            console.log(err)
        })
    }

   

    _renderItem(item) {
        const { navigation } = this.props;
        const description = item.nota;
        const ubicacion = item.client.ciudad;
        const titulo = item.product.title;
        const date = new Date(item.endDate + ' ' + item.time);
        const year = date.getFullYear();
        // const year = date.getUTCFullYear();
        let month = date.getMonth() + 1;
        // let month = date.getUTCMonth() + 1;
        month = (month < 10) ? '0' + month : month;
        const day = date.getDate();
        // const day = date.getUTCDate();
        let hours = date.getHours();
        // let hours = date.getUTCHours();
        hours = (hours < 10) ? '0' + hours : hours;
        const minutes = date.getMinutes();
        // const minutes = date.getUTCMinutes();

        const startDateString = year + '' + month + '0' + day + 'T' + hours + '' + minutes + '000';

        const productoTime = item.product.time.split(':');
        let endHours = Number(hours) + (Number(productoTime[0]) * Number(item.cantidad));
        let endMinutes = Number(minutes) + (Number(productoTime[1]) * Number(item.cantidad));
        if (endMinutes > 60) {
            endHours++;
            endMinutes -= 60;
        }

        endHours = (endHours < 10) ? '0' + endHours : endHours;
        endMinutes = (endMinutes < 10) ? '0' + endMinutes : endMinutes;
        const endDateString = year + '' + month + '0' + day + 'T' + endHours + '' + endMinutes + '00';

        const link = "https://calendar.google.com/calendar/r/eventedit?text=" + titulo + "&dates=" + startDateString + "/" + endDateString + "&details=" + description + "&location=" + ubicacion + "&sprop=name:Locatefit&sf=true";
        
        return (
            <ViewCuston light={colors.white} dark={colors.black} containers={
            <View style={styles.container}>
            <View style={{paddingHorizontal:dimensions.Height(2),}}>
            <Header navigation={navigation} />
            </View>
            
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View>
                        {item.product.fileList.length > 0 ?
                            <Image source={{ uri: item.product.fileList.length > 0 ? NETWORK_INTERFACE_LINK_AVATAR + item.product.fileList[0] : "" }} style={{ width: dimensions.Width(100), height: dimensions.Height(30) }} /> : null}
                    </View>
                    <View style={{ width: dimensions.Width(40), height: dimensions.Height(5), backgroundColor: colors.white, borderRadius: 30, position: 'absolute', shadowColor: 'black', shadowOffset: { width: 0, height: 2 }, shadowRadius: 6, shadowOpacity: 0.2, elevation: 3, marginTop: dimensions.Height(27), marginLeft: 20 }}>
                    <CustomText style={{ color: colors.blue_main, alignSelf: 'center', marginTop: dimensions.Height(1.3), fontWeight: '200', fontSize: dimensions.FontSize(16) }}>
                        {item.estado}
                    </CustomText>
                </View>
                    <View style={{ width: dimensions.Width(95), borderTopWidth: 0.5, borderBottomWidth: 0.5, marginTop: 40, borderTopColor: colors.light_grey, borderBottomColor: colors.light_grey, height: dimensions.Height(8), alignSelf: 'center',  }}>
                        <CustomText  light={colors.blue_main} dark={colors.white} style={{fontSize: dimensions.FontSize(18), fontWeight: '300', marginTop: 5}}>
                            {item.product.title}
                        </CustomText>
                        <CustomText style={{ color: colors.rgb_153, fontSize: dimensions.FontSize(14), fontWeight: '300', marginTop: 5}}>
                            Cantidad: {item.cantidad} {item.product.currency}
                        </CustomText>
                    </View>
                    <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{marginTop: dimensions.Height(3)}} containers={
                    <View style={{width: dimensions.Width(100), height: 'auto'}}>
                        <CustomText style={{padding: 10, color: colors.grey, fontSize: dimensions.FontSize(18)}}>
                            Fecha del realización del servicio:
                        </CustomText>
                    </View>}
                    />
                    <View style={{marginTop: 20, paddingHorizontal: dimensions.Width(4)}}>
                        <CustomText  light={colors.black} dark={colors.white} style={{fontWeight: '200', fontSize: dimensions.FontSize(16)}}>
                            {item.endDate} a las {item.time}
                        </CustomText>
                        <View style={{marginTop: dimensions.Height(2), alignSelf: 'center'}}>
                            <View style={styles.buttonView}>
                                <OpenURLButton url={link} containerStyle={styles.buttonView} text='AÑADIR A GOOGLE CALENDAR'  />
                            </View>
                        </View>
                    </View>
                    <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{marginTop: dimensions.Height(3)}} containers={
                    <View style={{width: dimensions.Width(100), height: 'auto'}}>
                        <CustomText style={{padding: 10, color: colors.grey, fontSize: dimensions.FontSize(18)}}>
                            Informacón del cliente:
                        </CustomText>
                    </View>
                        }
                        />
                    <View style={{marginLeft: 15, marginTop: 20}}>
                    <CustomText light={colors.blue_main} dark={colors.white} style={{fontSize: dimensions.FontSize(18)}}>
                        {item.client.nombre}
                    </CustomText>
                    <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(16), fontWeight: '200', marginTop: 10}}>
                        {item.client.calle}
                    </CustomText>
                    <View style={{flexDirection: 'row'}}>
                    <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(16), fontWeight: '200', marginTop: 5}}>
                        {item.client.ciudad},
                    </CustomText>
                    <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(16), fontWeight: '200', marginTop: 5, marginLeft: 10}}>
                        {item.client.provincia}
                    </CustomText>
                    </View>
                    <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(16), fontWeight: '200', marginTop: 5}}>
                        {item.client.codigopostal}
                    </CustomText>
                    <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(16), fontWeight: '200', marginTop: 5}}>
                        {item.client.telefono}
                    </CustomText>
                </View>
                <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{marginTop: dimensions.Height(3)}} containers={
                    <View style={{width: dimensions.Width(100), height: 'auto'}}>
                        <CustomText style={{padding: 10, color: colors.grey, fontSize: dimensions.FontSize(18)}}>
                            Detalles del Pedido:
                        </CustomText>
                    </View>}
                    />
                    <View style={{marginLeft: 10}}>
                    <View style={{flexDirection: 'row', marginTop: 20}}>
                    <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(18), fontWeight: '200'}}>
                        Nº Pedido:
                    </CustomText>
                    <CustomText light={colors.black} dark={colors.white} style={{marginLeft: 'auto', marginRight: 15, fontWeight: '200'}}>
                        {item.id}
                    </CustomText>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 20}}>
                    <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(18), fontWeight: '200'}}>
                        Fecha:
                    </CustomText>
                    <CustomText light={colors.black} dark={colors.white} style={{marginLeft: 'auto', marginRight: 15, fontWeight: '200'}}>
                    {moment(Number(item.created_at)).format('LL')}
                    </CustomText>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 20}}>
                    <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(18), fontWeight: '200'}}>
                        Total:
                    </CustomText>
                    <CustomText light={colors.black} dark={colors.white} style={{marginLeft: 'auto', marginRight: 15, fontWeight: '200'}}>
                        {item.cantidad * item.product.number}€
                    </CustomText>
                </View>
                </View>
                <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{marginTop: dimensions.Height(3)}} containers={
                <View style={{width: dimensions.Width(100), height: 'auto'}}>
                    <CustomText style={{padding: 10, color: colors.grey, fontSize: dimensions.FontSize(18)}}>
                        Descripción de la tarea:
                    </CustomText>
                    </View>}
                    />
                    <CustomText light={colors.black} dark={colors.white} style={{marginRight: 15, fontWeight: '200', marginLeft: 15, marginTop: 15}}>
                        {item.nota}
                    </CustomText>
                    <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{marginTop: dimensions.Height(3)}} containers={
                    <View style={{width: dimensions.Width(100), height: 'auto'}}>
                        <CustomText style={{padding: 10, color: colors.grey, fontSize: dimensions.FontSize(18)}}>
                            Acción:
                        </CustomText>
                    </View>}
                    />
                        {
                            item.stado ? 
                            <View style={{paddingHorizontal: dimensions.Width(4), alignSelf: 'center', marginBottom: 40}}>
                            
                            </View> : null
                        }

                        {
                            (item.estado === 'Nueva' || !item.estado)?
                            <View style={{paddingHorizontal: dimensions.Width(4), alignSelf: 'center', marginBottom: 40}}>   
                            <View style={{marginBottom: 15, marginTop: 15}}>
                            <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
                                {(ordenProceed) => (
                                <View style={styles.buttonView}>
                                <Button light={colors.white} dark={colors.white} onPress={e => this.handleAcepta(e, ordenProceed, item, item.refetch)} titleStyle={styles.buttonTitle} title="ACEPTAR ORDEN"/>
                                </View>)}
                            </Mutation>
                            </View>
                            <View style={{marginBottom: 15,}}>
                            <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
                            {(ordenProceed) => (
                                <View style={styles.buttonView1}>
                                <Button light={colors.white} dark={colors.white} onPress={ () => {
                                    Alert.alert(
                                        '¿Estas seguro que deseas rechazar la orden?',
                                        'Lamentamos que no haya podido cumplir con esta orden',
                                        [
                                          { text: 'Rechazar', onPress: (e) => this.handleCancelar(e, ordenProceed, item, item.refetch) },
                                          {text: 'Aceptar', onPress: (e) => this.handleAcepta(e, ordenProceed, item, item.refetch)}
                                        ],
                                        { cancelable: false },
                                      )
                                }}
                                titleStyle={styles.buttonTitle} title="RECHAZAR ORDEN"/>
                                </View>)}
                            </Mutation> 
                        </View> 
                        </View> : null
                        }
                        {
                            (item.estado === 'Aceptado' || !item.estado)?
                            <View style={{paddingHorizontal: dimensions.Width(4), alignSelf: 'center', marginBottom: 40}}> 
                                <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
                                    {(ordenProceed) => (
                                    <View style={styles.buttonView2}>
                                    <Button light={colors.white} dark={colors.white} onPress={e => this.finalizar(e, ordenProceed, item)} titleStyle={styles.buttonTitle} title="FINALIZAR ORDEN"/>
                                    </View>)}
                                </Mutation>
                            </View> : null
                        }
                        
                    
                </ScrollView>
            </View>}
            />
        )
    }
    render() {

        const { data } = this.state;
        if (data) {

            return this._renderItem(data);
        }
        return null;
    }
}
 
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        height: dimensions.Height(100)
    },
    buttonView:{
        backgroundColor:colors.light_sky,
        width:dimensions.Width(84),
        borderRadius:dimensions.Width(8),
      },
      buttonView1:{
        backgroundColor:colors.ERROR,
        width:dimensions.Width(84),
        borderRadius:dimensions.Width(8),
      },
      buttonView2:{
        backgroundColor:colors.green_main,
        width:dimensions.Width(84),
        borderRadius:dimensions.Width(8),
      },
      buttonTitle:{
        alignSelf:'center',
        paddingVertical:dimensions.Height(2),
        paddingHorizontal:dimensions.Width(5),
        color:colors.white,
        fontWeight:'bold',
        fontSize:dimensions.FontSize(17),
      }

});

export default withApollo(OrdenDetails);