import React from 'react';
import { View, StyleSheet, Alert, TextInput, Image, SafeAreaView, Modal, Text, TouchableHighlight, TouchableOpacity, Platform } from 'react-native'
import { colors, dimensions } from './../theme';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { CustomText } from './../components/CustomText';
import Icons from 'react-native-vector-icons/FontAwesome';
import { Button } from './../components/Button';
import { ScrollView } from 'react-native-gesture-handler';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { image } from '../constants/image';
import AsyncStorage from '@react-native-community/async-storage';
import { NETWORK_INTERFACE_LINK_AVATAR, NETWORK_INTERFACE_LINK } from '../constants/config';
import { Avatar } from 'react-native-elements';
import { Badge } from '@ant-design/react-native';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import moment, { now } from "moment";
import 'moment/locale/es';
import _get from "lodash.get";
import { withApollo } from 'react-apollo';
import { ListItem } from 'react-native-elements'
import { TextareaItem, Stepper } from '@ant-design/react-native';
import Adress from './Addresspayment';
import TimePicker from "react-native-24h-timepicker";
import Header from './../components/Header';
import Iconsverifiel from 'react-native-vector-icons/Octicons';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';
import { CustomTextInput } from './../components/CustomTextInput';



LocaleConfig.locales['es'] = {
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene.', 'Feb.', 'Mar', 'Abr', 'May', 'Jun', 'Jul.', 'Ago', 'Sept.', 'Oct.', 'Nov.', 'Dic.'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom.', 'Lun.', 'Mar.', 'Mie.', 'Jue.', 'Vie.', 'Sab.'],
    today: 'Hoy\'Hoy'
};
LocaleConfig.defaultLocale = 'es';

const CREATE_NOTIFICATION = gql`
  mutation createNotification($input: NotificationInput) {
    createNotification(input: $input) {
      success
      message
    }
  }
`;

const CREAR_MODIFICAR_ORDEN = gql`
  mutation crearModificarOrden($input: OrdenCreationInput) {
    crearModificarOrden(input: $input) {
      id
      cupon
      nota
      aceptaTerminos
      direccion {
        id
        nombre
        calle
        ciudad
        provincia
        codigopostal
        telefono
        usuario_id
      }
      producto
      cantidad
      cliente
      profesional
      descuento {
        clave
        descuento
        tipo
      }
    }
  }
`;

const USUARIO_DIRECCION_QUERY = gql`
    query{
        getUsuarioDireccion {
        success
        message
        data {
            id
            nombre
            calle
            ciudad
            provincia
            codigopostal
            telefono
            }
          }
        }
    `;

const ELIMINAR_USUARIO_DIRECCION = gql`
mutation eliminarUsuarioDireccion($id: ID!) {
  eliminarUsuarioDireccion(id: $id) {
    success
    message
  }
}
`;


const _format = 'YYYY-MM-DD'
const _today = moment().format(_format)
const _maxDate = moment().add(12, 'month').format(_format)

class Paymentpage extends React.PureComponent {

    constructor(props) {
        super(props);
        this.onChange = valuetime => {
            this.setState({ valuetime });
            console.log('esta es la hora', moment(valuetime).format('LT'))
        };
        this.state = {
            modalVisible: false,
            modalVisible1: false,
            modalVisible2: false,
            refetch: null,
            orden: '',
            data: null,
            cantidad: 1,
            UserID: '',
            recipient: '',
            direccion: '',
            nota: '',
            clave: '',
            profesional: '',
            descuento: '',
            selectedDayOfWeek: null,
            selectedDate: null,
            selectedTime: null,
            selectedHour: null,
            minCantidad: 0,
            maxCantidad: 0,
            hora: '',
            fecha: '',
            day: '',
            value: '',
            valuetime: undefined,
            messageTexteNeworden: 'Locatefit S.L., has recibido una nueva orden ve, a tu perfil para procesarla. https://locatefit.es/profile',
            useridpush: '',
            time: ""
        }

        this.onDayPress = this.onDayPress
    }

    refetch = null

    actualizarEstado(name, value) {
        this.setState({
            [name]: value,
        });
    };

    onCancel() {
        this.TimePicker.close();
    }

    onConfirm(hour, minute) {
        this.setState({ time: `${hour}:${minute}` });
        this.TimePicker.close();
    }

    componentDidUpdate = async () => {
        const { navigation } = this.props;
        const id = await AsyncStorage.getItem('id');
        const data = await navigation.getParam('data');
        this.setState({
            data,
            UserID: id,
            recipient: data.creator.telefono,
            useridpush: data.creator.UserID
        })
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    setModalVisible1(visible) {
        this.setState({ modalVisible1: visible });
    }

    setModalVisible2(visible) {
        this.setState({ modalVisible2: visible });
    }

    SendTextSMSNeworden = () => {
        fetch(`${NETWORK_INTERFACE_LINK}/send-message?recipient=${this.state.recipient}&textmessage=${this.state.messageTexteNeworden}`)
            .catch(err => console.log(err))
    };
    SendPushNotificationNeworden = () => {
        fetch(`${NETWORK_INTERFACE_LINK}/send-push-notification?IdOnesignal=${this.state.useridpush}&textmessage=${this.state.messageTexteNeworden}`)
            .catch(err => console.log(err))
    };

    abrirdatepiker = () => {
        this.setState({
            setShow: true
        })
    }

    async handleEliminar(e, eliminarUsuarioDireccion, id) {
        await eliminarUsuarioDireccion({ variables: { id } }).then(async ({ data: res }) => {
            if (
                res &&
                res.eliminarUsuarioDireccion &&
                res.eliminarUsuarioDireccion.success
            ) {
                console.log(res.eliminarUsuarioDireccion.message);
                this.refetch()
            }
            else if (
                res &&
                res.eliminarUsuarioDireccion &&
                !res.eliminarUsuarioDireccion.success
            ) {
                console.log(res.eliminarUsuarioDireccion.message);
                this.refetch()
            }
        });

    }

    crearOrden = async (mutacion, refetch = () => { }) => {
        const { nota } = this.state;
        const { data } = this.state;
        const input = { producto: data.id, cantidad: this.state.cantidad, endDate: this.state.fecha, time: this.state.time, direccion: this.state.direccion, nota: nota, clave: '' }
        const resultado = await mutacion({ variables: { input } });
        const orden = _get(resultado, 'data.crearModificarOrden.id', null);
        const ordenFull = _get(resultado, 'data.crearModificarOrden', null);
        this.setState({
            orden: ordenFull,
        })
        console.log(this.state.orden)
        if (!orden) {
            Alert.alert(
                'Upps debes iniciar sesión para continuar',
                'Para continuar con la contratación debes iniciar sesión o regístrarte',
                [
                    {
                        text: 'Iniciar sesión', onPress: () => this.props.navigation.navigate('Login'),
                    },
                    {
                        text: 'Regístrarme', onPress: () => this.props.navigation.navigate('Register')
                    },
                ],
                { cancelable: false },
            )
            return null
        } if (!this.state.day && !this.state.time) {
            Alert.alert(
                'Upps debes seleccionar una fecha',
                'Para continuar con la contratación debes seleccionar un día del calendario',
                [
                    {
                        text: 'OK', onPress: () => console.log('OK'),
                    },

                ],
                { cancelable: false },
            )
            return null
        } if (!this.state.time) {
            Alert.alert(
                'Upps debes seleccionar una hora',
                'Para continuar con la contratación debes seleccionar una hora',
                [
                    {
                        text: 'OK', onPress: () => console.log('OK'),
                    },

                ],
                { cancelable: false },
            )
            return null
        } if (this.state.UserID === data.creator.id) {
            Alert.alert(
                'Upps álgo va mal',
                'No puedes contratar tu propio sevicio',
                [
                    {
                        text: 'OK', onPress: () => console.log('OK'),
                    },

                ],
                { cancelable: false },
            )
            return null
        } else {
            this.setState({ ordenFull });
            const NotificationInput = {
                orden: orden,
                user: data.created_by,
                cliente: this.state.UserID,
                profesional: data.created_by,
                type: 'new_order'
            }
            this.props.client
                .mutate({
                    mutation: CREATE_NOTIFICATION,
                    variables: { input: NotificationInput }
                })
                .then(async (results) => {
                    console.log("results", results)
                }).catch(err => {
                    console.log("err", err)
                })
            this.SendTextSMSNeworden();
            this.SendPushNotificationNeworden()
            this.setModalVisible(true);
            console.log('La orden ha sido creada éxitosamente');
            this.refetch()
        }
    }
    catch(error) {
        console.log("error creando orden", error)
    }

    handleSubmit = (e, mutacion, err, refetch) => {
        const input = {
            id: this.state.orden.id,
            nota: this.state.orden.nota,
            aceptaTerminos: this.state.orden.aceptaTerminos,
            producto: this.state.orden.producto,
            cantidad: this.state.orden.cantidad,
            clave: this.state.clave
        };
        if (!this.state.clave) {
            Alert.alert(
                'Upps cupón no válido ',
                'Parece que el cupón no es válido intenta con otro código de descuento',
                [
                    {
                        text: 'OK', onPress: () => console.log('OK'),
                    }
                ],
                { cancelable: false },
            )
            this.actualizarEstado('clave', " ")
            return null
        }
        mutacion({ variables: { input } }).then(data => {
            this.setState(prevState => ({
                data: { ...prevState.data, clave: this.state.clave },
                descuento: data.data.crearModificarOrden.descuento
            }))
            console.log('orden modificada ', data);
            if (data) {
                this.setModalVisible1(!this.state.modalVisible1)
                console.log('orden modificada y cupon aplicado')
                this.refetch()
            }
        }).catch(err => {
            console.log("err", err)
        })
    }

    guardarOrden = (e, mutacion, err, refetch = () => { }) => {
        const { navigation } = this.props;
        const input = {
            id: this.state.orden.id,
            cupon: this.state && this.state.descuento ? this.state.descuento.id : '',
            nota: this.state.nota,
            aceptaTerminos: this.state.orden.aceptaTerminos,
            direccion: this.state.direccion,
            producto: this.state.orden.producto,
            cantidad: this.state.orden.cantidad,
        };
        if (!input.direccion, !input.nota) {
            Alert.alert(
                'Upps falta información',
                'Para continuar con la contratación debes añadir una dirección y la descripción de la tarea',
                [
                    {
                        text: 'OK', onPress: () => console.log('OK'),
                    }
                ],
                { cancelable: false },
            )
            return null
        } else (input.direccion, input.nota)
        console.log('saved order modified', input)
        mutacion({ variables: { input } }).then(data => {
            console.log('data: ', data);
            if (data) {
                console.log('orden modificada proceder al pago')
                this.setModalVisible(!this.state.modalVisible)
                navigation.navigate('Paymentype', { data: data })
            }
        }).catch(err => {
            console.log("err", err)
        })
    }


    _renderItem(item, refetch) {
        const { cantidad = 0, descuento = this.state.descuento } = this.state;
        const valorDescuento = _get(descuento, 'descuento', null);
        const valorclave = _get(descuento, 'clave', this.state.clave);
        const tipo = _get(descuento, 'tipo', null);
        const { nota, clave } = this.state;
        const subtotal = item.number * cantidad;
        let displayDescuento = `0€`;
        let descuentoFinal = 0;
        let total = subtotal;
        if (valorDescuento && tipo) {
            switch (tipo) {
                case 'dinero':
                    displayDescuento = `${valorDescuento}€`;
                    total = subtotal - valorDescuento;
                    break;
                case 'porcentaje':
                    displayDescuento = `${valorDescuento}%`;
                    descuentoFinal = valorDescuento / 100;
                    total = subtotal - subtotal * descuentoFinal;
                    break;
            }
        }

        const ImagePro = item.fileList.length > 0 ? item.fileList.length > 0 ? NETWORK_INTERFACE_LINK_AVATAR + item.fileList[0] : "" : null

        console.log(ImagePro)

        return (
            <View style={styles.container}>
                <View style={{ height: 'auto', borderBottomWidth: 0.5, borderBottomColor: colors.rgb_235 }}>
                    <View style={{ flexDirection: 'row', padding: 10, marginTop: 20 }}>
                        <View>
                            <Image source={{ uri: ImagePro }} style={{ width: 80, height: 80, borderRadius: 8 }} />
                        </View>
                        <View style={{ marginLeft: 15 }}>
                            <CustomText light={colors.blue_main} dark={colors.white} numberOfLines={1} style={{ fontWeight: '500', width: dimensions.Width(70) }}>{item.title}</CustomText>
                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                <Text style={{ color: colors.rgb_153, fontWeight: '200' }}>
                                    <AntdIcons name="staro" size={14} color={colors.orange} style={{ alignSelf: 'center' }} /> Valoraciones de cliente ({item.professionalRating.length})
                            </Text>
                            </View>
                            <View style={{ marginTop: 0 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ textAlign: 'center', fontSize: dimensions.FontSize(18), color: colors.rgb_153, marginTop: 5, fontWeight: '200' }}>Ofrecido por: </Text>
                                    <CustomText light={colors.blue_main} dark={colors.white} style={{ textAlign: 'center', fontSize: dimensions.FontSize(18), marginLeft: 10, marginTop: 5 }}>{item.creator.nombre} {item.creator.apellidos}</CustomText>
                                    {
                                        !item.creator.Verified ?
                                            <Iconsverifiel name="verified" style={{ alignSelf: 'center', marginLeft: 7, marginTop: 7 }} size={12} color={colors.twitter_color} /> : null
                                    }
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ marginTop: dimensions.Height(2) }}>
                    <View style={{ alignItems: 'center', paddingBottom: 20 }}>
                        <CustomText light={colors.blue_main} dark={colors.white} style={{ fontWeight: '200', marginTop: 30, marginBottom: 20 }}>
                            Hora y fecha seleccionada
                        </CustomText>
                        <View style={{ width: dimensions.Width(95), marginLeft: 15 }}>
                            <CustomText light={colors.blue_main} dark={colors.white} style={{ fontSize: 14, marginTop: 10 }}>
                                Fecha: {this.state.fecha}
                            </CustomText>
                            <CustomText light={colors.blue_main} dark={colors.white} style={{ fontSize: 14, marginTop: 10 }}>
                                Hora: {this.state.time}
                            </CustomText>
                        </View>
                    </View>

                    <View style={{ marginTop: 20, borderTopWidth: 1, borderBottomWidth: 1, borderBottomColor: colors.rgb_235, borderTopColor: colors.rgb_235, paddingBottom: 10 }}>
                        <View style={{ flexDirection: 'column', alignItems: 'flex-start', marginRight: 10, marginLeft: 15 }}>
                            <Text style={{ fontSize: 14, color: colors.rgb_102, marginTop: 10 }}>
                                Precio unitario
                            </Text>
                            <Text style={{ fontSize: 14, color: colors.rgb_102, marginTop: 10 }}>
                                Cantidad
                            </Text>
                            <Text style={{ fontSize: 14, color: colors.rgb_102, marginTop: 10 }}>
                                Subtotal
                            </Text>
                            <Text style={{ fontSize: 14, color: colors.rgb_102, marginTop: 10 }}>
                                Descuento
                            </Text>
                            <Text style={{ fontSize: 14, color: colors.rgb_102, marginTop: 10 }}>
                                Total a pagar
                            </Text>
                        </View>
                        <View style={{ flexDirection: 'column', alignItems: 'flex-end', marginRight: 15, marginLeft: 15, marginTop: -130 }}>
                            <Text style={{ fontSize: 14, color: colors.rgb_102, marginTop: 10 }}>
                                {item.number}€
                            </Text>
                            <Text style={{ fontSize: 14, color: colors.rgb_102, marginTop: 10 }}>
                                {cantidad}
                            </Text>
                            <Text style={{ fontSize: 14, color: colors.rgb_102, marginTop: 10 }}>
                                {subtotal}€
                            </Text>
                            <Text style={{ fontSize: 14, color: colors.rgb_102, marginTop: 10 }}>
                                {displayDescuento}
                            </Text>
                            <Text style={{ fontSize: 14, color: colors.rgb_102, marginTop: 10 }}>
                                {total}€
                            </Text>
                        </View>
                    </View>
                    <View style={{ alignItems: 'center', borderBottomWidth: 1, borderBottomColor: colors.rgb_235, paddingBottom: 20 }}>
                        <CustomText light={colors.black} dark={colors.white} style={{ fontWeight: '200', marginTop: 30 }}>
                            ¿Tienes un cupón?
                         </CustomText>
                        <View style={{ marginTop: 22 }}>
                            <Modal
                                animationType="slide"
                                transparent={false}
                                visible={this.state.modalVisible1}>
                                <ViewCuston light={colors.white} dark={colors.black} containers={
                                    <View>
                                        <SafeAreaView style={styles.headers}>
                                            <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                                <View style={{ alignItems: 'flex-start', marginLeft: 10 }}>
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity onPress={() => { this.setModalVisible1(!this.state.modalVisible1) }}>
                                                        <Text style={{ marginRight: 20 }}>
                                                            <AntdIcons name="close" size={25} color={colors.green_main} />
                                                        </Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </SafeAreaView>
                                        <Text style={{ marginTop: dimensions.Height(20), fontSize: dimensions.FontSize(16), color: colors.slight_black, padding: 10 }}>¿Tienes un cúpon de descuento? Este es el momento de canjearlo</Text>
                                        <View style={{ alignSelf: 'center', marginTop: dimensions.Height(5) }}>
                                        <KeyboardAwareScrollView keyboardShouldPersistTaps="always" style={{paddingHorizontal: dimensions.Width(4)}}> 
                                                <CustomTextInput
                                                    name='clave'
                                                    value={clave}
                                                    containerStyle={{ marginTop: dimensions.Height(1)}}
                                                    onChangeText={(text) => this.actualizarEstado('clave', text)}
                                                    keyboardType="default" 
                                                    placeholder="Cupón" />

                                                <View style={{marginTop: dimensions.Height(3)}}>
                                                <Mutation mutation={CREAR_MODIFICAR_ORDEN}>
                                                    {(crearModificarOrden, { loading, error, data, refetch }) => (
                                                        <Button light={colors.white} dark={colors.white} containerStyle={styles.buttonView1}
                                                            onPress={e => this.handleSubmit(e, crearModificarOrden, refetch)}
                                                            title="Aplicar cupón"
                                                            titleStyle={styles.buttonTitle} />
                                                    )}
                                                </Mutation>
                                            </View>
                                            </KeyboardAwareScrollView>
                                            
                                        </View>
                                    </View>}
                                />
                            </Modal>
                        </View>
                        {
                            !valorclave ?
                                <TouchableOpacity style={styles.cardtarjeta} onPress={() => { this.setModalVisible1(true) }}>
                                    <CustomText light={colors.blue_main} dark={colors.white}>
                                        <AntdIcons name="tagso" size={16} color={colors.rgb_153} />   Añadir cupón
                                    </CustomText>
                                </TouchableOpacity > : null
                        }

                        {
                            valorclave ?
                                <View style={styles.cardtarjeta1}>
                                    <Text style={{ color: '#52c41a', textAlign: 'center' }}>
                                        <AntdIcons name="check" size={16} color='#52c41a' />   Cupón añadido con éxito
                                                    </Text>
                                </View > : null
                        }

                    </View>


                    <View style={{ alignItems: 'center', paddingBottom: 20 }}>
                        <CustomText light={colors.black} dark={colors.white} style={{ fontWeight: '200', marginTop: 30, marginBottom: 20 }}>
                            Añadir descripción de la tarea
                        </CustomText>
                        <View style={{ width: dimensions.Width(95) }}>
                            <TextareaItem
                                name='nota'
                                value={nota}
                                onChangeText={(text) => this.actualizarEstado('nota', text)}
                                rows={3}
                                style={{backgroundColor: 'transparent', color: colors.rgb_153}}
                                placeholderTextColor={colors.rgb_153}
                                placeholder="Descripción de la tarea"
                                count={100} />
                        </View>
                    </View>
                    <Query query={USUARIO_DIRECCION_QUERY}>
                        {({ loading, error, data, refetch }) => {
                            this.refetch = refetch;
                            if (loading) {
                                return null
                            }
                            else {
                                let response = { data: data }
                                const laData = response && response.data && response.data.getUsuarioDireccion.data ? response.data.getUsuarioDireccion.data : ''
                                this.setState({
                                    direccion: laData.id
                                })
                                const isDisableCard = !!response && response.data && response.data.getUsuarioDireccion.data ? response.data.getUsuarioDireccion.data : '' && !!response && response.data && response.data.getUsuarioDireccion.data ? response.data.getUsuarioDireccion.data.id : ''

                                return (
                                    <View style={{ padding: 15, borderBottomWidth: 0.5, borderBottomColor: '#ddd', alignItems: 'center' }}>
                                        <CustomText light={colors.blue_main} dark={colors.white} style={{ fontWeight: '200', marginBottom: 10 }}>
                                            ¿Dónde quieres que vayamos?
                                        </CustomText>
                                        {
                                            isDisableCard ?
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Text numberOfLines={1} style={{ fontWeight: '200', marginBottom: 5, color: colors.rgb_153 }}>
                                                        <AntdIcons name="home" size={16} color={colors.green_main} /> {laData.calle} {laData.cuidad}, {laData.codigopostal}, {laData.telefono}
                                                    </Text>
                                                    <Mutation mutation={ELIMINAR_USUARIO_DIRECCION}>
                                                        {(eliminarUsuarioDireccion) => {
                                                            return (
                                                                <TouchableOpacity style={{ marginLeft: 'auto' }} onPress={e =>
                                                                    this.handleEliminar(e, eliminarUsuarioDireccion, laData.id, refetch)
                                                                }>
                                                                    <Text numberOfLines={1} style={{ fontWeight: '200', marginBottom: 5, color: colors.rgb_153, marginLeft: 'auto' }}>
                                                                        <AntdIcons name="delete" size={16} color={colors.ERROR} style={{ marginLeft: 15 }} />
                                                                    </Text>
                                                                </TouchableOpacity>
                                                            );
                                                        }}
                                                    </Mutation>
                                                </View> : null
                                        }
                                        {
                                            !isDisableCard ?
                                                <View style={{ alignItems: 'center' }}>
                                                    <View style={{ marginTop: 22 }}>
                                                        <Modal
                                                            animationType="slide"
                                                            transparent={false}
                                                            visible={this.state.modalVisible2}>
                                                            <ViewCuston light={colors.white} dark={colors.black} containers={
                                                                <View>
                                                            <SafeAreaView style={styles.headers}>
                                                                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                                                    <View style={{ alignItems: 'flex-start', marginLeft: 10 }}>
                                                                    </View>
                                                                    <View style={{ flexDirection: 'row' }}>
                                                                        <TouchableHighlight onPress={() => { this.setModalVisible2(!this.state.modalVisible2) }}>
                                                                            <Text style={{ marginRight: 20 }}>
                                                                                <AntdIcons name="close" size={25} color={colors.green_main} />
                                                                            </Text>
                                                                        </TouchableHighlight>
                                                                    </View>
                                                                </View>
                                                            </SafeAreaView>
                                                            <View style={{ alignSelf: 'center', marginTop: dimensions.Height(5) }}>
                                                                <Adress />
                                                            </View>
                                                            </View>}
                                                            />
                                                        </Modal>
                                                    </View>
                                                    <TouchableOpacity style={styles.cardtarjeta} onPress={() => { this.setModalVisible2(!this.state.modalVisible2) }}>
                                                        <CustomText light={colors.blue_main} dark={colors.white}><AntdIcons name="plus" size={16} color={colors.green_main} />   Añadir dirección</CustomText>
                                                    </TouchableOpacity>
                                                </View> : null
                                        }
                                    </View>
                                )
                            }
                        }}
                    </Query>
                    <View>
                    </View>
                    <View style={{ alignItems: 'center', paddingBottom: 20 }}>
                        <CustomText light={colors.black} dark={colors.white} style={{ fontWeight: '200', marginTop: 30 }}>
                            Continuar implica que has leído y aceptado los términos y condiciones de uso y compra
                        </CustomText>
                    </View>
                    <View style={{ alignItems: 'center', marginBottom: dimensions.Height(30), marginTop: 30 }}>
                        <Mutation mutation={CREAR_MODIFICAR_ORDEN}>
                            {(crearModificarOrden, { loading, error, data, refetch }) => (
                                <View style={styles.signupButtonContainer}>
                                    <Button light={colors.white} dark={colors.white} containerStyle={styles.buttonView}
                                        onPress={e => this.guardarOrden(e, crearModificarOrden, refetch)}
                                        title="CONTINUAR"
                                        titleStyle={styles.buttonTitle} />
                                </View>
                            )}
                        </Mutation>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        )
    }

    onDayPress = day => {
        this.setState({
            selected: day.dateString,
            day
        })
    }
    render() {
        const { navigation } = this.props;
        const { data, cantidad } = this.state;
        const fecha = moment(this.state.day.timestamp).format('L')
        this.setState({
            fecha
        })

        if (data) {
            return (
                <ViewCuston light={colors.white} dark={colors.black} containers={
                    <View style={{ paddingHorizontal: dimensions.Width(4) }}>
                        <Header navigation={navigation} />
                        <Modal
                            animationType="slide"
                            transparent={false}
                            visible={this.state.modalVisible}
                            onRequestClose={() => {
                                Alert.alert('Esta seguro que deseas cancelar la orden.');
                            }}>
                            <ViewCuston light={colors.white} dark={colors.black} containers={
                                <View>
                                    <SafeAreaView style={styles.headers}>
                                        <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                            <View style={{ alignItems: 'flex-start', marginLeft: 10 }}>
                                            </View>
                                            <View style={{ flexDirection: 'row' }}>
                                                <TouchableHighlight onPress={() => { this.setModalVisible(!this.state.modalVisible) }}>
                                                    <Text style={{ marginRight: 20 }}>
                                                        <AntdIcons name="close" size={25} color={colors.green_main} />
                                                    </Text>
                                                </TouchableHighlight>
                                                <Text style={{ alignContent: 'center', marginTop: 5, color: colors.rgb_153, fontSize: dimensions.FontSize(16) }}>
                                                    Completar la contratación
                                    </Text>
                                            </View>
                                        </View>
                                    </SafeAreaView>
                                    {this._renderItem(data)}
                                </View>}
                            />
                        </Modal>
                        <View style={{ marginTop: 30, }}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                <Calendar
                                    theme={{
                                        backgroundColor: 'transparent',
                                        calendarBackground: 'transparent',
                                        textSectionTitleColor: '#b6c1cd',
                                        selectedDayBackgroundColor: colors.green_main,
                                        selectedDayTextColor: '#ffffff',
                                        todayTextColor: colors.green_main,
                                        dayTextColor: colors.rgb_153,
                                        textDisabledColor: '#d9e1e8',
                                        dotColor: colors.green_main,
                                        selectedDotColor: '#ffffff',
                                        arrowColor: colors.green_main,
                                        monthTextColor: colors.rgb_153,
                                        indicatorColor: '#d9e1e8',
                                        textDayFontWeight: '300',
                                        textMonthFontWeight: '200',
                                        textDayHeaderFontWeight: '300',
                                        textDayFontSize: 16,
                                        textMonthFontSize: 16,
                                        textDayHeaderFontSize: 16
                                    }}

                                    firstDay={1}
                                    minDate={_today}
                                    maxDate={_maxDate}
                                    disableArrowLeft={true}
                                    disableArrowRight={true}
                                    hideArrows={false}
                                    hideExtraDays={true}
                                    onDayPress={this.onDayPress}
                                    markedDates={{
                                        [this.state.selected]: {
                                            selected: true,
                                            disableTouchEvent: true,
                                            selectedDotColor: 'orange',
                                        },
                                    }}

                                />

                                {
                                    this.state.day ?
                                        <View style={{ marginBottom: 30, marginTop: 50 }}>
                                            <Text style={{ width: dimensions.Width(90), alignSelf: 'center', fontSize: dimensions.FontSize(14), fontWeight: '300', color: colors.rgb_153, }}>¿A qué hora quieres reservar? </Text>
                                            <TimePicker
                                                ref={ref => { this.TimePicker = ref; }}
                                                onCancel={() => this.onCancel()}
                                                onConfirm={(hour, minute) => this.onConfirm(hour, minute)}
                                                textCancel="Cancelar"
                                                textConfirm="Selecionar"
                                            />
                                            <Text onPress={() => this.TimePicker.open()} style={styles.buttonView2}>{this.state.time ? this.state.time : 'Seleccionar hora'}</Text>
                                        </View> : null
                                }

                                {
                                    this.state.time ?
                                        <View style={{ width: dimensions.Width(90), marginBottom: 20, alignSelf: 'center' }}>
                                            <Text style={{ width: dimensions.Width(90), marginBottom: 20, fontSize: dimensions.FontSize(14), fontWeight: '300', color: colors.rgb_153, marginBottom: 40 }}>¿Cuantas {data.currency} quieres reservar? </Text>
                                            <Stepper
                                                key="1"
                                                style={{ width: dimensions.Width(30) }}
                                                min={1}
                                                readOnly={false}
                                                defaultValue={1}
                                                value={cantidad}
                                                name="cantidad"
                                                onChange={(text) => this.actualizarEstado('cantidad', text)}
                                                styles={{ highlightStepBorderColor: colors.green_main }}
                                                inputStyle={{ backgroundColor: 'transparent', color: colors.rgb_153 }}
                                            />
                                        </View>
                                        : null
                                }
                                <View style={{ marginBottom: dimensions.Height(50) }}>
                                    <Mutation mutation={CREAR_MODIFICAR_ORDEN}>
                                        {(crearModificarOrden) => (
                                            <View style={styles.signupButtonContainer}>
                                                <Button light={colors.white} dark={colors.white} containerStyle={styles.buttonView}
                                                    onPress={() => this.crearOrden(crearModificarOrden, data)}
                                                    title="CONTINUAR"
                                                    titleStyle={styles.buttonTitle} />
                                            </View>
                                        )}
                                    </Mutation>
                                </View>
                            </ScrollView>
                        </View>
                    </View>}
                />
            )
        }
        return null;
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        paddingHorizontal: dimensions.Width(2),
        height: dimensions.Height(100)
    },
    payment: {
        height: 'auto',
        alignSelf: 'center',
        width: dimensions.Width(90),
        backgroundColor: colors.white,
        marginTop: 20,
        borderRadius: 10,
        paddingVertical: dimensions.Width(4),
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.2,
        elevation: 3,
    },
    buttonView: {
        marginTop: dimensions.Height(4),
        alignSelf: 'center',
        backgroundColor: colors.green_main,
        width: dimensions.Width(84),
        borderRadius: dimensions.Width(8),
    },

    buttonView2: {
        marginTop: dimensions.Height(4),
        alignSelf: 'center',
        backgroundColor: 'transparent',
        width: dimensions.Width(90),
        padding: dimensions.Height(2),
        borderRadius: dimensions.Width(2),
        borderWidth: 0.5,
        borderColor: colors.green_main,
        color: colors.rgb_153
    },

    buttonView1: {
        marginTop: dimensions.Height(1),
        alignSelf: 'center',
        backgroundColor: colors.blue_main,
        width: dimensions.Width(84),
        borderRadius: dimensions.Width(8),
    },
    buttonTitle: {
        alignSelf: 'center',
        paddingVertical: dimensions.Height(2),
        paddingHorizontal: dimensions.Width(2),
        color: colors.white,
        fontWeight: 'bold',
        fontSize: dimensions.FontSize(17),
    },

    cardtarjeta: {
        width: 300,
        height: 60,
        backgroundColor: 'transparent',
        borderWidth: 1,
        borderColor: colors.green_main,
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        paddingLeft: 30,
        paddingRight: 30,

    },

    cardtarjeta1: {
        color: "#52c41a",
        width: 300,
        height: 60,
        backgroundColor: "#b7eb8f",
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        paddingLeft: 30,
        paddingRight: 30,

    },


});

export default withApollo(Paymentpage);