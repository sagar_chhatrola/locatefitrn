import React from "react";
import { SafeAreaView, View } from "react-native";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { dimensions } from './../theme';
import { useDynamicValue } from 'react-native-dark-mode'
import { colors } from '../theme/colors';

export default function LoadingIns(){
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark)
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030')
  return(
    <SafeAreaView style={{alignSelf: 'center'}}>
    <SkeletonPlaceholder backgroundColor={placeholderColor} highlightColor={placeholderColor1}>
    <View style={{width: dimensions.Width(100), marginBottom: 40 }}>
        <View style={{ width: dimensions.Width(100), height: 250, borderRadius: 3}} />
        <View style={{flexDirection: 'row'}}>
          <View>
            <View style={{ width: dimensions.Width(65), height: 5, borderRadius: 3, marginTop: 15}} />
            <View style={{ width: dimensions.Width(45), height: 5, borderRadius: 3, marginTop: 15}} />
          </View>
          <View style={{marginLeft: 'auto', marginRight: 15, flexDirection: "row"}}>
            <View style={{ width: 30 , height: 30, borderRadius: 3, marginTop: 15, marginRight: 20}} />
            <View style={{ width: 30, height: 30, borderRadius: 3, marginTop: 15}} />
          </View>
        </View>
    </View>
    <View style={{width: dimensions.Width(100), marginBottom: 40 }}>
        <View style={{ width: dimensions.Width(100), height: 250, borderRadius: 3}} />
        <View style={{flexDirection: 'row'}}>
          <View>
            <View style={{ width: dimensions.Width(65), height: 5, borderRadius: 3, marginTop: 15}} />
            <View style={{ width: dimensions.Width(45), height: 5, borderRadius: 3, marginTop: 15}} />
          </View>
          <View style={{marginLeft: 'auto', marginRight: 15, flexDirection: "row"}}>
            <View style={{ width: 30 , height: 30, borderRadius: 3, marginTop: 15, marginRight: 20}} />
            <View style={{ width: 30, height: 30, borderRadius: 3, marginTop: 15}} />
          </View>
        </View>
    </View>
    <View style={{width: dimensions.Width(100), marginBottom: 40 }}>
        <View style={{ width: dimensions.Width(100), height: 250, borderRadius: 3}} />
        <View style={{flexDirection: 'row'}}>
          <View>
            <View style={{ width: dimensions.Width(65), height: 5, borderRadius: 3, marginTop: 15}} />
            <View style={{ width: dimensions.Width(45), height: 5, borderRadius: 3, marginTop: 15}} />
          </View>
          <View style={{marginLeft: 'auto', marginRight: 15, flexDirection: "row"}}>
            <View style={{ width: 30 , height: 30, borderRadius: 3, marginTop: 15, marginRight: 20}} />
            <View style={{ width: 30, height: 30, borderRadius: 3, marginTop: 15}} />
          </View>
        </View>
    </View>
    <View style={{width: dimensions.Width(100), marginBottom: 40 }}>
        <View style={{ width: dimensions.Width(100), height: 250, borderRadius: 3}} />
        <View style={{flexDirection: 'row'}}>
          <View>
            <View style={{ width: dimensions.Width(65), height: 5, borderRadius: 3, marginTop: 15}} />
            <View style={{ width: dimensions.Width(45), height: 5, borderRadius: 3, marginTop: 15}} />
          </View>
          <View style={{marginLeft: 'auto', marginRight: 15, flexDirection: "row"}}>
            <View style={{ width: 30 , height: 30, borderRadius: 3, marginTop: 15, marginRight: 20}} />
            <View style={{ width: 30, height: 30, borderRadius: 3, marginTop: 15}} />
          </View>
        </View>
    </View>
    </SkeletonPlaceholder>
  </SafeAreaView>
  )
}
