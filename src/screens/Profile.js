import React, { Component } from 'react';
import { View, Image, ImageBackground, StyleSheet, Alert, Linking, SafeAreaView, TextInput } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { connect } from 'react-redux';
import { colors, dimensions } from './../theme';
import { CustomText } from './../components/CustomText';
import { Button } from './../components/Button';
import { user, image } from './../constants';
import { NETWORK_INTERFACE_LINK_AVATAR } from '../constants/config';
import { Avatar } from 'react-native-elements';
import ButtonSocial from '../components/RedesSociales';
import { WebView } from 'react-native-webview';
import Link from '../components/Link';
import Header from './../components/Header';
import Iconsverifiel from 'react-native-vector-icons/Octicons';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';


export default class Profile extends React.Component {

    _renderItem(item) {
        const { navigation } = this.props;
        return (
            <ViewCuston light={colors.white} dark={colors.black} containers={
            <View style={styles.container}>
            <Header navigation={navigation} />
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false} >
                <View style={{ flexDirection: 'row' }}>
                    <View>
                        <Avatar
                            size="large"
                            rounded
                            source={{ uri: NETWORK_INTERFACE_LINK_AVATAR + item.creator.foto_del_perfil }}
                            containerStyle={{borderWidth: 3, borderColor: colors.rgb_235}}
                        />
                    </View>
                    <View>
                        <View style={{flexDirection: 'row'}}>
                            <CustomText light={colors.blue_main} dark={colors.white} style={{fontSize: dimensions.FontSize(24), marginLeft: 20, marginTop: 10, fontWeight: 'bold'}}>
                            {item.creator.nombre} {item.creator.apellidos}
                        </CustomText>
                        {
                            item.creator.Verified ?
                            <Iconsverifiel name="verified" style={{alignSelf: 'center', marginLeft: 5, marginTop: 15}} size={12} color={colors.twitter_color}/>
                            : null
                        
                        }
                        </View> 
                        <CustomText style={{ width: dimensions.Width(70) ,fontSize: dimensions.FontSize(14), marginLeft: 20, color: colors.rgb_153, fontWeight: '200'}}>
                            {item.creator.descripcion}
                    </CustomText>
                    </View>
                </View>
                <View style={{width: dimensions.Width(90), borderTopWidth: 0.5, borderBottomWidth: 0.5, marginTop: 20, borderTopColor: colors.light_grey, borderBottomColor: colors.light_grey, height: dimensions.Height(8), flexDirection: 'row', alignSelf: 'center'}}>
                    <CustomText style={{alignSelf: 'center', color: colors.rgb_153,fontSize: dimensions.FontSize(14), fontWeight: '300'}}>
                        Sobre el profeisonal: 
                    </CustomText>
                    <View style={{alignSelf: 'center', marginLeft: 30}}>
                        <CustomText light={colors.blue_main} dark={colors.white} style={{alignSelf: 'center', fontSize: dimensions.FontSize(24), fontWeight: '500'}}>
                         {item && item.ordenes ? item.ordenes.length : ''}
                        </CustomText>
                        <CustomText style={{alignSelf: 'center', color: colors.rgb_153,fontSize: dimensions.FontSize(14), fontWeight: '200'}}>
                            Ordenes
                        </CustomText>
                    </View>
                    <View style={{alignSelf: 'center', marginLeft: 30}}>
                        <CustomText light={colors.blue_main} dark={colors.white} style={{alignSelf: 'center',fontSize: dimensions.FontSize(24), fontWeight: '500'}}>
                            { item && item.professionalRating ? item.professionalRating.length : ''}
                        </CustomText>
                        <CustomText style={{alignSelf: 'center', color: colors.rgb_153,fontSize: dimensions.FontSize(14), fontWeight: '200'}}>
                            Valoraciones
                        </CustomText>
                    </View>
                </View>
                <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{marginTop: 20, borderRadius: 3}}containers={
                <View style={{width: dimensions.Width(100), height: 'auto', }}>
                    <CustomText style={{padding: 10, color: colors.grey, fontSize: dimensions.FontSize(18)}}>
                        Información Personal
                    </CustomText>
                </View>}
                />
                <View style={{marginLeft: 10}}>
                <View style={{flexDirection: 'row', marginTop: 20}}>
                    <CustomText light={colors.black} dark={colors.white} >
                        Ciudad:
                    </CustomText>
                    <CustomText light={colors.black} dark={colors.white} style={{marginLeft: 30}}>
                        {item.creator.ciudad}
                    </CustomText>
                </View>
                <View style={{flexDirection: 'row', marginTop: 20}}>
                    <CustomText light={colors.black} dark={colors.white} style={{}}>
                        País:
                    </CustomText>
                    <CustomText light={colors.black} dark={colors.white} style={{marginLeft: 30}}>
                        España 🇪🇸
                    </CustomText>
                </View>
                <View style={{flexDirection: 'row', marginTop: 20}}>
                    <CustomText light={colors.black} dark={colors.white} style={{}}>
                        Sitio web:
                    </CustomText>
                    <View style={{marginLeft: 30}}>
                        <Link url={item.creator.propia_web_enlazar} light={colors.white} dark={colors.black} text={item.creator.propia_web_enlazar}/>
                    </View>
                </View>
                </View>
                <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{marginTop: 20, borderRadius: 3}}containers={
                <View style={{width: dimensions.Width(100), height: 'auto'}}>
                    <CustomText style={{padding: 10, color: colors.grey, fontSize: dimensions.FontSize(18)}}>
                        Información Profesional
                    </CustomText>
                </View>}
                />
                <View style={{marginLeft: 10}}>
                <View style={{flexDirection: 'row', marginTop: 20}}>
                    <CustomText light={colors.black} dark={colors.white} style={{}}>
                        Profesión:
                    </CustomText>
                    <CustomText light={colors.black} dark={colors.white} style={{marginLeft: 30}}>
                        {item.creator.profesion}
                    </CustomText>
                </View>
                <View style={{flexDirection: 'row', marginTop: 20}}>
                    <CustomText light={colors.black} dark={colors.white} style={{}}>
                        Estudio:
                    </CustomText>
                    <CustomText light={colors.black} dark={colors.white} style={{marginLeft: 30}}>
                        {item.creator.estudios}
                    </CustomText>
                </View>
                <View style={{flexDirection: 'row', marginTop: 20}}>
                    <CustomText light={colors.black} dark={colors.white} style={{}}> 
                        Grado de experiencia:
                    </CustomText>
                    <CustomText light={colors.black} dark={colors.white} style={{marginLeft: 30}}>
                        {item.creator.grado}
                    </CustomText>
                </View>
                </View>
                <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{marginTop: 20, borderRadius: 3}}containers={
                <View style={{width: dimensions.Width(100), height: 'auto'}}>
                    <CustomText style={{padding: 10, color: colors.grey, fontSize: dimensions.FontSize(18)}}>
                        Información de contacto
                    </CustomText>
                </View>}
                />

                <View style={{marginLeft: 10}}>
                <View style={{flexDirection: 'row', marginTop: 20}}>
                    <CustomText light={colors.black} dark={colors.white} style={{}}>
                        Email:
                    </CustomText>
                    <View style={{marginLeft: 30}}>
                        <Link url={'mailto:' + item.creator.email} light={colors.white} dark={colors.black} text="Escribirle al Email"/>
                    </View>
                </View>
                <View style={{flexDirection: 'row', marginTop: 20}}>
                    <CustomText light={colors.black} dark={colors.white} style={{}}>
                        Número movil:
                    </CustomText>
                    <CustomText light={colors.black} dark={colors.white} style={{marginLeft: 30}}>
                        {item.creator.telefono}
                    </CustomText>
                </View>
                </View>
                <View style={{flexDirection: 'row', alignSelf: 'center', marginTop: 20, marginBottom: 50}}>
                    <ButtonSocial type='facebook' url={item.creator.fb_enlazar} />
                    <ButtonSocial type='twitter' url={item.creator.twitter_enlazar} />
                    <ButtonSocial type='instagram' url={item.creator.instagram_enlazar} />
                    <ButtonSocial type='linkedin' url={item.creator.youtube_enlazar} />
                </View>
            
            </KeyboardAwareScrollView>
            </View>}
            />
        )
    }
    render() {
        const { navigation } = this.props;
        const data = navigation.getParam('data');
        return this._renderItem(data)
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        height: dimensions.Height(100),
        marginBottom: 0,
        paddingHorizontal: 15

    },
});