import React from 'react';
import { View, StyleSheet, Image, Switch, PixelRatio, TouchableOpacity, Text, DeviceEventEmitter } from 'react-native'
import { colors, dimensions } from './../theme';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { CustomText } from './../components/CustomText';
import { CustomTextInput } from './../components/CustomTextInput';
import MaterialCommunityIcons from 'react-native-vector-icons/Feather';
import { Button } from './../components/Button';
import { ScrollView } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import { NETWORK_INTERFACE_LINK_AVATAR } from './../constants/config';
import { Avatar } from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {
  TextField,
  FilledTextField,
  OutlinedTextField,
} from 'react-native-material-textfield';
import ImagePicker from 'react-native-image-picker';
import Header from './../components/Header';
import Geolocation from '@react-native-community/geolocation';
import RNLocation from 'react-native-location';
import { ActivityIndicator } from '@ant-design/react-native';
import Toast from 'react-native-simple-toast';
import HeaderParallax from './../components/HeaderParallaxMyaccount';
import Iconsverifiel from 'react-native-vector-icons/Octicons';



const USER_DETAIL = gql`
  query getUsuario($id: ID!) {
    getUsuario(id: $id) {
      email
      nombre
      apellidos
      ciudad
      telefono
      foto_del_perfil
      fotos_tu_dni
      profesion
      descripcion
      fecha_de_nacimiento
      notificacion
      grado
      estudios
      formularios_de_impuesto
      fb_enlazar
      twitter_enlazar
      instagram_enlazar
      youtube_enlazar
      propia_web_enlazar
      UserID
      Verified
    }
  }
`;

const ACTUALIZAR_USUARIO = gql`
  mutation actualizarUsuario($input: ActualizarUsuarioInput) {
    actualizarUsuario(input: $input) {
      id
      email
      nombre
      apellidos
      ciudad
      telefono
      foto_del_perfil
      fotos_tu_dni
      profesion
      descripcion
      fecha_de_nacimiento
      notificacion
      grado
      estudios
      formularios_de_impuesto
      fb_enlazar
      twitter_enlazar
      instagram_enlazar
      youtube_enlazar
      propia_web_enlazar
    }
  }
`;

const UPLOAD_FILE = gql`
  mutation singleUpload($imgBlob: Upload) {
    singleUpload(file: $imgBlob) {
      filename
    }
  }
`;


function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
    console.log(file)
  });
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('Solo puedes subir archivos JPG/PNG!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('La imagen debe ser inferior a 2MB!');
  }
  return isJpgOrPng && isLt2M;
}

class Myaccount extends React.Component {

  constructor(props) {
    super(props);
    this.onSwitchChange = value => {
      this.setState({
        checked: value,
      });
    };
    this.state = {
      checked: false,
    };
  }

  state = {
    email: '',
    nombre: '',
    apellidos: '',
    profesion: '',
    telefono: '',
    ciudad: '',
    foto_del_perfil: '',
    descripcion: '',
    notificacion: '',
    userId: '',
    fetched: false,
    Loading: false,
    Verified: ''
  };

  updateProfilePicUrl = filename => {
    if (filename) this.setState({ imageUrl: NETWORK_INTERFACE_LINK_AVATAR + filename });
  };

  selectPhotoTapped = (singleUpload) => {
    const options = {
      quality: 1.0,
      title: 'Seleccionar Avatar',
      cancelButtonTitle: 'Cancelar',
      takePhotoButtonTitle: 'Hacer foto',
      chooseFromLibraryButtonTitle: 'Seleccionar foto existente',
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        storageOptions: {
          skipBackup: true,
        },
      },

    };

    ImagePicker.showImagePicker(options, async response => {
      if (response) {
        const imgBlob = 'data:image/jpeg;base64,' + response.data;
        if (imgBlob) {
          singleUpload({ variables: { imgBlob } }).then((res) => {
            console.log('filemane: ', res.data.singleUpload.filename)
            this.setState({
              foto_del_perfil: res.data.singleUpload.filename
            })
          }).catch(error => {
            console.log('fs error: ', error)
          })
        }
      }
    });
  }



  _handleSubmit = async (mutation) => {
    const id = await AsyncStorage.getItem('id');
    let input = this.state;
    delete input.fetched;
    delete input.checked;
    delete input.Loading
    delete input.userId
    delete input.image
    delete input.Verified
    input.id = id;
    console.log("input", input)
    mutation({ variables: { input } }).then(res => {
      Toast.showWithGravity('Perfil actualizado con éxito', Toast.LONG, Toast.TOP)
      console.log('done')
      this.refetch()
    }).catch(err => console.log(err))
  }

  async componentDidMount() {
    const userId = await AsyncStorage.getItem('id');
    this.setState({ userId })
  }

  _handleInputValue = (name, value) => {
    this.setState({ [name]: value })
  }

  setUserData = (user) => {
    const {
      email,
      nombre,
      apellidos,
      telefono,
      fotos_tu_dni,
      ciudad,
      profesion,
      descripcion,
      fecha_de_nacimiento,
      notificacion,
      grado,
      estudios,
      formularios_de_impuesto,
      fb_enlazar,
      twitter_enlazar,
      instagram_enlazar,
      youtube_enlazar,
      propia_web_enlazar, } = user;
    this.setState({
      email,
      nombre,
      apellidos,
      ciudad,
      telefono,
      fotos_tu_dni,
      profesion,
      descripcion,
      fecha_de_nacimiento,
      notificacion,
      grado,
      estudios,
      formularios_de_impuesto,
      fb_enlazar,
      twitter_enlazar,
      instagram_enlazar,
      youtube_enlazar,
      propia_web_enlazar,
      fetched: true
    })
  }

  render() {
    const {
      userId,
      email,
      nombre,
      apellidos,
      ciudad,
      telefono,
      profesion,
      descripcion,
      notificacion,
      Verified,
      fetched,
    } = this.state;
    const { navigation } = this.props;
    const data = navigation.getParam('data')
    if (userId == '') {
      return null
    }

    return (
      <Query query={USER_DETAIL} variables={{ id: data }}>
        {({ loading, error, data, refetch }) => {
          this.refetch = refetch
          if (error) {
            console.log('Response Error-------', error);
            return <ActivityIndicator />
          }
          if (loading) {
            return <ActivityIndicator />
          }

          if (data) {
            const userData = data.getUsuario
            if (userData && !fetched) {
              this.setUserData(userData)
            }
            return (
              <HeaderParallax name={
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ color: 'white', fontSize: 24 }}>{nombre} {apellidos}</Text>
                  {
                    userData.Verified ?
                      <Iconsverifiel name="verified" style={{ alignSelf: 'center', marginLeft: 5, marginTop: 5 }} size={12} color={colors.white} /> : null
                  }
                </View>
              }
                name1={<Text style={styles.stickySectionText}>{nombre} {apellidos}</Text>}
                descripcion={<Text style={{ fontSize: 14, fontWeight: '400', color: 'white' }}>
                  {descripcion}
                </Text>}
                back={<AntdIcons name="close" style={{ alignSelf: 'center', marginTop: 5, marginLeft: 2 }} size={24} color={colors.green_main} onPress={() => navigation.goBack(null)} />}
                avatar={
                  <Mutation mutation={UPLOAD_FILE}>
                    {(singleUpload) => (
                      <View>
                        <TouchableOpacity onPress={() => this.selectPhotoTapped(singleUpload)}>
                          <Image style={styles.avatar} source={{ uri: NETWORK_INTERFACE_LINK_AVATAR + userData.foto_del_perfil }} />
                        </TouchableOpacity>
                      </View>
                    )}
                  </Mutation>}
                component={
                  <View style={styles.container}>
                    <Mutation
                      mutation={ACTUALIZAR_USUARIO} >
                      {(mutation) => {
                        return (
                          <View style={{marginTop: dimensions.Height(5)}}>
                            <KeyboardAwareScrollView keyboardShouldPersistTaps="always" style={{ marginBottom: 150 }}>
                              <CustomTextInput   
                              containerStyle={{ marginTop: dimensions.Height(1)}}
                              value={nombre} 
                              onChangeText={(nombre) => this.setState({ nombre })} 
                              name='nombre' 
                              keyboardType="default" 
                              placeholder="Nombres" />

                              <CustomTextInput   
                              containerStyle={{ marginTop: dimensions.Height(2)}}
                              value={apellidos} 
                              onChangeText={(apellidos) => this.setState({ apellidos })} 
                              name='apellidos' 
                              keyboardType="default" 
                              placeholder="Apellidos" />

                              <CustomTextInput   
                              containerStyle={{ marginTop: dimensions.Height(2)}}
                              value={email} 
                              onChangeText={(email) => this.setState({ email })} 
                              name='email' 
                              keyboardType="default" 
                              placeholder="Email" />

                              <CustomTextInput   
                              containerStyle={{ marginTop: dimensions.Height(2)}}
                              value={ciudad} 
                              onChangeText={(ciudad) => this.setState({ ciudad })} 
                              name='ciudad' 
                              keyboardType="default" 
                              placeholder="Ciudad" />

                              <CustomTextInput   
                              containerStyle={{ marginTop: dimensions.Height(2)}}
                              value={profesion} 
                              onChangeText={(profesion) => this.setState({ profesion })} 
                              name='profesion' 
                              keyboardType="default" 
                              placeholder="Profesión" />

                              <CustomTextInput   
                              containerStyle={{ marginTop: dimensions.Height(2)}}
                              value={telefono} 
                              onChangeText={(telefono) => this.setState({ telefono })} 
                              name='telefono' 
                              keyboardType="numeric" 
                              placeholder="Número móvil" />

                              <CustomTextInput   
                              containerStyle={{ marginTop: dimensions.Height(2)}}
                              value={descripcion} 
                              onChangeText={(descripcion) => this.setState({ descripcion })} 
                              name='descripcion' 
                              keyboardType="default" 
                              placeholder="Descripción" 
                              />
                              
                              <View style={{marginTop: dimensions.Width(3) }}>
                                <Button light={colors.white} dark={colors.white} containerStyle={styles.buttonView1}
                                  onPress={() => this._handleSubmit(mutation)}
                                  title="Guardar cambios"
                                  titleStyle={styles.buttonTitle} />
                              </View>
                            </KeyboardAwareScrollView>
                          </View>

                        )
                      }}
                    </Mutation>
                  </View>}

              />
            )
          }
        }}
      </Query>
    )
  }
}

const AVATAR_SIZE = 90;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    paddingHorizontal: dimensions.Width(4),
    marginBottom: 10
  },
  payment: {
    height: 'auto',
    alignSelf: 'center',
    width: dimensions.Width(90),
    backgroundColor: 'transparent',
    marginTop: 20,
    borderRadius: 10,
    paddingVertical: dimensions.Width(4),
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.2,
    elevation: 3,
  },
  buttonView: {
    marginTop: dimensions.Height(4),
    alignSelf: 'center',
    backgroundColor: colors.ERROR,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
  },
  buttonView1: {
    marginTop: dimensions.Height(3),
    alignSelf: 'center',
    backgroundColor: colors.blue_main,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(17),
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 75,
    width: 150,
    height: 150,
  },

  stickySectionText: {
    color: '#95ca3e',
    fontSize: 20,
    margin: 10,
    marginTop: 15
  },

  avatar: {
    marginBottom: 10,
    borderRadius: AVATAR_SIZE / 2,
    borderWidth: 2,
    borderColor: '#d6d7da',
    width: AVATAR_SIZE,
    height: AVATAR_SIZE
  },

});

export default Myaccount;