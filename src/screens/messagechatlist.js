import React from 'react';
import { View, ScrollView, StyleSheet, KeyboardAvoidingView, SafeAreaView, TouchableOpacity, Text, Image } from 'react-native';
import { colors, dimensions } from '../theme';
import { CustomText } from '../components/CustomText';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import AsyncStorage from '@react-native-community/async-storage';
import { Avatar } from 'react-native-elements';
import { NETWORK_INTERFACE_LINK_AVATAR, NETWORK_INTERFACE_LINK, NETWORK_INTERFACE } from './../constants/config';
import { GiftedChat, Send, Bubble, InputToolbar } from 'react-native-gifted-chat'
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { withApollo } from 'react-apollo';
import Iconsverifiel from 'react-native-vector-icons/Octicons';
import io from 'socket.io-client';
import  moment  from 'moment';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';

const USER_DETAIL = gql`
  query getUsuario($id: ID!) {
    getUsuario(id: $id) {
      id
      email
      nombre
      apellidos
      Verified
      ciudad
      telefono
      foto_del_perfil
      fotos_tu_dni
      profesion
      descripcion
      fecha_de_nacimiento
      notificacion
      grado
      estudios
      formularios_de_impuesto
      fb_enlazar
      twitter_enlazar
      instagram_enlazar
      youtube_enlazar
      propia_web_enlazar
      created_at
    }
  }
`;


class MessagePage extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            UserID: '',
            profesional: '',
            cliente: '',
            isconected: false,
            ultinahora: '',
            OneSignalUserID: '',
            receptor: ''
        }
    }

    renderBubble(props) {
        return (
          <Bubble
            {...props}
            textStyle={{
              right: {
                color: colors.black,
                fontWeight: '200'
              },
              left: {
                color: colors.black,
                fontWeight: '200'
              },
            }}
            wrapperStyle={{
              left: {
                backgroundColor: colors.rgb_235,
              },
              right: {
                backgroundColor: "#b0d36c",
              },
            }}
          />
        );
      }

    renderSend(props) {
        return (
            <Send
                {...props}
            >
                <View style={{ marginRight: 5, marginTop: 5 }}>
                    <Icons name="send-circle" size={40} color={colors.green_main} style={{ alignSelf: 'center' }} />
                </View>
            </Send>
        );
    }

    renderInputToolbar (props) {
      //Add the extra styles via containerStyle
     return <InputToolbar {...props} containerStyle={{borderTopWidth: 0.5, borderTopColor: colors.rgb_235, backgroundColor: 'transparent', color: colors.rgb_153}} />
   }

    componentDidMount = async() => {
      const { navigation } = this.props;
      const data = navigation.getParam('data');
      const id = await AsyncStorage.getItem('id');

        if(id === data.client[0].id) {
          this.setState({
            receptor: data.prfsnal[0].id
          })
        }else{
          this.setState({
            receptor: data.client[0].id
          })
        }
          this.setState({
            profesional: data.prfsnal[0].id,
            cliente: data.client[0].id,
            OneSignalUserID: data.client[0].UserID,
            UserID: id
          })      
         this.socket = io(NETWORK_INTERFACE_LINK, {
          forceNew: true
         });
         this.socket.on('connect',() => {
           this.socket.emit('online_user', {id, profesional: this.state.profesional, cliente: this.state.cliente})
         })
          this.socket.on('messages', input => {
              this.setState({ 
                messages: [...input.messagechat, ...this.state.messages]
              });
          })

          this.socket.on('message', datas =>{
            this.setState({
             isconected: datas.connected,
             ultinahora: datas.time
            })
          })

          }

    onSend = (messages = []) =>{
        this.setState(previousState => ({
          messages: GiftedChat.append(previousState.messages, messages),
        }))
        const input = { messagechat: [messages[messages.length - 1]]};//run
        this.socket.emit('private_message', { input, profesional: this.state.profesional, cliente: this.state.cliente, Userid: this.state.OneSignalUserID, receptor: this.state.receptor})
      }

    render() {
        const { navigation } = this.props;
        const data = navigation.getParam('data');
        const datos = data.prfsnal[0]
        if(this.state.UserID === datos.id){
        const datos = data.client[0]
        return (
          <ViewCuston light={colors.light_white} dark={colors.black} containers={
            <View style={styles.container}>
                <SafeAreaView style={styles.headers}>
                    <View style={{ flexDirection: 'row', marginTop: 20 }}>
                        <View style={{ alignItems: 'flex-start', marginLeft: 10 }}>
                            <TouchableOpacity onPress={() => navigation.goBack(null)} style={{ marginTop: 6 }}>
                                <CustomText>
                                    <IconsD name="left" size={20} light={colors.blue_main} dark={colors.white} style={{ alignSelf: 'center' }} />
                                </CustomText>
                            </TouchableOpacity>
                        </View>
                        <View style={{ alignSelf: 'center', marginLeft: 20, flexDirection: 'row' }}>
                            <Avatar
                                size="small"
                                rounded
                                source={{ uri: NETWORK_INTERFACE_LINK_AVATAR + datos.foto_del_perfil}}
                                StyleSheet={{ alignSelf: 'center' }}
                            />
                            <View style={{flexDirection: 'row'}}>
                                <CustomText light={colors.blue_main} dark={colors.white} style={{ textAlign: 'center', fontSize: dimensions.FontSize(18), marginLeft: 10, marginTop: 5 }}>{datos.nombre} {datos.apellidos}</CustomText>
                                 {
                                    datos.Verified ? 
                                    <Iconsverifiel name="verified" style={{ alignSelf: 'center', marginLeft: 5, marginTop: 2 }} size={12} color={colors.twitter_color} /> : null
                                 }
                            </View>
                            
                        </View>
                    </View>
                    <View style={{marginLeft: 100}}>
                    {
                      !this.state.isconected ? 
                      <Text style={{color: colors.rgb_153, fontWeight: '300'}}>En linea</Text> : null
                    }
                   {
                     this.state.isconected ? 
                     <Text style={{color: colors.rgb_153, fontWeight: '300'}}>{moment(this.state.ultinahora).startOf('hour').fromNow()}</Text> : null

                   }
                      
                    </View>
                </SafeAreaView>
                <Query query={USER_DETAIL} variables={{ id: this.state.UserID }}>
                    {(response, error, ) => {
                        if (error) {
                            return console.log('Response Error-------', error);
                        }
                        if (response) {
                            const DataUser = response && response.data ? response.data.getUsuario : ''
                            return (
                              <View style={{ flex: 1 }}>
                                <GiftedChat
                                    messages={this.state.messages}
                                    placeholder='Escribe algo ...'
                                    locale='ES'
                                    messageRead={true}
                                    messageDelivered={true}
                                    renderUsernameOnMessage={true}
                                    renderBoobleFooter={ ()=> {
                                      renderTime();
                                      renderTicks();
                                    } }
                                    showUserAvatar={true}
                                    renderAvatarOnTop={true}
                                    renderSend={this.renderSend}
                                    renderInputToolbar={this.renderInputToolbar}
                                    forceGetKeyboardHeight={true}
                                    textInputStyle={{color: colors.rgb_153}}
                                    onSend={messages => this.onSend(messages)}
                                    renderBubble={this.renderBubble}
                                    isTyping={true}
                                    user={{
                                        _id: DataUser.id,
                                        name: DataUser.nombre,
                                        avatar: NETWORK_INTERFACE_LINK_AVATAR + DataUser.foto_del_perfil
                                    }}
                                />
                              </View>
                            )
                        }
                    }}
                </Query>
            </View>}
            />
        )
      }else{
        const datos = data.prfsnal[0]
        return(
          <ViewCuston light={colors.light_white} dark={colors.black} containers={
          <View style={styles.container}>
                <SafeAreaView style={styles.headers}>
                    <View style={{ flexDirection: 'row', marginTop: 20 }}>
                        <View style={{ alignItems: 'flex-start', marginLeft: 10 }}>
                            <TouchableOpacity onPress={() => navigation.goBack(null)} style={{ marginTop: 6 }}>
                                <CustomText>
                                <IconsD name="left" size={20} light={colors.blue_main} dark={colors.white} style={{ alignSelf: 'center' }} />
                                </CustomText>
                            </TouchableOpacity>
                        </View>
                        <View style={{ alignSelf: 'center', marginLeft: 20, flexDirection: 'row' }}>
                            <Avatar
                                size="small"
                                rounded
                                source={{ uri: NETWORK_INTERFACE_LINK_AVATAR + datos.foto_del_perfil}}
                                StyleSheet={{ alignSelf: 'center' }}
                            />
                            <View style={{flexDirection: 'row'}}>
                                <CustomText light={colors.blue_main} dark={colors.white} style={{ textAlign: 'center', fontSize: dimensions.FontSize(18), marginLeft: 10, marginTop: 5 }}>{datos.nombre} {datos.apellidos}</CustomText>
                                 {
                                    datos.Verified ? 
                                    <Iconsverifiel name="verified" style={{ alignSelf: 'center', marginLeft: 5, marginTop: 2 }} size={12} color={colors.twitter_color} /> : null
                                 }
                            </View>
                            
                        </View>
                    </View>
                    <View style={{marginLeft: 100}}>
                    {
                      !this.state.isconected ? 
                      <Text style={{color: colors.rgb_153, fontWeight: '300'}}>En linea</Text> : null
                    }
                   {
                     this.state.isconected ? 
                     <Text style={{color: colors.rgb_153, fontWeight: '300'}}>{moment(this.state.ultinahora).startOf('hour').fromNow()}</Text> : null

                   }
                      
                    </View>
                </SafeAreaView>
                <Query query={USER_DETAIL} variables={{ id: this.state.UserID }}>
                    {(response, error, ) => {
                        if (error) {
                            return console.log('Response Error-------', error);
                        }
                        if (response) {
                            const DataUser = response && response.data ? response.data.getUsuario : ''
                            return (
                              <View style={{ flex: 1 }}>
                                <GiftedChat
                                    messages={this.state.messages}
                                    placeholder='Escribe algo ...'
                                    locale='ES'
                                    messageRead={true}
                                    messageDelivered={true}
                                    renderBoobleFooter={ ()=> {
                                      renderTime();
                                      renderTicks();
                                    } }
                                    showUserAvatar={true}
                                    renderAvatarOnTop={true}
                                    renderSend={this.renderSend}
                                    onSend={messages => this.onSend(messages)}
                                    renderBubble={this.renderBubble}
                                    renderInputToolbar={this.renderInputToolbar} 
                                    forceGetKeyboardHeight={true}
                                    textInputStyle={{color: colors.rgb_153}}
                                    isTyping={true}
                                    user={{
                                        _id: DataUser.id,
                                        name: DataUser.nombre,
                                        avatar: NETWORK_INTERFACE_LINK_AVATAR + DataUser.foto_del_perfil
                                    }}
                                />
                              </View>
                            )
                        }
                    }}
                </Query>
            </View>}
            />
        )
      }
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        height: dimensions.Height(100)
    },
    headers: {
        height: dimensions.Height(15),
        backgroundColor: 'transparent',
        borderBottomWidth: 0.5,
        borderBottomColor: colors.rgb_235

    },
})


export default withApollo(MessagePage);