import React, { Component } from 'react';
import { View, Image, ImageBackground, StyleSheet, Alert, Linking, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import { colors, dimensions } from './../theme';
import { CustomTextInput } from './../components/CustomTextInput';
import { CustomText } from './../components/CustomText';
import { Button } from './../components/Button';
import { LoginButton, AccessToken } from 'react-native-fbsdk';
import { LoginManager,GraphRequest,GraphRequestManager } from "react-native-fbsdk";
import AsyncStorage from '@react-native-community/async-storage';

import TwitterButton from './../services/TwitterLogin';
import { GoogleSignin, GoogleSigninButton } from '@react-native-community/google-signin';
GoogleSignin.configure({
  iosClientId: '342095203350-olearqs7fsekumrj454abl02aslhn764.apps.googleusercontent.com', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
});
import { Mutation,withApollo } from 'react-apollo';
import gql from 'graphql-tag';

import { user, image } from './../constants';
import { axiosPost } from '../services/networkRequestService';
import { NETWORK_INTERFACE_LINK } from '../constants/config';
import { store } from '../redux/store';
import { SET_USER } from '../redux/type';
import { ViewCuston } from '../components/CustonView';
import {Logo} from '../constants/logo';


const AUTENTICA_USUARIO = gql`
mutation autenticarUsuario($email: String!, $password: String!) {
  autenticarUsuario(email: $email, password: $password) {
    success
    message
    data {
      token
      id
    }
  }
}`

class LoginScreen extends Component {
  constructor(props){
    super(props)
    this.state = {
      rememberMe: true,
      email:'',
      password:'',
      secureTextEntry: true
    }
  }
  _signIn = () => {
    
  };

  googleSignIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      //alert(userInfo.user.email)
      
      let firstName = userInfo.user.givenName
      let lastName = userInfo.user.familyName
      let email = userInfo.user.email
      let token = userInfo.user.id
      this.socialLogin({firstName,lastName,email,token})

    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  facebookLogin = () =>{
    let self = this
    LoginManager.logInWithPermissions(["public_profile","email"]).then(
      function(result) {
        if (result.isCancelled) {
          console.log("Login cancelled");
        } else {
          console.log(
            "Login success with permissions: " +
              result.grantedPermissions.toString()
          );
          
          const responseInfoCallback = (error, result) => {
            if (error) {
              console.log(error)
              alert('Facebook request failed. Try Again.')
            } else {
              //alert(JSON.stringify(result))
              self.getFacebookUserInfo(result)
            }
          }

              const infoRequest = new GraphRequest(
                '/me?fields=email,name,picture.height(480)',
                null,
                responseInfoCallback,
              );
              // Start the graph request.
              console.log(
                "start  graph request" 
              );
              new GraphRequestManager().addRequest(infoRequest).start();
            
        }
      },
      function(error) {
        console.log("Login fail with error: " + error);
      }
    );


  }

  getFacebookUserInfo = (result) =>{
    console.log(JSON.stringify(result))
    let name  = result.name.split(' ')
    let firstName = name[0]
    let lastName = name[1] || ''
    let email = result.email
    let token = result.id
    this.socialLogin({firstName,lastName,email,token})
  }


  socialLogin = (postData) =>{
    axiosPost(`${NETWORK_INTERFACE_LINK}/api/v1/auth/social/mobile`,postData).then(res=>{
      let email = res.data.nuevoUsuario.email
      let password = res.data.token
      this.props.client
      .mutate({
          mutation: AUTENTICA_USUARIO,
          variables: {
              // Your variables here
              email,
              password
          }
      })
      .then(async (results)=> {
        await AsyncStorage.setItem('token', results.data.autenticarUsuario.data.token);
        await AsyncStorage.setItem('id', results.data.autenticarUsuario.data.id);
        store.dispatch({type:SET_USER,payload:results.data.autenticarUsuario.data})
        this.props.navigation.navigate('Home')
      })
      .catch(error => {
        alert(`error : ${error}`);
      })
      //this.setState({usuario,password})
    }).catch(err=>{
      alert(JSON.stringify(err))
    })
  }

  actualizarState = (name, value) => {
    this.setState({
      [name]: value
    });
  };

  onCompletedLogin = async (res) => {
    const {
      email,
      password,
    } = this.state

    if (password=='', email==''){
      Alert.alert(
        'Error con el inicio de sesión',
        'Todos los campos son obligatorio para el inicio de sesión',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
        )
        return null
    }  
    console.log('res: ', res)
    if(!res.autenticarUsuario.success) {
      
      Alert.alert(
        'Error con el Inicio de sesión',
        (res.autenticarUsuario.message),
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
        ),
        this.actualizarState('email',  "")
        this.actualizarState('password',  "")
        return ;
      }
        await AsyncStorage.setItem('token', res.autenticarUsuario.data.token);
        await AsyncStorage.setItem('id', res.autenticarUsuario.data.id);
        this.actualizarState('email',  "")
        this.actualizarState('password',  "")
        store.dispatch({type:SET_USER,payload:res.autenticarUsuario.data})
        this.props.navigation.navigate('Home');
      }

  render() {
    const { email, password ,secureTextEntry} = this.state
    return  <Mutation mutation={AUTENTICA_USUARIO} variables={{ email, password }} onCompleted={this.onCompletedLogin}>
            {(mutation) => {

          return(
            <ViewCuston light={colors.white} dark={colors.black} containers={
                  <View style={styles.container}>
                    <KeyboardAwareScrollView  keyboardShouldPersistTaps="always">
                      <ImageBackground source={image.img_map} resizeMode="contain" style={{justifyContent:'center',alignItems:'center',height:dimensions.Height(30)}}> 
                        <Logo />
                      </ImageBackground>
                      <View style={styles.formView}>
                        
                        <CustomTextInput placeholderTextColor={colors.rgb_153} containerStyle={{color: colors.black}} autoCompleteType="email" autoCapitalize={"none"}  onPress={()=>{}} left={image.ic_email} value={email} onChangeText={(email)=>this.setState({email})} keyboardType="email-address" placeholder="Correo electrónico"/>
                        <CustomTextInput placeholderTextColor={colors.rgb_153} onPress={()=>this.setState({secureTextEntry:!secureTextEntry})} left={image.ic_eye} containerStyle={{marginTop:dimensions.Height(3.5), color: colors.black}} value={password} onChangeText={(password)=>this.setState({password})} secureTextEntry={secureTextEntry} placeholder="Contraseña"/>
                        <CustomText light={colors.blue_main} dark={colors.white} style={{marginTop:dimensions.Height(2.5),alignSelf:'flex-end',fontSize:dimensions.FontSize(14),fontWeight:'500'}} onPress={()=>this.props.navigation.navigate('Forgot')} >¿Has olvidado tu contraseña?</CustomText>
                      
                        <View style={styles.signupButtonContainer}>
                          <Button light={colors.white} dark={colors.white}  containerStyle={styles.buttonView} 
                                    onPress={mutation} title="Iniciar sesión" 
                                    titleStyle={styles.buttonTitle}/>
                        </View>
                        <CustomText style={{marginTop:dimensions.Height(3),fontSize:dimensions.FontSize(14),color:colors.slight_black,alignSelf:'center'}}>
                          ¿Aún no tienes una cuneta? <CustomText light={colors.blue_main} dark={colors.white} onPress={()=>this.props.navigation.navigate('Register')} style={{fontSize:dimensions.FontSize(14),fontWeight:'500'}}>Regístrarme</CustomText>
                        </CustomText>
                        <View style={{flex:1,flexDirection:'row',alignItems:'center',marginTop:dimensions.Height(5)}}>  
                          <View style={{backgroundColor:colors.light_white,flex:1,borderBottomWidth:dimensions.Height(0.05),bottomBorderColor:colors.light_white}}></View>
                          <View style={{flex:1,justifyContent:'center'}}><CustomText style={{alignSelf:'center',fontSize:dimensions.FontSize(13),color:colors.slight_black}}>O inicia sesión con</CustomText></View>
                          <View style={{backgroundColor:colors.light_white,flex:1,borderBottomWidth:dimensions.Height(0.05),bottomBorderColor:colors.light_white}}></View>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-evenly',flex:1,marginTop:dimensions.Height(2)}}>
                            <TouchableOpacity onPress={()=>this.facebookLogin()} style={{flexDirection:'row',flex:0.4,borderRadius:dimensions.Width(8),backgroundColor:colors.fb_color}}>
                            <View style={{flexDirection:'row',paddingVertical:dimensions.Height(2),}}>
                              <Image resizeMode="contain" style={{marginLeft:dimensions.Width(4)}} source={image.ic_facebook}/><CustomText style={{paddingLeft:dimensions.Width(2),fontWeight:'bold',color:colors.white,fontSize:dimensions.FontSize(13)}}>Facebook</CustomText>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>this.googleSignIn()} style={{flexDirection:'row',flex:0.4,borderRadius:dimensions.Width(8),backgroundColor:colors.g_color, justifyContent:'center',alignItems:'center', marginLeft: 10, marginRight: 10}}>
                              <View style={{flexDirection:'row',paddingVertical:dimensions.Height(2),}}>
                                <CustomText style={{paddingLeft:dimensions.Width(2),fontWeight:'bold',color:colors.white,fontSize:dimensions.FontSize(13)}}>Google</CustomText>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={{flexDirection:'row',flex:0.4,borderRadius:dimensions.Width(8),backgroundColor:colors.twitter_color,justifyContent:'center',alignItems:'center'}}>
                            <TwitterButton onSuccess={(data)=>this.socialLogin(data)}/>
                            </TouchableOpacity>

                        </View>

                      </View>
                    </KeyboardAwareScrollView>
                  </View>}
                  />
        );
      }
    }
        </Mutation >
  }
}

export default connect(
  null,
  {  }
)(withApollo(LoginScreen));

const styles = StyleSheet.create({
  container:{
    backgroundColor: 'transparent',
    height: dimensions.Height(100)
  },
  formView:{
    marginHorizontal:dimensions.Width(8),
    flex:1,
    marginTop:dimensions.Height(4)
  },
  rememberMeView:{
    marginTop:dimensions.Height(2),
    flexDirection:'row'
  },
  rememberText:{
    fontSize:dimensions.FontSize(18)
  },
  signupButtonContainer:{
    marginTop:dimensions.Height(5),
    alignSelf:'center'
  },
  buttonView:{
    backgroundColor:colors.light_blue,
    width:dimensions.Width(84),
    borderRadius:dimensions.Width(8),
  },
  buttonTitle:{
    alignSelf:'center',
    paddingVertical:dimensions.Height(2),
    paddingHorizontal:dimensions.Width(5),
    color:colors.white,
    fontWeight:'bold',
    fontSize:dimensions.FontSize(17),
  }
})