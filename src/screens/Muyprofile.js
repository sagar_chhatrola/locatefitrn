import React from 'react';
import { View, TouchableOpacity, StyleSheet, Text, Image, Share, Alert, ImageBackground } from 'react-native';
import { colors, dimensions } from '../theme';
import { image } from './../constants/image';
import { CustomText } from '../components/CustomText'
import AntdIcons from 'react-native-vector-icons/AntDesign';
import Link from './../components/Link3';
import 'moment/locale/es';
import moment, { now } from "moment";
import AsyncStorage from '@react-native-community/async-storage';
import Iconsverifiel from 'react-native-vector-icons/Octicons';
import { NETWORK_INTERFACE_LINK_AVATAR } from './../constants/config';
import MaterialCommunityIcons from 'react-native-vector-icons/Feather';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { Button } from './../components/Button';
import { Badge } from '@ant-design/react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { SliderBox } from "react-native-image-slider-box";
import Header from './../components/Header';
import { Avatar } from 'react-native-elements';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';
import {Logo} from '../constants/logo';


const USER_DETAIL = gql`
  query getUsuario($id: ID!) {
    getUsuario(id: $id) {
      email
      nombre
      apellidos
      Verified
      ciudad
      telefono
      foto_del_perfil
      fotos_tu_dni
      profesion
      descripcion
      fecha_de_nacimiento
      notificacion
      grado
      estudios
      formularios_de_impuesto
      fb_enlazar
      twitter_enlazar
      instagram_enlazar
      youtube_enlazar
      propia_web_enlazar
      created_at
    }
  }
`;

const USUARIO_FAVORITO_INSPIRATION = gql`
    query{
        getUsuarioFavoritoInspiracion{
          success
          message
          list{
            id
            InspiracionID
            inspiracion{
              id
              title
              description
              image
              image1
              image2
              created_at
              anadidoFavoritoinspiration
            }
          }
        }
      }
      `;


const GET_MESSAGE_CHAT_NO_READ = gql`
query getMessageNoread($UserID: ID) {
  getMessageNoread (UserID: $UserID){
    message
    success
    messagechats{
      _id
    }
  }
    }
`;


const dataList = [
  {
    navegations: 'MyAccount',
    icon: 'user',
    icon1: 'right',
    text: 'Mi cuenta'
  },
  {
    navegations: 'Chatscreen',
    icon: 'message1',
    icon1: 'right',
    text: 'Mensajes'
  },
  {
    navegations: 'Address',
    icon: 'enviromento',
    icon1: 'right',
    text: 'Mi dirección'
  },
  {
    navegations: 'Mypayment',
    icon: 'creditcard',
    icon1: 'right',
    text: 'Mis pagos'
  },
  {
    navegations: 'Coments',
    icon: 'staro',
    icon1: 'right',
    text: 'Opiniones'
  },
  {
    navegations: 'Myadds',
    icon: 'notification',
    icon1: 'right',
    text: 'Mis anuncios'
  },
  {
    navegations: 'MayInspiration',
    icon: 'picture',
    icon1: 'right',
    text: 'Mis inspiraciones'
  },
]

const dataList1 = [
  {
    navegations: 'Pedido',
    icon: 'file1',
    icon1: 'right',
    text: 'Mi Pedidos'
  },
  {
    navegations: 'Orders',
    icon: 'calendar',
    icon1: 'right',
    text: 'Mis ordenes'
  },
 
]


export default class Configuration extends React.PureComponent {

  constructor(props) {
    super(props)
    this.state = {
      images: [
        require('./../assets/images/barner1.png'),
        require('./../assets/images/Barner2png.png'),
        require('./../assets/images/barner3.png'),
      ]
    };
  }

  state = { loggedIn: false, userId: '', countchat: '' }

  componentDidMount = async () => {
    const token = await AsyncStorage.getItem('token');
    const id = await AsyncStorage.getItem('id');
    if (token) {
      this.setState({ loggedIn: true, userId: id })
    }
  }
  async componentDidUpdate() {
    const token = await AsyncStorage.getItem('token');
    const id = await AsyncStorage.getItem('id');
    if (token) {
      this.setState({ loggedIn: true, userId: id })
    } else {
      this.setState({ loggedIn: false })
    }
  }

  logout = async () => {
    await AsyncStorage.removeItem('token')
    await AsyncStorage.removeItem('id')
    this.props.navigation.navigate('Landing')
  }

  onShare = async () => {
    try {
      const result = await Share.share({
        title: 'Locatefit',
        message:
          'Locatefit | Los mejores profeisonales',

      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };


  render() {
    const { navigation } = this.props;
    const { loggedIn, userId } = this.state;
    const data = navigation.getParam('data')
    if (!loggedIn) {
      return (
        <ViewCuston light={colors.white} dark={colors.black} containers={
        <View style={styles.container}>
          <KeyboardAwareScrollView keyboardShouldPersistTaps="always" showsVerticalScrollIndicator={false}>
            <View style={{ marginTop: dimensions.Height(10) }}>
              <SliderBox
                images={this.state.images}
                sliderBoxHeight={300}
                autoplay
                disableOnPress={false}
                circleLoop={true}
                dotColor={colors.green_main}
                imageLoadingColor={colors.green_main}
                dotStyle={{
                  width: 50,
                  height: 10,
                  borderRadius: 5,
                  marginHorizontal: 0,
                  padding: 0,
                  margin: 0,
                }}
              />
            </View>
            <ImageBackground source={image.img_map} resizeMode="contain" style={{ justifyContent: 'center', alignItems: 'center', height: dimensions.Height(30) }}>
              <CustomText light={colors.blue_main} dark={colors.white} style={{fontSize: dimensions.FontSize(28), fontWeight: 'bold' }}>Los mejores profesionales</CustomText>
              <CustomText style={{ marginTop: dimensions.Height(1), fontSize: dimensions.FontSize(20), color: colors.slight_black, alignSelf: 'center' }}>
                Tu eliges la fecha, hora y lugar
              </CustomText>
              <CustomText style={{ marginTop: dimensions.Height(1), fontSize: dimensions.FontSize(16), color: colors.rgb_153, alignSelf: 'center', paddingHorizontal: 20, textAlign: 'center', }}>
                En Locatefit nuestro principal objetivo es ofrecerte los mejores profesionales al menor precio, para que te ayuden con las tareas del día a día.
              </CustomText>
            </ImageBackground>
            <View style={styles.formView1}>
              <View style={styles.signupButtonContainer}>
                <Button light={colors.white} dark={colors.white} containerStyle={styles.buttonView}
                  onPress={() => this.props.navigation.navigate('Home')} title="Seguir explorando"
                  titleStyle={styles.buttonTitle} />
              </View>
              <View style={styles.signupButtonContainer}>
                <Button light={colors.white} dark={colors.white} containerStyle={styles.buttonView}
                  onPress={() => this.props.navigation.navigate('Login')} title="Iniciar sesión"
                  titleStyle={styles.buttonTitle} />
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>}
        />
      )
    } else {
      return (
        <Query query={USER_DETAIL} variables={{ id: userId }}>
          {(response, error,) => {
            if (error) {
              return console.log('Response Error-------', error);
            }
            if (response) {
              const dataUser = response && response.data ? response.data.getUsuario : ''
              const profileLink = NETWORK_INTERFACE_LINK_AVATAR + dataUser.foto_del_perfil;
              const FechaIni = dataUser.created_at;
              return (
                <ViewCuston light={colors.white} dark={colors.black} containers={
                  <View style={styles.container}>
                  <View style={{ paddingHorizontal: dimensions.Width(3) }}>
                    <Header navigation={navigation} />
                  </View>
                  <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
                    <View style={{ flexDirection: 'row' }}>
                      <View>
                        <Avatar
                          size="large"
                          rounded
                          source={{ uri: profileLink }}
                          containerStyle={{ borderWidth: 3, borderColor: colors.rgb_235, marginTop: 15 }}
                        />
                      </View>
                      <View>
                        <View style={{ flexDirection: 'row' }}>
                          <CustomText light={colors.blue_main} dark={colors.white} style={{ fontSize: dimensions.FontSize(24), marginLeft: 20, marginTop: 10, fontWeight: 'bold' }}>
                            {dataUser.nombre} {dataUser.apellidos}
                          </CustomText>
                          {
                            dataUser.Verified ?
                              <Iconsverifiel name="verified" style={{ alignSelf: 'center', marginLeft: 5, marginTop: 15 }} size={12} color={colors.twitter_color} />
                              : null

                          }
                        </View>
                        <Text numberOfLines={3} style={{ width: dimensions.Width(70), fontSize: dimensions.FontSize(14), marginLeft: 20, color: colors.rgb_153, fontWeight: '200' }}>
                          {dataUser.descripcion}
                        </Text>
                      </View>
                    </View>
                    <Text style={{ fontSize: 14, fontWeight: '200', color: colors.rgb_153, marginLeft: 95, marginTop: 10 }}>
                      Miembro desde  {moment(Number(FechaIni)).format('ll')}
                    </Text>
                    <View style={{ width: dimensions.Width(90), borderTopWidth: 0.5, borderBottomWidth: 0.5, marginTop: 20, borderTopColor: colors.light_grey, borderBottomColor: colors.light_grey, height: dimensions.Height(8), flexDirection: 'row', alignSelf: 'center' }}>
                      <CustomText style={{ alignSelf: 'center', color: colors.rgb_153, fontSize: dimensions.FontSize(14), fontWeight: '300' }}>
                        Sobre el profeisonal:
                    </CustomText>
                      <View style={{ alignSelf: 'center', marginLeft: 30 }}>
                        <CustomText light={colors.blue_main} dark={colors.white} style={{ alignSelf: 'center', fontSize: dimensions.FontSize(24), fontWeight: '500' }}>
                          {dataUser.grado}
                        </CustomText>
                        <CustomText style={{ alignSelf: 'center', color: colors.rgb_153, fontSize: dimensions.FontSize(14), fontWeight: '200' }}>
                          Experiencia
                        </CustomText>
                      </View>
                      <View style={{ alignSelf: 'center', marginLeft: 30 }}>
                        <CustomText light={colors.blue_main} dark={colors.white} style={{ alignSelf: 'center', fontSize: dimensions.FontSize(24), fontWeight: '500' }}>
                          {dataUser.ciudad}
                        </CustomText>
                        <CustomText style={{ alignSelf: 'center', color: colors.rgb_153, fontSize: dimensions.FontSize(14), fontWeight: '200' }}>
                          Ciudad
                        </CustomText>
                      </View>
                    </View>
                    <View style={styles.container1}>
                      <Text style={{ color: colors.rgb_153, fontWeight: '300', marginBottom: 20, marginTop: 30 }}>Cuenta</Text>
                      {
                        dataList.map((dat, i) =>(
                            <ViewCuston key={dat.navegations} light={colors.light_white} dark={colors.back_dark} style={{marginBottom: 20, borderRadius: 8}} containers={
                              <TouchableOpacity onPress={() => this.props.navigation.navigate(dat.navegations, { data: data })} style={{ flexDirection: 'row', height: dimensions.Height(5), backgroundColor: 'transparent', alignItems: 'center' }}>
                                <Text style={{marginLeft: 15}}>
                                  <IconsD name={dat.icon} size={14} light={colors.blue_main} dark={colors.white}/>
                                </Text>
                                <CustomText light={colors.black} dark={colors.white} style={styles.textlink}>{dat.text}</CustomText>
                                <Text style={{marginLeft: 'auto', marginRight: 15}}>
                                  <IconsD name={dat.icon1} size={14} light={colors.blue_main} dark={colors.white}/>
                                </Text>
                              </TouchableOpacity>} />
                          )
                        )
                      }
                    
                      <Text style={{ color: colors.rgb_153, fontWeight: '300', marginBottom: 20 }}>Transacciones</Text>

                      {
                        dataList1.map((dat, i) =>(
                            <ViewCuston key={dat.navegations} light={colors.light_white} dark={colors.back_dark} style={{marginBottom: 20, borderRadius: 8}} containers={
                              <TouchableOpacity onPress={() => this.props.navigation.navigate(dat.navegations, { data: data })} style={{ flexDirection: 'row', height: dimensions.Height(5), backgroundColor: 'transparent', alignItems: 'center' }}>
                                <Text style={{marginLeft: 15}}>
                                  <IconsD name={dat.icon} size={14} light={colors.blue_main} dark={colors.white}/>
                                </Text>
                                <CustomText light={colors.black} dark={colors.white} style={styles.textlink}>{dat.text}</CustomText>
                                <Text style={{marginLeft: 'auto', marginRight: 15}}>
                                  <IconsD name={dat.icon1} size={14} light={colors.blue_main} dark={colors.white}/>
                                </Text>
                              </TouchableOpacity>} />
                          )
                        )
                      }

                      <Text style={{ color: colors.rgb_153, fontWeight: '300', marginBottom: 20 }}>Profesional</Text>

                      <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{marginBottom: 20, borderRadius: 8}} containers={
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('')} style={{ flexDirection: 'row', height: dimensions.Height(5), backgroundColor: 'transparent', alignItems: 'center' }}>
                          <Text style={{marginLeft: 15}}>
                            <IconsD name="pluscircleo" size={14} light={colors.blue_main} dark={colors.white}/>
                          </Text>
                          <View style={{ marginLeft: 10 }}><Link light={colors.white} dark={colors.black} url="https://locatefit.es/profile" text="Publicar un servicio" /></View>
                          <Text style={{marginLeft: 'auto', marginRight: 15}}>
                            <IconsD name="right" size={14} light={colors.blue_main} dark={colors.white}/>
                          </Text>
                        </TouchableOpacity>} />

                      <Text style={{ color: colors.rgb_153, fontWeight: '300', marginBottom: 20 }}>Privacidad</Text>

                      <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{marginBottom: 20, borderRadius: 8}} containers={
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("Configuration")} style={{ flexDirection: 'row', height: dimensions.Height(5), backgroundColor: 'transparent', alignItems: 'center' }}>
                          <Text style={{marginLeft: 15}}>
                            <IconsD name="setting" size={14} light={colors.blue_main} dark={colors.white}/>
                          </Text>
                          <CustomText light={colors.black} dark={colors.white} style={styles.textlink}>Configuración y privacidad</CustomText>
                          <Text style={{marginLeft: 'auto', marginRight: 15}}>
                            <IconsD name="right" size={14} light={colors.blue_main} dark={colors.white}/>
                          </Text>
                        </TouchableOpacity>} />


                        <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{marginBottom: 20, borderRadius: 8}} containers={
                            <TouchableOpacity onPress={() => Alert.alert(
                              'Cerrar sesión',
                              '¿Seguro que quieres cerrar sesión?',
                              [
                                {
                                  text: 'Cancelar',
                                  onPress: () => console.log('Cancel Pressed'),
                                  style: 'cancel',
                                },
                                { text: 'Sí', onPress: () => this.logout() },
                              ],
                              { cancelable: false })}>
                              <View style={{ flexDirection: 'row', height: dimensions.Height(5), alignItems: 'center'}}>
                                <AntdIcons name="logout" style={{ marginLeft: 10 }} size={14} color={colors.ERROR} />
                                <CustomText light={colors.black} dark={colors.white} style={styles.textlink}>Cerrar sesión</CustomText>
                                <Text style={{marginLeft: 'auto', marginRight: 15}}>
                                  <IconsD name="right" size={14} light={colors.blue_main} dark={colors.white}/>
                                </Text>
                              </View>
                            </TouchableOpacity>} />

                      <View style={{ alignSelf: 'center', marginTop: dimensions.Height(3), marginBottom: dimensions.Height(8) }}>
                        <Logo />
                        <Text style={{ color: colors.rgb_153, fontWeight: '300', marginTop: 10, textAlign: 'center', fontSize: dimensions.FontSize(18) }}>Profesionales</Text>
                      </View>
                    </View>
                  </KeyboardAwareScrollView>
                </View>
                }
                />
              )
            }
          }}
        </Query>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: dimensions.Width(2),
    height: dimensions.Height(100)
  },
  container1: {
    paddingHorizontal: dimensions.Width(1)
  },
  formView: {
    marginHorizontal: dimensions.Width(6),
    marginTop: dimensions.Height(8),
    flexDirection: 'row',
  },
  formView1: {
    marginHorizontal: dimensions.Width(6),
    marginTop: dimensions.Height(2),
    marginBottom: dimensions.Height(15),
  },
  rememberMeView: {
    marginTop: dimensions.Height(2),
    flexDirection: 'row'
  },
  rememberText: {
    fontSize: dimensions.FontSize(18)
  },
  signupButtonContainer: {
    marginTop: dimensions.Height(2),
    alignSelf: 'center'

  },
  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(60),
    borderRadius: dimensions.Width(8),
    marginHorizontal: dimensions.Width(6),
    marginTop: dimensions.Height(2),

  },

  buttonView11: {
    backgroundColor: colors.green_main,
    width: dimensions.Width(60),
    borderRadius: dimensions.Width(8),
    marginHorizontal: dimensions.Width(6),
    marginTop: dimensions.Height(2),

  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1.5),
    paddingHorizontal: dimensions.Width(3),
    color: colors.white,
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(17),
  },

  stickySectionText: {
    color: colors.blue_main,
    fontSize: 20,
    margin: 10,
    marginTop: 15
  },

  textlink: {
    fontSize: dimensions.FontSize(16),
    fontWeight: '300',
    marginLeft: 10
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(17),
  },
});