import React from 'react';
import { View, StyleSheet, Alert, FlatList, Image, TouchableOpacity } from 'react-native'
import { colors, dimensions } from './../theme';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { CustomText } from './../components/CustomText';
import { image } from '../constants/image';
import AsyncStorage from '@react-native-community/async-storage';
import { NETWORK_INTERFACE_LINK_AVATAR } from '../constants/config';
import moment from 'moment';
import { Avatar } from 'react-native-elements';
import { Badge } from '@ant-design/react-native';
import Header from './../components/Header';
import { ActivityIndicator } from '@ant-design/react-native';
import { ViewCuston } from '../components/CustonView';

const GET_NOTIFICATIONS = gql`
query getNotifications($userId: ID!) {
  getNotifications(userId: $userId) {
      success
      message
      notifications{
        _id
        type
        read
        createdAt
        user{
          id
          nombre
          apellidos
          foto_del_perfil
        }
        profesional{
          id
          nombre
          apellidos
          foto_del_perfil
        }
        cliente{
          id
          nombre
          apellidos
          foto_del_perfil
        }
        orden{
          id
        }
      }
  }
}
`;

const READ_NOTIFICATION = gql`
  mutation readNotification($notificationId: ID!) {
    readNotification(notificationId: $notificationId) {
      success
      message
    }
  }
`;


class Notification extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            refreshing: false
        };
    }

    refetch = null

    async componentDidMount() {
        const userId = await AsyncStorage.getItem('id');
        this.setState({ userId })
    }

    _onRefresh = () => {
        this.setState({ refreshing: true });
        setTimeout(() => {
            this.setState({
                refreshing: false
            })
        }, 2000)
    }

    _renderItem(item, refetch) {
        let title = ''
        let description = ''
        let avatar = ''
        switch (item.type) {
            case 'new_order':
                title = 'Nueva orden'
                description = `Enhorabuena ha recibido una nueva orden de ${item.cliente.nombre} ${item.cliente.apellidos}.`
                avatar = NETWORK_INTERFACE_LINK_AVATAR +
                    item.cliente.foto_del_perfil
                break;
            case 'resolution_order':
                title = 'Orden en resolución'
                description = `${item.cliente.nombre} ${item.cliente.apellidos} ha reportado un problema con la orden estamos trabajando para solucionarlo.`
                avatar = NETWORK_INTERFACE_LINK_AVATAR +
                    item.cliente.foto_del_perfil
                break;
            case 'valored_order':
                title = 'Orden valorada'
                description = `Enhorabuena ${item.cliente.nombre} ${item.cliente.apellidos} ha valorado tu trabajo.`
                avatar = NETWORK_INTERFACE_LINK_AVATAR +
                    item.cliente.foto_del_perfil
                break;
            case 'accept_order':
                title = 'Orden Aceptada'
                description = `Genial ${item.profesional.nombre} ${item.profesional.apellidos} ha aceptado la orden.`
                avatar = NETWORK_INTERFACE_LINK_AVATAR +
                    item.profesional.foto_del_perfil
                break;
            case 'reject_order':
                title = 'Orden rechazada'
                description = `Opps ${item.profesional.nombre} ${item.profesional.apellidos} ha rechazado la orden.`
                avatar = NETWORK_INTERFACE_LINK_AVATAR +
                    item.profesional.foto_del_perfil
                break;
            case 'finish_order':
                title = 'Orden finalizada'
                description = `Genial ${item.profesional.nombre} ${item.profesional.apellidos} ha finalizado la orden es hora de valorarlo.`
                avatar = NETWORK_INTERFACE_LINK_AVATAR +
                    item.profesional.foto_del_perfil
        }
        return (
            <Mutation mutation={READ_NOTIFICATION} variables={{ notificationId: item._id }}>
                {(readNotification, data) => {
                    return (
                        <TouchableOpacity style={{alignSelf: 'center', marginTop: 20, width: '95%'}}
                        onPress={() => {
                            readNotification({ variables: { notificationId: item._id } }).then(() => {
                                refetch()
                                if (
                                    data &&
                                    data.readNotification &&
                                    data.readNotification.success
                                ) {
                                  console.log(data.readNotification.message);
                                } else if (
                                    data &&
                                    data.readNotification &&
                                    !data.readNotification.success
                                )
                                  console.log(data.readNotification.message)
                              }
                            );
                          }
                          }>
                          <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{borderRadius: 10}}containers={
                          <View style={{height: 'auto', padding: 10, shadowColor: colors.rgb_153,
                          shadowOffset: { width: 0, height: 1 },
                          shadowRadius: 10,
                          shadowOpacity: 0.4,
                          elevation: 5,}}>
                            <View style={{flexDirection: 'row', width: '90%',}}>
                            <View style={{justifyContent: 'center', alignSelf: 'center', marginRight: 20}}>
                            <Avatar
                            size='medium'
                            containerStyle={{borderWidth: 2, borderColor: colors.rgb_235}}
                            rounded
                            source={{ uri: avatar }}
                        />
                            </View>
                            <View style={{width: '95%'}}>
                                <View style={{flexDirection: 'row',width: '83%'}}>
                                    <CustomText light={colors.blue_main} dark={colors.white} style={{fontWeight: '300', fontSize: dimensions.FontSize(16)}}>
                                    {title}
                                    </CustomText>
                                    <Badge style={{marginLeft: 'auto', marginTop: 10}} text='Nueva' />
                                </View>
                                <CustomText style={{fontWeight: '200', color: colors.rgb_153, width: '90%', marginTop: 20}}>
                                    {description}
                                </CustomText>
                                <View style={{flexDirection: 'row', marginTop: 10}}>
                                    <CustomText style={{marginRight: 15, color: colors.rgb_235}}>
                                        <AntdIcons name="clockcircleo" style={{ marginLeft: 'auto' }} size={14} color={colors.rgb_235} />  {moment(Number(item.createdAt)).fromNow()}
                                    </CustomText>
                                    <CustomText style={{color: colors.rgb_235}}>
                                        <AntdIcons name="eye" style={{ marginLeft: 'auto' }} size={14} color={colors.rgb_235} /> Leer
                                </CustomText>
                                </View>
                            </View>
                            </View>
                        </View>}
                        />
                        </TouchableOpacity>
                    );
                }}
            </Mutation>
        )
    }

    render() {
        const { navigation } = this.props;
        return (
            <ViewCuston light={colors.white} dark={colors.black} containers={
            <View style={styles.container}>
            <Header navigation={navigation} />
                <View style={{ marginBottom: 25, marginTop: 5 }}>
                    <CustomText light={colors.blue_main} dark={colors.white} style={{ fontSize: 20, fontWeight: '500', marginBottom: 10 }}>
                        <AntdIcons name="bells" style={{ marginLeft: 'auto' }} size={20} color={colors.orange} />  Notificaciones
                </CustomText>
                    <CustomText style={{ color: colors.rgb_153, fontWeight: '300', fontSize: dimensions.FontSize(14) }}>
                        Mantente al día de todo lo que está sucediendo.
                </CustomText>
                </View>
                <Query query={GET_NOTIFICATIONS} variables={{ userId: this.state.userId }}>
                    {(response, error, loading, refetch) => {
                        this.refetch = refetch;
                        if (loading) {
                            return <ActivityIndicator />
                        }
                        if (error) {
                            return console.log('Response Error-------', error);
                        }
                        if (response) {
                            return (

                                <FlatList
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                    data={response && response.data && response.data.getNotifications ? response.data.getNotifications.notifications : ''}
                                    showsVerticalScrollIndicator={false}
                                    renderItem={({ item }) => this._renderItem(item, response.refetch)}
                                    keyExtractor={item => item._id}
                                    ListEmptyComponent={
                                        <View style={{ alignSelf: 'center', padding: 20 }}>
                                            <Image source={image.Vacia} style={{ width: dimensions.Width(34), alignSelf: 'center', height: dimensions.Height(13) }} />
                                            <CustomText light={colors.blue_main} dark={colors.white} style={{ textAlign: 'center', fontSize: dimensions.FontSize(20), fontWeight: '200' }}>Bien hecho estas al día</CustomText>
                                        </View>
                                    }
                                />
                            )
                        }
                    }}
                </Query>
            </View>}
            />
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        paddingHorizontal: dimensions.Width(4),
        height: dimensions.Height(100)
    },
    payment: {
        height: 'auto',
        alignSelf: 'center',
        width: dimensions.Width(90),
        backgroundColor: colors.white,
        marginTop: 20,
        borderRadius: 10,
        paddingVertical: dimensions.Width(4),
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.2,
        elevation: 3,
    },
    buttonView: {
        marginTop: dimensions.Height(4),
        alignSelf: 'center',
        backgroundColor: colors.ERROR,
        width: dimensions.Width(84),
        borderRadius: dimensions.Width(8),
    },
    buttonView1: {
        marginTop: dimensions.Height(3),
        alignSelf: 'center',
        backgroundColor: colors.blue_main,
        width: dimensions.Width(84),
        borderRadius: dimensions.Width(8),
    },
    buttonTitle: {
        alignSelf: 'center',
        paddingVertical: dimensions.Height(2),
        paddingHorizontal: dimensions.Width(2),
        color: colors.white,
        fontWeight: 'bold',
        fontSize: dimensions.FontSize(17),
    }

});


export default Notification;