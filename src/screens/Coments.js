import React from 'react';
import { View, StyleSheet, ActivityIndicator, FlatList, Image } from 'react-native'
import { colors, dimensions } from './../theme';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { CustomText } from './../components/CustomText';
import { image } from './../constants';
import { Avatar } from 'react-native-elements';
import { NETWORK_INTERFACE_LINK_AVATAR } from './../constants/config';
import 'moment/locale/es';
import moment from "moment";
import Header from './../components/Header';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';

const PROFESSIONAL_RATING_QUERY = gql`
    query {
        getProfessionalRating {
        success
        message
        list {
            id
            coment
            rate
            updated_at
            customer {
                id
                ciudad
                nombre
                apellidos
                foto_del_perfil
              }
             }
            }
          }
        `;


class Coments extends React.Component {

    _renderItem({ item }) {
        return (
            <ViewCuston light="#F3F3F3" dark={colors.back_dark} style={{ width: dimensions.Width(90), height: dimensions.Width(45), color: colors.rgb_153, padding: 10, borderRadius: 10, marginBottom: 20}}containers={
            <View style={{ marginBottom: 20, alignSelf: 'center' }}>
                <View>
                    <View style={{ width: '100%', padding: 5, flexDirection: 'row', }}>
                        <Avatar
                            containerStyle={{ borderWidth: 3, borderColor: colors.rgb_235 }}
                            rounded
                            source={{ uri: NETWORK_INTERFACE_LINK_AVATAR + item.customer.foto_del_perfil }}
                        />
                        <View>
                            <CustomText numberOfLines={1} style={{ fontSize: 20, marginLeft: 5, fontWeight: '300', color: colors.green_main, }}>
                                {item.customer.nombre}  {item.customer.apellidos}
                            </CustomText>
                            <View style={{flexDirection: 'row'}}>
                                <CustomText style={{ fontSize: 12, marginLeft: 5, fontWeight: '300', color: colors.rgb_153, }}>
                                    {moment(Number(item.updated_at)).fromNow()}
                                </CustomText>
                                <CustomText style={{ fontSize: 12, marginLeft: 10, fontWeight: '300', color: colors.rgb_153,}}>
                                    {item.customer.ciudad}
                                </CustomText>
                            </View>
                        </View>
                        <CustomText style={{ fontSize: 12, fontWeight: '400', color: colors.rgb_153, marginLeft: 'auto', alignItems: 'flex-end', marginTop: 5 }}>
                            <AntdIcons name="staro" size={18} color={colors.orange} /> ({item.rate}) Valoración
                        </CustomText>
                    </View>
                    <CustomText numberOfLines={5} style={{ fontSize: 14, fontWeight: '400', color: colors.rgb_153, textAlign: "justify", padding: 15 }}>
                        {item.coment}
                    </CustomText>
                </View>
            </View>}
            />
        )
    }
    render() {
        const { navigation } = this.props;
        return (
            <Query query={PROFESSIONAL_RATING_QUERY}>
                {(response, error, loading) => {
                    if (loading) {
                        return <ActivityIndicator size="large" color={colors.green_main} />
                    }
                    if (error) {
                        return console.log('Response Error-------', error);
                    }
                    if (response) {
                        console.log('shfbviybfviy====================', response)
                        return (
                            <ViewCuston light={colors.white} dark={colors.black} containers={
                            <View style={styles.container}>  
                            <Header navigation={navigation} />
                                <View style={{ marginBottom: 25, marginTop: 5, paddingHorizontal: dimensions.Width(2) }}>
                                    <CustomText light={colors.blue_main} dark={colors.white} style={{ fontSize: 20, fontWeight: '500',marginBottom: 10 }}>
                                        <AntdIcons name="staro" size={20} color={colors.orange} /> Opiniones de clientes
                                    </CustomText>
                                    <CustomText style={{ color: colors.rgb_153, fontWeight: '300', fontSize: dimensions.FontSize(14) }}>
                                        Mira lo que opinan los clientes de tu trabajo.
                                    </CustomText>
                                </View>
                                <FlatList
                                    showsVerticalScrollIndicator={false}
                                    keyExtractor={item => item.id}
                                    data={response && response.data ? response.data.getProfessionalRating.list : ''}
                                    renderItem={(item) => this._renderItem(item)}
                                    ListEmptyComponent={
                                        <View style={{ alignSelf: 'center', padding: 20 }}>
                                            <Image source={image.Vacia} style={{ width: dimensions.Width(34), alignSelf: 'center', height: dimensions.Height(13) }} />
                                            <CustomText style={{ textAlign: 'center', fontSize: dimensions.FontSize(20), color: colors.blue_main, fontWeight: '200' }}>Aún no tienes opiniones de cliente.</CustomText>
                                        </View>
                                    }
                                />
                            </View>}
                            />
                        )
                    }
                }}
            </Query>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        paddingHorizontal: dimensions.Width(4),
        height: dimensions.Height(100)
    },


});

export default Coments;