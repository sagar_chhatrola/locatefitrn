import React, { Component } from 'react';
import { View, Image, ImageBackground, StyleSheet, Alert, Linking, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { colors, dimensions } from './../theme';
import { CustomTextInput } from './../components/CustomTextInput';
import { CustomText } from './../components/CustomText';
import { Button } from './../components/Button';
import { NETWORK_INTERFACE_LINK } from '../constants/config';
import { user, image } from './../constants';
import axios from "axios";
import { ViewCuston } from '../components/CustonView';


class LandingScreen extends Component {
  constructor(props){
    super(props)
    this.state = {
      email: '',
      noEmail: false
    }
  }

  UpdateState = (name, value) => {
    this.setState({
      [name]: value
    });
  };

  handleSubmit = () => {
    const {email} = this.state;
    console.log(email)
    const {navigation} = this.props;
    const url =  NETWORK_INTERFACE_LINK + '/forgotpassword';
    if (email==''){
      Alert.alert(
        'Ohhh algo va mal ',
        'por favor escribe tu email para continuar',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
        )
        return null
    }  
    axios
      .post(url, {email})
      .then(res => {
       if (res.data.noEmail) {
          this.setState({
            noEmail: res.data.noEmail,
            message: res.data.message,
            email: ''
          });

          Alert.alert(
            'Error con tu Email',
            this.state.message,
            [
              {
                text: 'Regístrarme',
                onPress: () => navigation.navigate('Register'),
                style: 'cancel',
              },
              {
                text: 'Volver a intentarlo',
                onPress: () => console.log('Volver a intentar'),
              },
            ],
          );

        } else {
          this.setState({
            email:''
          })
            navigation.navigate('Login')
        }
      })
      .catch(err => {
        console.log('error:', err);
      });
};
  render() {
    let { email } = this.state
    return (
      <ViewCuston light={colors.white} dark={colors.black} containers={
      <View style={styles.container}>
        <KeyboardAwareScrollView  keyboardShouldPersistTaps="always">
          <ImageBackground source={image.img_map} resizeMode="contain" style={{justifyContent:'center',alignItems:'flex-start',height:dimensions.Height(30), marginLeft: 15, marginRight: 20}}> 
            <CustomText light={colors.black} dark={colors.white} style={{fontSize:dimensions.FontSize(20)}}>¿Olvidaste tu contraseña?</CustomText>
            <CustomText style={{marginTop:dimensions.Height(1),fontSize:dimensions.FontSize(12),color:colors.slight_black,alignSelf:'center'}}>
            Ingresa el email asociado a tu cuenta y te enviaremos un enlace para restablecer tu contraseña.
            </CustomText>
          </ImageBackground>
          <View style={styles.formView}>
          <CustomTextInput  placeholderTextColor={colors.rgb_153} autoCompleteType="email" autoCapitalize={"none"}  onPress={()=>{}} left={image.ic_email} value={email} onChangeText={(email)=>this.setState({email})}  name='email' keyboardType="email-address" placeholder="Correo electrónico"/>
          <View style={styles.signupButtonContainer}>
            <Button light={colors.white} dark={colors.white} containerStyle={styles.buttonView} 
                      onPress={this.handleSubmit} title="Enviadme el elnace" 
                      titleStyle={styles.buttonTitle}/>
          </View>
          <CustomText style={{marginTop:dimensions.Height(3),fontSize:dimensions.FontSize(14),color:colors.slight_black,alignSelf:'center'}}>
            ¿Aún no tienes una cuenta? <CustomText light={colors.blue_main} dark={colors.white} onPress={()=>this.props.navigation.navigate('Register')} style={{fontSize:dimensions.FontSize(14),fontWeight:'500'}}>Registrarme</CustomText>
          </CustomText>
          </View>
        </KeyboardAwareScrollView>
      </View>}
      />
    );
  }
}

export default connect(
  null,
  {  }
)(LandingScreen);

const styles = StyleSheet.create({
  container:{
    backgroundColor: 'transparent',
    height: dimensions.Height(100)
  },
  formView:{
    marginHorizontal:dimensions.Width(8),
    flex:1,
    marginTop:dimensions.Height(4)
  },
  rememberMeView:{
    marginTop:dimensions.Height(2),
    flexDirection:'row'
  },
  rememberText:{
    fontSize:dimensions.FontSize(18)
  },
  signupButtonContainer:{
    marginTop:dimensions.Height(5),
    alignSelf:'center'
  },
  buttonView:{
    backgroundColor:colors.light_blue,
    width:dimensions.Width(84),
    borderRadius:dimensions.Width(8),
  },
  buttonTitle:{
    alignSelf:'center',
    paddingVertical:dimensions.Height(2),
    paddingHorizontal:dimensions.Width(5),
    color:colors.white,
    fontWeight:'bold',
    fontSize:dimensions.FontSize(17),
  }
})