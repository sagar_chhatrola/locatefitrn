import React from 'react';
import { View, ScrollView, StyleSheet, RefreshControl } from 'react-native';
import { colors, dimensions } from '../theme';
import { CustomText } from '../components/CustomText';
import CardInspirationfavourites from '../components/CardInspirationfavourites';
import LoadingPlaceHolders from './placeholderloadinginspiration';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import Header from './../components/Header';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';


export default class Favourites extends React.Component {

  constructor() {
    super()
    this.state = {
      Loading: true,
      refreshing: false
    }
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({
        Loading: false
      })
    }, 2000)
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.setState({
        refreshing: false
      })
    }, 2000)
  }

  renderArticles = () => {
    const { navigation , refetch} = this.props;
    return (
      <ViewCuston light={colors.white} dark={colors.black} containers={
      <View>
      <View style={styles.container}>
      <Header navigation={navigation} />
      </View>
        <View style={{ marginBottom: 25, marginTop: 5, paddingHorizontal: dimensions.Width(4)}}>
          <CustomText light={colors.blue_main} dark={colors.white} style={{ fontSize: 20, fontWeight: '500', marginBottom: 10 }}>
            <AntdIcons name="picture" style={{ marginLeft: 'auto' }} size={20} color={colors.green_main} />  Mis inspiraciones
          </CustomText>
          <CustomText style={{ color: colors.rgb_153, fontWeight: '300', fontSize: dimensions.FontSize(14) }}>
            Guarda aquí tus inspiraciones para que puedas implementarla en tu hogar.
          </CustomText>

        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >
          {
            this.state.Loading ?
              <LoadingPlaceHolders />
              :
              <View style={{marginBottom: dimensions.Height(80)}}>
                <CardInspirationfavourites navigation={navigation} refetch={refetch}/>
              </View>
          }
        </ScrollView>
      </View>
        }
        />
    )
  }

  render() {
    return (
      <ViewCuston light={colors.white} dark={colors.black} containers={
      <View>
        {this.renderArticles()}
      </View>
      }
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    paddingHorizontal: dimensions.Width(4)
  },

});