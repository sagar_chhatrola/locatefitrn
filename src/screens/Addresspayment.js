import React from 'react';
import { View, StyleSheet, KeyboardAvoidingView, Alert, TextInput } from 'react-native'
import { colors, dimensions } from './../theme';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { Query, Mutation} from 'react-apollo';
import gql from 'graphql-tag';
import { CustomText } from './../components/CustomText';
import MaterialCommunityIcons from 'react-native-vector-icons/Feather';
import { Button } from './../components/Button';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { CustomTextInput } from './../components/CustomTextInput';

const USUARIO_DIRECCION_QUERY = gql`
    query{
        getUsuarioDireccion {
        success
        message
        data {
            id
            nombre
            calle
            ciudad
            provincia
            codigopostal
            telefono
            }
          }
        }
    `;

    const ELIMINAR_USUARIO_DIRECCION = gql`
    mutation eliminarUsuarioDireccion($id: ID!) {
      eliminarUsuarioDireccion(id: $id) {
        success
        message
      }
    }
  `;

const NUEVO_USUARIO_DIRECCION = gql`
mutation crearUsuarioDireccion($input: UsuarioDireccionInput) {
  crearUsuarioDireccion(input: $input) {
    success
    message
    data {
      id
      nombre
      calle
      ciudad
      provincia
      codigopostal
      telefono
    }
  }
}
`;

class Address extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            nombre: '',
            calle: '',
            ciudad: '',
            provincia: '',
            codigopostal: '',
            telefono: ''
        };
    }

    state = {
        direccion: {}
      };

      updateState = (name, value) => {
        this.setState({
            [name]: value
        });
    };


    handleSubmit(e, crearUsuarioDireccion, data) {
        this.setState();
        const input = this.state;
        const {
            nombre,
            calle,
            ciudad,
            provincia,
            codigopostal,
            telefono
          } = this.state;
          if (nombre == '', calle == '', ciudad == '', provincia == '', codigopostal == '', telefono=='') {
            Alert.alert(
              'Error al guardar',
              'Todos los campos son obligatorio para para guardar tu dirección',
              [
                { text: 'OK', onPress: () => console.log('OK Pressed') },
              ],
              { cancelable: false },
            )
            return null
          }
        console.log('sending input', input)
        crearUsuarioDireccion({ variables: {input} }).then(async ({ data: res }) => {
            if (
                res &&
                res.crearUsuarioDireccion &&
                res.crearUsuarioDireccion.success
              ) {
                console.log(res.crearUsuarioDireccion.message);
                this.updateState('nombre', '');
                this.updateState('calle', '')
                this.updateState('ciudad', '')
                this.updateState('provincia', '')
                this.updateState('codigopostal', '')
                this.updateState('telefono', '')
                if (this.refetch) {
                    this.refetch()
                    .then(async ({ data: res }) => {
                        if (res && res.data && res.data.crearUsuarioDireccion && res.data.crearUsuarioDireccion.success) {
                        }
                    });
                }
            }
        }).catch(err => {
            console.log(err);
        })
    }

      async handleEliminar(e, eliminarUsuarioDireccion, id) {
        await eliminarUsuarioDireccion({ variables: { id } }).then(async ({ data: res }) => {
            if (
              res &&
              res.eliminarUsuarioDireccion &&
              res.eliminarUsuarioDireccion.success
            ) {
              console.log(res.eliminarUsuarioDireccion.message);
              this.refetch()
            }
            else if (
              res &&
              res.eliminarUsuarioDireccion &&
              !res.eliminarUsuarioDireccion.success
            ){
                this.refetch()
                console.log(res.eliminarUsuarioDireccion.message);

            }
          });
       
      }

    render() {
        const {
            nombre,
            calle,
            ciudad,
            provincia,
            codigopostal,
            telefono
          }  = this.state;
          const { navigation } = this.props
        return (
            <Query query={USUARIO_DIRECCION_QUERY}>
 {                  ({ loading, error, data, refetch }) => {
                    console.log(loading, error, data, refetch)  
                    this.refetch = refetch;
                    if (loading) {
                        console.log('Response loading-------', loading);
                        return null
                    }
                    else{
                        let response = {data:data}             
                        console.log('Response response-------', response);
                        const  datadireccion = response && response.data ? response.data.getUsuarioDireccion.data : ''
                        const isDisableCard = !!response && response.data && response.data.getUsuarioDireccion.data ? response.data.getUsuarioDireccion.data : '' && !!response && response.data && response.data.getUsuarioDireccion.data ? response.data.getUsuarioDireccion.data.id : ''
                        return (
                            <View style={styles.container}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                <View style={{ marginBottom: 5, marginTop: 5}}>
                                    <CustomText light={colors.blue_main} dark={colors.white} style={{ fontSize: 20, fontWeight: '500', marginBottom: 10 }}>
                                        <MaterialCommunityIcons name="map-pin" size={20} color={colors.green_main} />  Mi dirección
                                        </CustomText>
                                    <CustomText style={{color: colors.rgb_153, fontWeight: '300', fontSize: dimensions.FontSize(14)}}>
                                    Gestiona tu dirección de para servicio.
                                    </CustomText>
                                </View>
                                {
                                    isDisableCard ?

                                    <View style={styles.payment}>
                                        <View>
                                        <AntdIcons name="home" size={20} color={colors.blue_main} />
                                        </View>
                                        <View style={{marginLeft: 20}}>
                                            <CustomText style={{marginLeft: 15, color: colors.blue_main, fontWeight: '500', fontSize: dimensions.FontSize(16) }}>
                                                {datadireccion.nombre}
                                            </CustomText>
                                            <CustomText style={{marginLeft: 15, color: colors.rgb_153, fontWeight: '300', fontSize: dimensions.FontSize(14) }}>
                                                {datadireccion.calle}
                                            </CustomText>
                                            <CustomText style={{marginLeft: 15, color: colors.rgb_153, fontWeight: '300', fontSize: dimensions.FontSize(14) }}>
                                                {datadireccion.ciudad},
                                            </CustomText>
                                            <CustomText style={{marginLeft: 15, color: colors.rgb_153, fontWeight: '300', fontSize: dimensions.FontSize(14) }}>
                                                {datadireccion.provincia}
                                            </CustomText>
                                            <CustomText style={{marginLeft: 15, color: colors.rgb_153, fontWeight: '300', fontSize: dimensions.FontSize(14) }}>
                                                {datadireccion.codigopostal}
                                            </CustomText>
                                            <CustomText style={{marginLeft: 15, color: colors.rgb_153, fontWeight: '300', fontSize: dimensions.FontSize(14) }}>
                                                {datadireccion.telefono}
                                            </CustomText>
                                        </View>
                                        <Mutation mutation={ELIMINAR_USUARIO_DIRECCION}>
                                        {(eliminarUsuarioDireccion) => {
                                            return (
                                            <View style={{marginLeft: 'auto', alignSelf: 'center', marginRight: 15}}>
                                                <TouchableOpacity onPress={e =>
                                                    this.handleEliminar(e, eliminarUsuarioDireccion, datadireccion.id , refetch)
                                                }>
                                                    <AntdIcons name="delete" size={25} color={colors.ERROR} />
                                                </TouchableOpacity>
                                            </View>
                                                );
                                            }}
                                        </Mutation>
                                    </View>
                                    : null
                                }

                                {
                                    !isDisableCard  ?
                                    <Mutation mutation={NUEVO_USUARIO_DIRECCION}>
                                    {crearUsuarioDireccion => {
                                     return (
                                        <KeyboardAwareScrollView style={{ marginTop: dimensions.Height(5), alignSelf: 'center', }} keyboardShouldPersistTaps="always">
                                                                <CustomTextInput
                                                                    containerStyle={{ marginTop: dimensions.Height(2) }}
                                                                    value={nombre}
                                                                    onChangeText={(nombre) => this.setState({ nombre })}
                                                                    name='nombre'
                                                                    keyboardType="default"
                                                                    placeholder="Nombre completo" />

                                                                <CustomTextInput
                                                                    containerStyle={{ marginTop: dimensions.Height(2) }}
                                                                    value={calle}
                                                                    onChangeText={(calle) => this.setState({ calle })}
                                                                    name='calle'
                                                                    keyboardType="default"
                                                                    placeholder="Calle, número y puerta" />

                                                                <CustomTextInput
                                                                    containerStyle={{ marginTop: dimensions.Height(2) }}
                                                                    value={ciudad}
                                                                    onChangeText={(ciudad) => this.setState({ ciudad })}
                                                                    name='ciudad'
                                                                    keyboardType="default"
                                                                    placeholder="Ciudad" />

                                                                <CustomTextInput
                                                                    containerStyle={{ marginTop: dimensions.Height(2) }}
                                                                    value={provincia}
                                                                    onChangeText={(provincia) => this.setState({ provincia })}
                                                                    name='provincia'
                                                                    keyboardType="default"
                                                                    placeholder="Provincia" />

                                                                <CustomTextInput
                                                                    containerStyle={{ marginTop: dimensions.Height(2) }}
                                                                    value={codigopostal}
                                                                    onChangeText={(codigopostal) => this.setState({ codigopostal })}
                                                                    name='codigopostal'
                                                                    keyboardType="default"
                                                                    placeholder="Código postal" />

                                                                <CustomTextInput
                                                                    containerStyle={{ marginTop: dimensions.Height(2) }}
                                                                    value={telefono}
                                                                    onChangeText={(telefono) => this.setState({ telefono })}
                                                                    name='telefono'
                                                                    keyboardType="numeric"
                                                                    placeholder="Número móvil" />
                                                                <View>
                                                                    <Button light={colors.white} dark={colors.white} containerStyle={styles.buttonView1}
                                                                        onPress={e => { this.handleSubmit(e, crearUsuarioDireccion) }}
                                                                        title="Guardar cambios"
                                                                        titleStyle={styles.buttonTitle} />
                                                                </View>
                                                            </KeyboardAwareScrollView>
                                    )
                                }}
                            </Mutation>
                                    : null
                                }
                                </ScrollView>
                            </View>
                            
                        )
                    }
                }}
            </Query>
        )
    }
}


  


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'transparent',
        paddingHorizontal: dimensions.Width(4),
        marginBottom: 30
    },
    payment: {
        height: 'auto',
        alignSelf: 'center',
        width: dimensions.Width(100),
        backgroundColor: colors.white,
        flexDirection: 'row',
        padding: 15,
        marginTop: 20,
        borderRadius: 10,
        paddingVertical: dimensions.Width(4),
        borderBottomWidth: 0.5,
        borderBottomColor: colors.rgb_235,
        borderTopWidth: 0.5,
        borderTopColor: colors.rgb_235,

    },
    buttonView: {
        marginTop: dimensions.Height(4),
        alignSelf: 'center',
        backgroundColor: colors.ERROR,
        width: dimensions.Width(84),
        borderRadius: dimensions.Width(8),
    },
    buttonView1: {
        marginTop: dimensions.Height(3),
        alignSelf: 'center',
        backgroundColor: colors.blue_main,
        width: dimensions.Width(84),
        borderRadius: dimensions.Width(8),
    },
    buttonTitle: {
        alignSelf: 'center',
        paddingVertical: dimensions.Height(2),
        paddingHorizontal: dimensions.Width(2),
        color: colors.white,
        fontWeight: 'bold',
        fontSize: dimensions.FontSize(17),
    }

});

export default Address;