import React from 'react';
import { Text, StyleSheet, View, Image, Modal, ScrollView, TouchableOpacity, Share, Alert, SafeAreaView } from 'react-native';
import { colors, dimensions } from '../theme';
import { NETWORK_INTERFACE_LINK_AVATAR } from './../constants/config';
import { image } from '../constants/image'
import Headerparallax from './../components/HeaderParallax';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/Feather';
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo';
import { Avatar } from 'react-native-elements';
import MapView, { Marker } from 'react-native-maps';
import 'moment/locale/es';
import moment, { now } from "moment";
import AsyncStorage from '@react-native-community/async-storage';
import Iconsverifiel from 'react-native-vector-icons/Octicons';
import Toast from 'react-native-simple-toast';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';
import { CustomText } from './../components/CustomText';

const CREAR_USUARIO_FAVORITE_PRODUCTO = gql`
  mutation crearUsuarioFavoritoProducto($productoId: ID!) {
    crearUsuarioFavoritoProducto(productoId: $productoId) {
      success
      message
    }
  }
`;

const ELIMINAR_PRODUCT = gql`
  mutation eliminarProducto($id: ID!) {
    eliminarProducto(id: $id) {
      success
      message
    }
  }
`;

class ProductDetails extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            data: null,
            value: '',
            modalVisible: false,
            city: '',
            UserID: ''
        }
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    async componentDidMount() {
        const { navigation } = this.props;
        const data = await navigation.getParam('data');
        const id = await AsyncStorage.getItem('id');
        this.setState({
            data,
            city: data.city,
            UserID: id,
            latitude: 0.000000,
            longitude: 0.000000
        })
    }

    componentDidUpdate() {
        let apiUrlWithParams = `https://maps.googleapis.com/maps/api/geocode/json?address=${this.state.city}&key=AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg`;
        fetch(apiUrlWithParams)
            .then(response => response.json())
            .then(data => {
                if (data.results.length > 0) {
                    this.setState({
                        latitude: data.results[0].geometry.location.lat,
                        longitude: data.results[0].geometry.location.lng
                    })
                }
            })

            .catch(error => {
                // this.setState({ locationFilterChecked: false });
                console.log('error in product-plan getting lat, lng: ', error);

            })
    }


    refetch = null;

    anadirFavorito = (crearUsuarioFavoritoProducto, productoId) => {
        crearUsuarioFavoritoProducto({ variables: { productoId } }).then(async ({ data: res }) => {
            if (res && res.crearUsuarioFavoritoProducto && res.crearUsuarioFavoritoProducto.success) {
                Toast.showWithGravity('Producto añadido con éxito a al lista de deseos', Toast.LONG, Toast.TOP)
                console.log('Producto añadido con éxito')
                this.setState(prevState => ({
                    data: { ...prevState.data, anadidoFavorito: true }
                }))
            }
            else if (res && res.crearUsuarioFavoritoProducto && !res.crearUsuarioFavoritoProducto.success)
            Alert.alert(
                'Upps debes iniciar sesión',
                (res.crearUsuarioFavoritoProducto.message),
                [
                    {
                      text: 'Iniciar sesión', onPress: () => this.props.navigation.navigate('Login'),
                    },
                    { 
                      text: 'Regístrarme', onPress: () => this.props.navigation.navigate('Register') 
                    },
                ],
                { cancelable: false },
            )
            return null

        });
    };

    handleDeleteAlert = async () =>{
        const { navigation } = this.props;
        const data = await navigation.getParam('data');
               
        }

        handleDeletePro = async (e, eliminarProducto, refetch) => {
            const { navigation } = this.props
            const data = await navigation.getParam('data')
            const id = data.id
            eliminarProducto({ variables:  {id} }).then(async ({ data }) => {
              if (data && data.eliminarProducto && data.eliminarProducto.success) {
                Toast.showWithGravity(data.eliminarProducto.message, Toast.LONG, Toast.TOP)
                console.log(data.eliminarProducto.message);
                navigation.navigate('Myadds')
              }
              else if (data && data.eliminarProducto && !data.eliminarProducto.success)
              console.log(data.eliminarProducto.message);
          
            });
          
          }


     onChat = async () =>{
        const id = await AsyncStorage.getItem('id');
        const { navigation } = this.props;
        const data = await navigation.getParam('data');
        if (id===null){
            Alert.alert(
                'Upps debes iniciar sesión',
                'Para iniciar una conversación debes iniciar sesión o regístrarte',
                [
                    {
                      text: 'Iniciar sesión', onPress: () => this.props.navigation.navigate('Login'),
                    },
                    { 
                      text: 'Regístrarme', onPress: () => this.props.navigation.navigate('Register') 
                    },
                ],
                { cancelable: false },
            )
            return null

        }if (id===data.creator.id){
            Alert.alert(
                'Upps álgo va mal',
                'no puedes entablar una conversación con tigo mismo',
                [
                    {
                      text: 'OK', onPress: () => console.log('ok'),
                    },
                ],
                { cancelable: false },
            )
            return null

        }else {
        navigation.navigate('Message', {data: data})  
        }     
    }

    onShare = async () => {
        try {
            const result = await Share.share({
                title: 'Locatefit',
                message:
                    'Locatefit | Los mejores profeisonales',

            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };

    _renderItem(item) {
        const { navigation } = this.props;
        let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
        item.professionalRating.forEach(start => {
            if (start.rate == 1) rating['1'] += 1;
            else if (start.rate == 2) rating['2'] += 1;
            else if (start.rate == 3) rating['3'] += 1;
            else if (start.rate == 4) rating['4'] += 1;
            else if (start.rate == 5) rating['5'] += 1;
        });

        const ar = (5 * rating['5'] + 4 * rating['4'] + 3 * rating['3'] + 2 * rating['2'] + 1 * rating['1']) / item.professionalRating.length;
        let averageRating = 0;
        if (item.professionalRating.length) {
            averageRating = ar.toFixed(1);
        }
        return (
            <Headerparallax name={
                <View style={{flexDirection: 'row'}}>
                <Text style={{color: 'white', fontSize: 24}}>{item.creator.nombre} {item.creator.apellidos}</Text>
                 {
                    item.creator.Verified ?
                    <Iconsverifiel name="verified" style={{ alignSelf: 'center', marginLeft: 5, marginTop: 5 }} size={12} color={colors.white} /> : null
                  }
                </View>
                }
                name1={<Text style={styles.stickySectionText}>{item.creator.nombre} {item.creator.apellidos}</Text>}
                descripcion={<Text style={{ fontSize: 14, fontWeight: '400', color: 'white' }}>
                    <AntdIcons family="Font-Awesome" name="staro" size={18} color={colors.orange} /> {averageRating} Valoración  ({item.professionalRating.length}) · {item.category.title} ·  <MaterialCommunityIcons name="map-pin" size={18} color={colors.green_main} /> {item.city}
                </Text>}
                back={<AntdIcons name="close" style={{ alignSelf: 'center', marginTop: 5, marginLeft: 2 }} size={24} color={colors.green_main} onPress={() => navigation.goBack(null)} />}
                back2={<TouchableOpacity onPress={this.onShare}><MaterialCommunityIcons style={{ alignSelf: 'center', marginTop: 5, marginLeft: 2 }} size={24} name="share" color={colors.green_main} /></TouchableOpacity>}
                avatar={{ uri: NETWORK_INTERFACE_LINK_AVATAR + item.creator.foto_del_perfil }}
                imagen={
                    item.fileList.length > 0 ?
                        { uri: item.fileList.length > 0 ? NETWORK_INTERFACE_LINK_AVATAR + item.fileList[0] : "" } : null}
                component={
                    <View style={{ marginTop: dimensions.Height(2), paddingHorizontal: dimensions.Width(3) }}>
                        <View style={{ flexDirection: 'row' }}>
                            <CustomText light={colors.black} dark={colors.white} numberOfLines={2} style={{ width: dimensions.Width(75), fontSize: dimensions.FontSize(20) }}>{item.title}</CustomText>
                            <Text style={{ marginLeft: 'auto', width: dimensions.Width(20), textAlign: 'right', color: colors.green_main, fontSize: dimensions.FontSize(20) }}>{item.number}€<Text style={{ color: colors.rgb_153, fontSize: dimensions.FontSize(16) }}>{item.currency}</Text></Text>
                        </View>
                        <View style={{ marginTop: 20, flexDirection: 'row' }}>
                            <CustomText light={colors.black} dark={colors.white} style={{ paddingRight: 10 }}>
                                <AntdIcons name="star" size={18} color={colors.orange} />
                                <AntdIcons name="star" size={18} color={colors.orange} />
                                <AntdIcons name="star" size={18} color={colors.orange} />
                                <AntdIcons name="star" size={18} color={colors.orange} />
                                <AntdIcons name="star" size={18} color={colors.orange} />
                                ({item.professionalRating.length})
                            </CustomText>
                            <View style={{ marginLeft: 'auto' }}>
                                {item.anadidoFavorito ?
                                    <View>
                                        <AntdIcons name="heart" size={25} color={colors.ERROR} />
                                    </View>
                                    :
                                    <View>
                                        <Mutation mutation={CREAR_USUARIO_FAVORITE_PRODUCTO}>
                                            {(crearUsuarioFavoritoProducto) => {
                                                return (
                                                    <TouchableOpacity onPress={() => this.anadirFavorito(crearUsuarioFavoritoProducto, item.id)}>
                                                        <AntdIcons name="hearto" size={25} color={colors.rgb_153} />
                                                    </TouchableOpacity>
                                                )
                                            }}
                                        </Mutation>
                                    </View>
                                }
                            </View>

                        </View>
                        <View style={{ width: dimensions.Width(93), marginTop: dimensions.Height(2) }}>
                            <Text style={{ fontSize: 20, fontWeight: '300', color: colors.rgb_153, marginBottom: 10 }}>
                                Descripción
                             </Text>
                            <Text numberOfLines={5} style={{ fontSize: 16, fontWeight: '200', color: colors.rgb_153, marginTop: 5 }}>
                                {item.description}
                            </Text>
                        </View>
                        <View style={{ width: '100%', justifyContent: 'flex-start' }}>

                            <View style={{ justifyContent: 'flex-start', flexDirection: 'row', marginTop: 20, marginLeft: 15 }}>
                                <View style={styles.qubo}>
                                    <Text style={{ fontSize: 12, fontWeight: '400', marginBottom: 5 }}>
                                        <AntdIcons size={18} name="clockcircleo" color="#3e5c0a" />
                                    </Text>
                                    <Text style={{ fontSize: 12, fontWeight: '400', color: '#3e5c0a', textAlign: 'center' }}>
                                        Tiempo limitado!
                                    </Text>
                                </View>

                                <View style={styles.qubo}>
                                    <Text style={{ fontSize: 12, fontWeight: '400', marginBottom: 5 }}>
                                        <AntdIcons size={18} name="eyeo" color="#3e5c0a" />
                                    </Text>
                                    <Text style={{ fontSize: 12, fontWeight: '400', color: '#3e5c0a', textAlign: 'center' }}>
                                        +{item.visitas} visitas
                                     </Text>
                                </View>

                                <View style={styles.qubo}>
                                    <Text style={{ fontSize: 12, fontWeight: '400', marginBottom: 5 }}>
                                        <AntdIcons size={18} name="staro" color="#3e5c0a" />
                                    </Text>
                                    <Text style={{ fontSize: 12, fontWeight: '400', color: '#3e5c0a', textAlign: 'center' }}>
                                        ({item.professionalRating.length}) Opiniones
                                    </Text>
                                </View>

                            </View>
                        </View>
                            <View>
                            <View style={{ marginBottom: 10, marginTop: 30 }}>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Paymentpage', { data: item })}
                                style={{
                                    width: dimensions.Width(95),
                                    marginRight: 15,
                                    height: 45,
                                    backgroundColor: colors.green_main,
                                    borderRadius: 5,
                                    justifyContent: "center",
                                    alignItems: "center",
                                }}
                                >
                                <Text style={{ fontSize: 16, alignItems: "center", color: colors.white, justifyContent: "center", }}>CONTRATAR PROFESIONAL</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginBottom: 30 }}>
                            <TouchableOpacity
                                onPress={() => this.onChat()}
                                style={{
                                    width: dimensions.Width(95),
                                    marginRight: 15,
                                    height: 45,
                                    backgroundColor: 'transparent',
                                    borderRadius: 5,
                                    borderWidth: 1,
                                    borderColor: colors.green_main,
                                    justifyContent: "center",
                                    alignItems: "center",
                                }}
                            >
                                <Text style={{ fontSize: 16, alignItems: "center", color: colors.green_main, justifyContent: "center", }}>CHAT CON EL PROFESIONAL</Text>
                            </TouchableOpacity>
                        </View>
                        {
                            item.creator.id === this.state.UserID ?
                            <Mutation mutation={ELIMINAR_PRODUCT}>
                              {(eliminarProducto, { loading, error, data }) => {
                                return (
                                    <View style={{ marginBottom: 30, marginTop: -20 }}>
                                        <TouchableOpacity
                                            onPress={() => Alert.alert(
                                                'Seguro que deseas elimiarlo',
                                                'Estas seguro que deseas eliminar este sevicio',
                                                [
                                                    {
                                                      text: 'Eliminar', onPress: e => this.handleDeletePro(e, eliminarProducto, item.id),
                                                    },
                                
                                                    {
                                                      text: 'Cancelar', onPress: () => console.log('Cancelado'),
                                                    },
                                                ],
                                                { cancelable: false },
                                            )}
                                            style={{
                                                width: dimensions.Width(95),
                                                marginRight: 15,
                                                height: 45,
                                                backgroundColor: 'transparent',
                                                borderRadius: 5,
                                                borderWidth: 1,
                                                borderColor: colors.ERROR,
                                                justifyContent: "center",
                                                alignItems: "center",
                                            }}
                                        >
                                            <Text style={{ fontSize: 16, alignItems: "center", color: colors.ERROR, justifyContent: "center", }}>ELIMINAR SERVICIO</Text>
                                        </TouchableOpacity>
                                    </View>)
                                }}
                              </Mutation> : null 
                                }
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, fontWeight: '300', color: colors.rgb_153, marginBottom: 10 }}>
                                Perfil del profesional
                            </Text>
                            <View style={{ width: '100%', height: 85, flexDirection: 'row', marginTop: 20, color: colors.rgb_153, }}>
                                <Avatar
                                    size="medium"
                                    rounded
                                    source={{ uri: NETWORK_INTERFACE_LINK_AVATAR + item.creator.foto_del_perfil }}
                                />
                                <View>
                                <View style={{flexDirection: 'row'}}>
                                    <Text style={{ fontSize: 20, marginLeft: 15, fontWeight: '300', color: colors.green_main, }}>
                                        {item.creator.nombre} {item.creator.apellidos}
                                    </Text>
                                    {
                                        item.creator.Verified ? 
                                        <Iconsverifiel name="verified" style={{alignSelf: 'center', marginLeft: 5, marginTop: 5}} size={12} color={colors.twitter_color}/> : null
                                    }
                                </View>
                                    <Text style={{ fontSize: 12, marginLeft: 15, fontWeight: '300', color: colors.rgb_153, }}>
                                        {item.creator.profesion}
                                    </Text>
                                </View>
                                <Text onPress={() => this.props.navigation.navigate('ProfilePublic', { data: item })} style={{ fontSize: 14, color: colors.green_main, marginTop: 18, textAlign: 'right', alignItems: 'flex-end', marginLeft: 'auto' }}>
                                    Más detalles <AntdIcons name="right" size={16} color={colors.green_main} />
                                </Text>
                            </View>
                        </View>
                        <View>
                            <View style={{ width: '80%', height: 'auto', backgroundColor: "#transparent", color: colors.rgb_153, flexDirection: 'row' }}>
                                <Text style={{ fontSize: 14, color: colors.rgb_153, fontWeight: '200' }}>
                                    Para proteger tus pagos, nunca trasfiera dinero ni te comuniques fuera de la página o la aplicación de Locatefit.
                                    </Text>
                                <Image style={{ width: 80, height: 80, marginLeft: 10, marginTop: -20 }}
                                    source={{ uri: 'https://bitbucket.org/leudy015/app-locatefit/raw/af523e73e8bf95f2902045fdd6c31d037887cd07/src/common/escudo.png' }}
                                />
                            </View>
                        </View>

                        <View style={{ marginTop: 20 }}>
                            <Text style={{ fontSize: 20, fontWeight: '300', color: colors.rgb_153, marginBottom: 20 }}>
                                Ubicación
                            </Text>
                            <View style={{ height: 300, width: "100%" }}>
                                <MapView
                                    style={{ height: 300, width: "100%" }}
                                    region={{
                                        latitude: this.state.latitude,
                                        longitude: this.state.longitude,
                                        latitudeDelta: 0.015,
                                        longitudeDelta: 0.0121,
                                    }}
                                    >
                                    <Marker coordinate={{ latitude: this.state.latitude ? this.state.latitude : 0.000000 , longitude: this.state.longitude ? this.state.longitude : 0.000000 }} />
                                </MapView>
                            </View>
                        </View>
                        <View style={{ marginTop: 20, marginBottom: 30 }}>
                            <Text style={{ fontSize: 20, fontWeight: '300', color: colors.rgb_153, marginBottom: 15 }}>
                                Opiniones de clientes
                            </Text>
                            {item.professionalRating.length > 0 ?
                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                    {
                                        item.professionalRating.map((coment, i) => {
                                            return (
                                                <ViewCuston key={i} light="#F3F3F3" dark={colors.back_dark} style={{ width: dimensions.Width(90), height: dimensions.Width(45), color: colors.rgb_153, borderRadius: 10, }} containers={
                                                <View>
                                                    <View style={{ width: 350, height: dimensions.Width(45), color: colors.rgb_153, padding: 10, borderRadius: 10, }}>
                                                        <View style={{ width: '100%', padding: 5, flexDirection: 'row', }}>
                                                            <Avatar
                                                                containerStyle={{ borderWidth: 3, borderColor: colors.rgb_235 }}
                                                                rounded
                                                                source={{ uri: NETWORK_INTERFACE_LINK_AVATAR + coment.customer.foto_del_perfil }}
                                                            />
                                                            <View>
                                                                <Text numberOfLines={1} style={{ fontSize: 20, marginLeft: 5, fontWeight: '300', color: colors.green_main, }}>
                                                                    {coment.customer.nombre} {coment.customer.apellidos}
                                                                </Text>
                                                                <View style={{ flexDirection: 'row' }}>
                                                                    <Text style={{ fontSize: 12, marginLeft: 5, fontWeight: '300', color: colors.rgb_153, }}>
                                                                        {moment(Number(coment.updated_at)).fromNow()}
                                                                    </Text>
                                                                    <Text style={{ fontSize: 12, marginLeft: 10, fontWeight: '300', color: colors.rgb_153, }}>
                                                                        {coment.customer.ciudad}
                                                                    </Text>
                                                                </View>
                                                            </View>
                                                            <Text style={{ fontSize: 12, fontWeight: '400', color: colors.rgb_153, marginLeft: 'auto', alignItems: 'flex-end', marginTop: 5 }}>
                                                                <AntdIcons name="staro" size={18} color={colors.orange} /> ({coment.rate}) Valoración
                                                            </Text>
                                                        </View>
                                                        <Text numberOfLines={5} style={{ fontSize: 14, fontWeight: '400', color: colors.rgb_153, textAlign: "justify", padding: 15 }}>
                                                            {coment.coment}
                                                        </Text>
                                                    </View>
                                                </View>}
                                                />

                                            )
                                        })
                                    }

                                </ScrollView>
                                : null
                            }
                            {(item.professionalRating.length === 0 || !item.professionalRating) ?
                                <View style={{ alignSelf: 'center', padding: 20 }}>
                                    <Image source={image.Vacia} style={{ width: dimensions.Width(34), alignSelf: 'center', height: dimensions.Height(13) }} />
                                    <CustomText light={colors.blue_main} dark={colors.white}  style={{ textAlign: 'center', fontSize: dimensions.FontSize(20),  fontWeight: '200' }}>Aún no tenemos valoraciones de este profesional</CustomText>
                                </View> : null
                            }
                        </View>
                    </View>
                }
            />
        )
    }
    render() {
        const { data } = this.state;
        if (data) {

            return this._renderItem(data);
        }
        return null;
    }
}

const styles = StyleSheet.create({
    qubo: {
        flexDirection: 'column',
        backgroundColor: '#CDD89B',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        padding: 10,
        width: 90,
        height: 90,
        borderRadius: 15,
        marginRight: 20,
        color: '#4B4B3A'
    },

    navigatebajo: {
        width: '100%',
        justifyContent: 'center',
        flexDirection: 'row',
        height: 80,
        marginTop: 30,
        marginRight: 15,
    },
    stickySectionText: {
        color: '#95ca3e',
        fontSize: 20,
        margin: 10,
        marginTop: 15
      },

});

export default ProductDetails;