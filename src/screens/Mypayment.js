import React from 'react';
import { View, StyleSheet, TextInput, TouchableOpacity, Alert } from 'react-native'
import { colors, dimensions } from './../theme';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { CustomText } from './../components/CustomText';
import { CustomTextInput } from './../components/CustomTextInput';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Button } from './../components/Button';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {
    TextField,
    FilledTextField,
    OutlinedTextField,
} from 'react-native-material-textfield';
import Header from './../components/Header';
import { user, image } from './../constants';
import { ActivityIndicator } from '@ant-design/react-native';
import Toast from 'react-native-simple-toast';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';

const USUARIO_PAGO_QUERY = gql`
    query{
      getUsuarioPago {
        success
        message
        data {
          id
          nombre
          iban
          }
        }
      }
    `;

const ELIMINAR_PAGO = gql`
  mutation eliminarPago($id: ID!) {
    eliminarPago(id: $id) {
      success
      message
    }
  }
`;

const NUEVO_PAGO = gql`
  mutation crearPago($input: PagoInput) {
    crearPago(input: $input) {
      success
      message
      data {
        id
        nombre
        iban
      }
    }
  }
`;

class Address extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            iban: '',
            nombre: '',
        };
    }

    refetch = null

    updateState = (name, value) => {
        this.setState({
            [name]: value
        });
    };


    handleSubmit(e, crearPago, refetch) {
        const input = this.state;
        const {
            iban,
            nombre,
        } = this.state;
        if (nombre == '', iban == '') {
            Alert.alert(
                'Error al guardar',
                'Todos los campos son obligatorio para para guardar tu método de pago',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false },
            )
            return null
        }
        crearPago({ variables: { input } }).then(async ({ data: res }) => {
            if (res && res.crearPago && res.crearPago.success) {
                Toast.showWithGravity(res.crearPago.message, Toast.LONG, Toast.TOP)
                console.log(res.crearPago.message);
                this.updateState('nombre', '');
                this.updateState('iban', '');
                if (this.refetch) {
                    this.refetch().then(async ({ data: res }) => {
                        if (res && res.data && res.data.crearPago && res.data.crearPago.success) {
                        }
                    });
                }
            }
        }).catch(err => {
            console.log(err);
        })
    }

    async handleEliminar(e, eliminarPago, id) {
        await eliminarPago({ variables: { id } }).then(async ({ data: res }) => {
            if (res && res.eliminarPago && res.eliminarPago.success) {
                Toast.showWithGravity(res.eliminarPago.message, Toast.LONG, Toast.TOP)
                console.log(res.eliminarPago.message);
                this.refetch()
            }
            else if (res && res.eliminarPago && !res.eliminarPago.success)
                console.log(res.eliminarPago.message);
        });
    }

    render() {
        const { iban, nombre, } = this.state;
        const { navigation } = this.props;
        return (
            <ViewCuston light={colors.white} dark={colors.black} containers={
                <View style={styles.container}>
                    <Header navigation={navigation} />
                    <View style={{ marginBottom: 5, marginTop: 5 }}>
                        <CustomText light={colors.blue_main} dark={colors.white} style={{ fontSize: 20, fontWeight: '500', marginBottom: 10 }}>
                            <Icons name="currency-eur" style={{ marginLeft: 'auto' }} size={20} color={colors.green_main} />  Mis datos de cobro
                    </CustomText>
                        <CustomText light={colors.blue_main} dark={colors.white} style={{ fontWeight: '300', fontSize: dimensions.FontSize(14) }}>
                            Aquí podrás editar tu información para retirar ganancias de la plataforma
                    </CustomText>
                    </View>
                    <Query query={USUARIO_PAGO_QUERY}>
                        {({ loading, error, data, refetch }) => {
                            this.refetch = refetch;
                            if (loading) {
                                return <ActivityIndicator />
                            }

                            else {
                                let response = { data: data }
                                const datapago = response && response.data && response.data.getUsuarioPago.data ? response.data.getUsuarioPago.data : ''
                                const isDisableCard = !!response && response.data && response.data.getUsuarioPago.data ? response.data.getUsuarioPago.data : '' && !!response && response.data && response.data.getUsuarioPago.data ? response.data.getUsuarioPago.data.id : ''
                                return (
                                    <View>
                                        {
                                            isDisableCard ?
                                                <View style={styles.payment}>
                                                    <View>
                                                        <IconsD name="wallet" size={20}  light={colors.blue_main} dark={colors.white}/>
                                                    </View>
                                                    <View style={{ marginLeft: 10 }}>
                                                        <CustomText light={colors.blue_main} dark={colors.white} style={{ marginLeft: 15, fontWeight: '500', fontSize: dimensions.FontSize(16) }}>
                                                            {datapago.nombre}
                                                        </CustomText>
                                                        <CustomText light={colors.rgb_153} dark={colors.white} style={{ marginLeft: 15, fontWeight: '300', fontSize: dimensions.FontSize(14) }}>
                                                            {datapago.iban}
                                                        </CustomText>
                                                    </View>
                                                    <Mutation mutation={ELIMINAR_PAGO}>
                                                        {(eliminarPago) => {
                                                            return (
                                                                <View style={{ marginLeft: 'auto', alignSelf: 'center', marginRight: 15 }}>
                                                                    <TouchableOpacity onPress={e => this.handleEliminar(e, eliminarPago, datapago.id)}>
                                                                        <AntdIcons name="delete" size={25} color={colors.ERROR} />
                                                                    </TouchableOpacity>
                                                                </View>
                                                            )
                                                        }}
                                                    </Mutation>
                                                </View>

                                                : null
                                        }

                                        {
                                            !isDisableCard ?
                                                <Mutation mutation={NUEVO_PAGO}>
                                                    {(crearPago, { refetch }) => {
                                                        return (
                                                            <KeyboardAwareScrollView style={{ marginTop: dimensions.Height(5), alignSelf: 'center', }} keyboardShouldPersistTaps="always">

                                                                <CustomTextInput
                                                                    containerStyle={{ marginTop: dimensions.Height(2) }}
                                                                    value={nombre}
                                                                    onChangeText={(nombre) => this.setState({ nombre })}
                                                                    name='nombre'
                                                                    keyboardType="default"
                                                                    placeholder="Nombre titular" />
                                                                <CustomTextInput
                                                                    containerStyle={{ marginTop: dimensions.Height(2) }}
                                                                    value={iban}
                                                                    onChangeText={(iban) => this.setState({ iban })}
                                                                    name='iban'
                                                                    keyboardType="default"
                                                                    placeholder="Iban bancarío" />
                                                                <View>
                                                                    <Button light={colors.white} dark={colors.white} containerStyle={styles.buttonView1}
                                                                        onPress={e => this.handleSubmit(e, crearPago, refetch)}
                                                                        title="Guardar cambios"
                                                                        titleStyle={styles.buttonTitle} />
                                                                </View>
                                                            </KeyboardAwareScrollView>
                                                        )
                                                    }}
                                                </Mutation>

                                                : null
                                        }
                                    </View>
                                )
                            }
                        }}
                    </Query>
                </View>}
            />
        )
    }
}

const styles = StyleSheet.create({
    container: {

        backgroundColor: 'transparent',
        paddingHorizontal: dimensions.Width(4),
        marginBottom: 30,
        height: dimensions.Height(100)
    },
    payment: {
        height: 'auto',
        alignSelf: 'center',
        width: dimensions.Width(100),
        backgroundColor: 'transparent',
        flexDirection: 'row',
        padding: 15,
        marginTop: 20,
        borderRadius: 10,
        paddingVertical: dimensions.Width(4),
        borderBottomWidth: 0.5,
        borderBottomColor: colors.rgb_235,
        borderTopWidth: 0.5,
        borderTopColor: colors.rgb_235,
    },

    buttonView: {
        marginTop: dimensions.Height(4),
        alignSelf: 'center',
        backgroundColor: colors.ERROR,
        width: dimensions.Width(84),
        borderRadius: dimensions.Width(8),
    },
    buttonView1: {
        marginTop: dimensions.Height(3),
        alignSelf: 'center',
        backgroundColor: colors.blue_main,
        width: dimensions.Width(84),
        borderRadius: dimensions.Width(8),
    },
    buttonTitle: {
        alignSelf: 'center',
        paddingVertical: dimensions.Height(2),
        paddingHorizontal: dimensions.Width(2),
        color: colors.white,
        fontWeight: 'bold',
        fontSize: dimensions.FontSize(17),
    },
    formView: {
        marginHorizontal: dimensions.Width(8),
        flex: 1,
        marginTop: dimensions.Height(4)
    },

});

export default Address;