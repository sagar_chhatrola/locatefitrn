import React from 'react';
import { TextInput, StyleSheet, View, Image, Modal, Alert, TouchableHighlight, SafeAreaView} from 'react-native';
import { colors, dimensions } from '../theme';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { image } from '../constants/image'
import MaterialCommunityIcons from 'react-native-vector-icons/Feather';
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { NETWORK_INTERFACE_LINK_AVATAR, NETWORK_INTERFACE_LINK} from '../constants/config';
import AsyncStorage from '@react-native-community/async-storage';
import { CustomText } from '../components/CustomText';
import { ScrollView } from 'react-native-gesture-handler';
import OpenURLButton from './../components/LinkOut';
import moment from 'moment';
import { Button } from './../components/Button';
import { Rating, Avatar } from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { withApollo } from 'react-apollo';
import Header from './../components/Header';
import Toast from 'react-native-simple-toast';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';


export const PROFESSIONAL_ORDEN_PROCEED = gql`
  mutation ordenProceed($ordenId: ID!, $estado: String!, $progreso: String!, $status: String!, $nota: String, $coment: String, $rate: Int, $descripcionproblem: String) {
    ordenProceed(ordenId: $ordenId, estado: $estado, progreso: $progreso, status: $status, nota: $nota, coment: $coment, rate: $rate, descripcionproblem: $descripcionproblem) {
      success
      message
    }
  }
`;

const CREATE_NOTIFICATION = gql`
  mutation createNotification($input: NotificationInput) {
    createNotification(input: $input) {
      success
      message
    }
  }
`;

class OrdenDetails extends React.Component {
    

    constructor(props) {
        super(props);
        this.state = {
            descripcionproblem: '',
            coment: '',
            rate: '',
            modalVisible: false,
            modalVisible1: false,
            userId: '',
            data: null,
            messageTexte: 'Enhora buena el cliente a valorado tu trabajo.',
            messageTexteresolucion: 'El cliente a abierto una reclamación del servicio pronto tendrás noticias de la resolución',
            useridpush: ''
        }; 
    }

    async componentDidMount() {
        const {navigation} = this.props;
        const data = await navigation.getParam('data');
        const userId = await AsyncStorage.getItem('id');
        this.setState({ 
            userId, data,
            useridpush: data.prfsnl.UserID
        })
      }

    ratingCompleted = (rating) => {
        this.setState({
            rate: rating
        });
        console.log('Rating value'  + rating)
      }


    updateState = (name, value) => {
        this.setState({
            [name]: value
        });
    };    

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    setModalVisible1(visible) {
        this.setState({ modalVisible1: visible });
    }

    ///////////send notification push///////////////

    SendPushNotificationvalorar = () => {
        fetch(`${NETWORK_INTERFACE_LINK}/send-push-notification?IdOnesignal=${this.state.useridpush}&textmessage=${this.state.messageTexte}`)
          .catch(err => console.log(err))

          console.log(this.state.useridpush, this.state.messageTexte)
      };

      SendPushNotificationresolucion = () => {
        fetch(`${NETWORK_INTERFACE_LINK}/send-push-notification?IdOnesignal=${this.state.useridpush}&textmessage=${this.state.messageTexteresolucion}`)
          .catch(err => console.log(err))
          console.log(this.state.useridpush, this.state.messageTexteresolucion)
      };




    reportproblem(e, ordenProceed, data) {
        this.setState();
            const formData = {
                ordenId: data.id,
                estado: 'Resolución',
                progreso: '0',
                status: 'exception',
                descripcionproblem: this.state.descripcionproblem,
              }

              const {
                descripcionproblem,
            } = this.state;
            if (descripcionproblem=='') {
              Alert.alert(
                'Error al reportar problema',
                'Debes explicarno cual ha sido tu problema con la orden',
                [
                  { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false },
              )
              return null
            }
        ordenProceed({ variables: formData }).then(async res => {
            if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
                Toast.showWithGravity('Pedido en resolución', Toast.LONG, Toast.TOP)
                this.setState(prevState => ({
                    data: { ...prevState.data, estado: 'Resolución' }
                }))
                const notification = {
                    orden: data.id, 
                    user: data.prfsnl.id,
                    cliente: this.state.userId,
                    profesional: data.prfsnl.id,
                    type:'resolution_order'
                }
                this.props.client.mutate({
                    mutation: CREATE_NOTIFICATION,
                    variables: {input: notification}
                  })
                  .then(async (results)=> {
                    console.log("results",results)
                  }).catch(err=>{
                    console.log("err",err)
                  })
                  this.SendPushNotificationresolucion()
                console.log('Has repostado tu problema éxitosamente');
                setTimeout( () =>{
                    setTimeout(() => {this.setModalVisible1(!this.state.modalVisible1)}, 100)
                },)
                if (this.refetch) {
                    this.refetch().then(res => {
                        if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
                            this.setState({ ordenes: res.data.getOrdenesByProfessional.list });
                        }
                    });
                }
            }
        }).catch(err => {
            console.log('Algo salió mal. Por favor intente nuevamente en un momento.')
        })
    }

   
    valorarprofesional(e, ordenProceed, data) {
        this.setState();
              const formData = {
                ordenId: data.id,
                estado: 'Valorada',
                progreso: '100',
                status: 'success',
                coment: this.state.coment,
                rate: this.state.rate,
              }
              const {
                  coment,
                  rate,
              } = formData;
              if (rate=='', coment=='') {
                Alert.alert(
                  'Error al valorara el profesional',
                  'Debes comentar la experiencia para poder valorara este profesional',
                  [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                  ],
                  { cancelable: false },
                )
                return null
              }
              console.log( 'esto es lo qie se esya enviadop', formData)
        ordenProceed({ variables: formData }).then(async res => {
            if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
                Toast.showWithGravity('Pedido valorado éxitosamente', Toast.LONG, Toast.TOP)
                this.setState(prevState => ({
                    data: { ...prevState.data, estado: 'Valorada' }
                }))
                const notification = {
                    orden: data.id, 
                    user: data.prfsnl.id,
                    cliente: this.state.userId,
                    profesional: data.prfsnl.id,
                    type:'valored_order'
                }
                this.props.client.mutate({
                    mutation: CREATE_NOTIFICATION,
                    variables: {input: notification}
                  })
                  .then(async (results)=> {
                    console.log("results",results)
                  }).catch(err=>{
                    console.log("err",err)
                  })
                  this.SendPushNotificationvalorar()
                console.log('Has valorado el profesional éxitosamente');
                setTimeout( () =>{
                    setTimeout(() => {this.setModalVisible(!this.state.modalVisible)}, 100)
                },)
                if (this.refetch) {
                    this.refetch().then(res => {
                        if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
                            this.setState({ ordenes: res.data.getOrdenesByProfessional.list });
                        }
                    });
                }
            }
        }).catch(err => {
            console.log('Algo salió mal. Por favor intente nuevamente en un momento.')
        })
    }


    _renderItem(item) {
        const { navigation } = this.props;
        const description = item.nota;
        const titulo = item.product.title;
        const date = new Date(item.endDate + ' ' + item.time);
        const year = date.getFullYear();
        // const year = date.getUTCFullYear();
        let month = date.getMonth() + 1;
        // let month = date.getUTCMonth() + 1;
        month = (month < 10) ? '0' + month : month;
        const day = date.getDate();
        // const day = date.getUTCDate();
        let hours = date.getHours();
        // let hours = date.getUTCHours();
        hours = (hours < 10) ? '0' + hours : hours;
        const minutes = date.getMinutes();
        // const minutes = date.getUTCMinutes();

        const startDateString = year + '' + month + '0' + day + 'T' + hours + '' + minutes + '000';

        const productoTime = item.product.time.split(':');
        let endHours = Number(hours) + (Number(productoTime[0]) * Number(item.cantidad));
        let endMinutes = Number(minutes) + (Number(productoTime[1]) * Number(item.cantidad));
        if (endMinutes > 60) {
            endHours++;
            endMinutes -= 60;
        }

        endHours = (endHours < 10) ? '0' + endHours : endHours;
        endMinutes = (endMinutes < 10) ? '0' + endMinutes : endMinutes;
        const endDateString = year + '' + month + '0' + day + 'T' + endHours + '' + endMinutes + '00';

        const link = "https://calendar.google.com/calendar/r/eventedit?text=" + titulo + "&dates=" + startDateString + "/" + endDateString + "&details=" + description + "&sprop=name:Locatefit&sf=true";

        let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
        item.product.professionalRating.forEach(start => {
            if (start.rate == 1) rating['1'] += 1;
            else if (start.rate == 2) rating['2'] += 1;
            else if (start.rate == 3) rating['3'] += 1;
            else if (start.rate == 4) rating['4'] += 1;
            else if (start.rate == 5) rating['5'] += 1;
        });

        const ar = (5 * rating['5'] + 4 * rating['4'] + 3 * rating['3'] + 2 * rating['2'] + 1 * rating['1']) / item.product.professionalRating.length;
        let averageRating = 0;
        if (item.product.professionalRating.length) {
            averageRating = ar.toFixed(1);
        }

        const { descripcionproblem, coment, rate } = this.props; 

        return (
            <ViewCuston light={colors.white} dark={colors.black} containers={
            <View style={styles.container}>
                <View style={{paddingHorizontal:dimensions.Height(2),}}>
                    <Header navigation={navigation} />
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View> 
                        {item.product.fileList.length > 0 ?
                            <Image source={{ uri: item.product.fileList.length > 0 ? NETWORK_INTERFACE_LINK_AVATAR + item.product.fileList[0] : "" }} style={{ width: dimensions.Width(100), height: dimensions.Height(30) }} /> : null}
                    </View>
                    
                    <View style={{ width: dimensions.Width(40), height: dimensions.Height(5), backgroundColor: colors.rgb_235, borderRadius: 30, position: 'absolute', shadowColor: 'black', shadowOffset: { width: 0, height: 2 }, shadowRadius: 6, shadowOpacity: 0.2, elevation: 3, marginTop: dimensions.Height(27), marginLeft: 20 }}>
                        <CustomText style={{ color: colors.blue_main, alignSelf: 'center', marginTop: dimensions.Height(1.3), fontWeight: '200', fontSize: dimensions.FontSize(16) }}>
                            {item.estado}
                        </CustomText>
                    </View>
                    <View style={{ width: dimensions.Width(95), borderTopWidth: 0.5, borderBottomWidth: 0.5, marginTop: 40, borderTopColor: colors.light_grey, borderBottomColor: colors.light_grey, height: dimensions.Height(8), alignSelf: 'center', }}>
                        <CustomText light={colors.blue_main} dark={colors.white} style={{ fontSize: dimensions.FontSize(18), fontWeight: '300', marginTop: 5 }}>
                            {item.product.title}
                        </CustomText>
                        <CustomText style={{ color: colors.rgb_153, fontSize: dimensions.FontSize(14), fontWeight: '300', marginTop: 5 }}>
                            Cantidad: {item.cantidad} {item.product.currency}
                        </CustomText>
                    </View>
                    <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{marginTop: dimensions.Height(3)}} containers={
                    <View style={{ width: dimensions.Width(100), height: 'auto' }}>
                        <CustomText style={{ padding: 10, color: colors.grey, fontSize: dimensions.FontSize(18) }}>
                            Fecha del realización del servicio:
                        </CustomText>
                    </View>
                    }
                    />
                    <View style={{ marginTop: 20, paddingHorizontal: dimensions.Width(4) }}>
                        <CustomText light={colors.black} dark={colors.white} style={{ fontWeight: '200', fontSize: dimensions.FontSize(16) }}>
                            {item.endDate} a las {item.time}
                        </CustomText>
                        <View style={{ alignSelf: 'center', marginTop: dimensions.Height(2) }}>
                            <View style={styles.buttonView}>
                                <OpenURLButton url={link} containerStyle={styles.buttonView} text='AÑADIR A GOOGLE CALENDAR' />
                            </View>
                        </View>
                    </View>
                    <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{marginTop: dimensions.Height(3)}} containers={
                    <View style={{ width: dimensions.Width(100), height: 'auto',}}>
                        <CustomText style={{ padding: 10, color: colors.grey, fontSize: dimensions.FontSize(18) }}>
                            Informacón del profesinal:
                        </CustomText>
                    </View>
                    }
                    />
                    <View style={{ marginLeft: 15, marginTop: 20 }}>
                        <Avatar
                            containerStyle={{}}
                            rounded
                            source={{ uri: NETWORK_INTERFACE_LINK_AVATAR + item.prfsnl.foto_del_perfil }}
                        />

                        <CustomText light={colors.black} dark={colors.white} style={{ fontSize: dimensions.FontSize(18), fontWeight: '200', marginTop: 10 }}>
                            {item.prfsnl.nombre} {item.prfsnl.apellidos}
                        </CustomText>
                        <CustomText light={colors.black} dark={colors.white} style={{ fontSize: dimensions.FontSize(18), fontWeight: '200', marginTop: 10 }}>
                            <MaterialCommunityIcons name="map-pin" size={16} color={colors.green_main} /> {item.prfsnl.ciudad}
                        </CustomText>
                        <CustomText light={colors.black} dark={colors.white} style={{ fontSize: dimensions.FontSize(18), fontWeight: '200', marginTop: 10 }}>
                            <MaterialCommunityIcons name="smartphone" size={16} color={colors.green_main} /> {item.prfsnl.telefono}
                        </CustomText>

                    </View>
                    <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{marginTop: dimensions.Height(3)}} containers={
                    <View style={{ width: dimensions.Width(100), height: 'auto',}}>
                        <CustomText style={{ padding: 10, color: colors.grey, fontSize: dimensions.FontSize(18) }}>
                            Detalles del Pedido:
                        </CustomText>
                    </View>}
                    />
                    <View style={{ marginLeft: 10 }}>
                        <View style={{ flexDirection: 'row', marginTop: 20 }}>
                            <CustomText light={colors.black} dark={colors.white} style={{ fontSize: dimensions.FontSize(18), fontWeight: '200' }}>
                                Nº Pedido:
                            </CustomText>
                            <CustomText light={colors.black} dark={colors.white} style={{ marginLeft: 'auto', marginRight: 15, fontWeight: '200' }}>
                                {item.id}
                            </CustomText>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 20 }}>
                            <CustomText light={colors.black} dark={colors.white} style={{ fontSize: dimensions.FontSize(18), fontWeight: '200' }}>
                                Fecha:
                    </CustomText>
                            <CustomText light={colors.black} dark={colors.white} style={{ marginLeft: 'auto', marginRight: 15, fontWeight: '200' }}>
                                   {moment(Number(item.created_at)).format('LL')}
                            </CustomText>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 20 }}>
                            <CustomText light={colors.black} dark={colors.white} style={{ fontSize: dimensions.FontSize(18), fontWeight: '200' }}>
                                Total:
                    </CustomText>
                            <CustomText light={colors.black} dark={colors.white} style={{ marginLeft: 'auto', marginRight: 15, fontWeight: '200' }}>
                                {item.cantidad * item.product.number}€
                    </CustomText>
                        </View>
                    </View>
                    <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{marginTop: dimensions.Height(3)}} containers={
                    <View style={{ width: dimensions.Width(100), height: 'auto' }}>
                        <CustomText style={{ padding: 10, color: colors.grey, fontSize: dimensions.FontSize(18) }}>
                            Acción:
                        </CustomText>
                    </View>
                    }
                    />
                    {
                        item.stado ?
                            <View style={{ paddingHorizontal: dimensions.Width(4), alignSelf: 'center', marginBottom: 40 }}>

                            </View> : null
                    }

                    {
                        (item.estado === 'Nueva' || !item.estado) ?
                            <View style={{ paddingHorizontal: dimensions.Width(4), alignSelf: 'center', marginBottom: 40 }}>
                                <View style={{ marginBottom: 15, marginTop: 15 }}>
                                        <View style={styles.buttonView1}>
                                            <OpenURLButton url={`mailto:info@locatefit.es?subject=Solicitud de devolución orden No. ${item.id}`} containerStyle={styles.buttonView} text='SOLICITAR DEVOLUCIÓN' />
                                        </View>
                                    </View>
                                </View> : null
                            }
                        {
                                    (item.estado === 'Finalizada' || !item.estado) ?
                                        <View style={{ paddingHorizontal: dimensions.Width(4), alignSelf: 'center', marginBottom: 40 }}>
                                            <View>
                                            <View style={{ marginTop: 22}}>
                                                    <Modal
                                                        style={{height: dimensions.Height(80)}}
                                                        animationType={'slide'}
                                                        transparent={false}
                                                        visible={this.state.modalVisible}
                                                        onRequestClose={() => {
                                                          Alert.alert('Seguro que deseas salir');
                                                        }}>
                                                        <ViewCuston light={colors.white} dark={colors.black} containers={
                                                        <View style={{height: dimensions.Height(100) }}>
                                                            <SafeAreaView style={styles.headers}>
                                                                    <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                                                    <View style={{ alignItems: 'flex-start', marginLeft: 20 }}>
                                                                    </View>
                                                                    <View style={{ marginLeft: 'auto', flexDirection: 'row' }}>
                                                                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible)}}>
                                                                        <CustomText light={colors.black} dark={colors.white} style={{ marginRight: 20 }}>
                                                                        <AntdIcons name="closecircle" size={25} color= {colors.ERROR} />
                                                                        </CustomText>
                                                                    </TouchableHighlight>
                                                                    </View>
                                                                    </View>
                                                                </SafeAreaView>
                                                                <ScrollView showsVerticalScrollIndicator={false}>
                                                                <View style={{flex: 1, paddingHorizontal: dimensions.Width(4), marginTop: dimensions.Height(4)}}>
                                                                    <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(18), fontWeight: '200'}}>Valora el servios brindado por el profeisonal para ayudar a otros clientes como tu.</CustomText>
                                                                    <View style={{ marginLeft: 15, marginTop: 20, borderBottomWidth: 0.5, borderBottomColor: colors.rgb_153, paddingBottom: 15}}>
                                                                        <Avatar
                                                                            containerStyle={{}}
                                                                            rounded
                                                                            source={{ uri: NETWORK_INTERFACE_LINK_AVATAR + item.prfsnl.foto_del_perfil }}
                                                                        />

                                                                        <CustomText light={colors.black} dark={colors.white} style={{ fontSize: dimensions.FontSize(18), fontWeight: '200', marginTop: 10 }}>
                                                                            {item.prfsnl.nombre} {item.prfsnl.apellidos}  
                                                                        </CustomText>
                                                                    </View>

                                                                    <View style={{marginTop: dimensions.Height(5), alignSelf: 'center'}}>
                                                                        <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(18), fontWeight: '200', marginBottom: 20}}>Dinos cual fue tu experiencia con el servicio.</CustomText>
                                                                        <Rating imageSize={30} startingValue={0} onFinishRating={this.ratingCompleted} value={rate} />
                                                                    </View>
                                                            <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
                                                                <View style={{marginTop: 30}}>
                                                                    <TextInput
                                                                        name='coment'
                                                                        value={coment}
                                                                        onChangeText={(text) => this.updateState('coment', text)} 
                                                                        placeholderTextColor={colors.rgb_153} 
                                                                        color={colors.blue_main}
                                                                        multiline
                                                                        numberOfLines={5}  
                                                                        placeholder="Comentarío"
                                                                        style={{ height: 150, paddingLeft: 20, paddingRight: 20, borderRadius: 5, marginBottom: 30,width: dimensions.Width(90), backgroundColor: 'transparent', fontSize: dimensions.FontSize(16), borderColor: colors.rgb_235, borderWidth: 0.5}}
                                                                     />
                                                                </View>
                                                                <View style={{alignSelf: 'center'}}>
                                                                <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
                                                                    {(ordenProceed) => (
                                                                <View style={styles.buttonView2}>
                                                                    <Button light={colors.white} dark={colors.white} onPress={e => this.valorarprofesional(e, ordenProceed, item)} titleStyle={styles.buttonTitle} title="VALORAR" />
                                                                </View>
                                                                )}
                                                                </Mutation>
                                                                </View>
                                                            </KeyboardAwareScrollView>
                                                                </View>
                                                            </ScrollView>
                                                        </View>}
                                                        />
                                                    </Modal>
                                                </View>
                                                <View style={styles.buttonView2}>
                                                    <Button light={colors.white} dark={colors.white} onPress={() => {this.setModalVisible(true)}}
                                                       titleStyle={styles.buttonTitle} title="VALORAR PROFESIONAL" />
                                                </View>
                                            </View>


                                            <View>
                                            <View style={{ marginTop: 22}}>
                                                    <Modal
                                                        style={{height: dimensions.Height(80)}}
                                                        animationType={'slide'}
                                                        transparent={false}
                                                        visible={this.state.modalVisible1}
                                                        onRequestClose={() => {
                                                            alert('Modal has been closed.');
                                                        }}>
                                                        <ViewCuston light={colors.white} dark={colors.black} containers={
                                                            <View style={{height: dimensions.Height(100) }}>
                                                            <SafeAreaView style={styles.headers}>
                                                                    <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                                                    <View style={{ alignItems: 'flex-start', marginLeft: 20 }}>
                                                                    </View>
                                                                    <View style={{ marginLeft: 'auto', flexDirection: 'row' }}>
                                                                    <TouchableHighlight onPress={() => {this.setModalVisible1(!this.state.modalVisible1)}}>
                                                                        <CustomText style={{ marginRight: 20 }}>
                                                                        <AntdIcons name="closecircle" size={25} color={colors.ERROR} />
                                                                        </CustomText>
                                                                    </TouchableHighlight>
                                                                    </View>
                                                                    </View>
                                                                </SafeAreaView>
                                                                <ScrollView showsVerticalScrollIndicator={false}>
                                                                <View style={{flex: 1, paddingHorizontal: dimensions.Width(4), marginTop: dimensions.Height(4)}}>
                                                                    <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(18), fontWeight: '200'}}>Lamentamos que hayas tenido un problema con tu pedido describenos que paso y lo resolveremos lo antes posible.</CustomText>
                                                                    <View style={{ marginLeft: 15, marginTop: 20, borderBottomWidth: 0.5, borderBottomColor: colors.rgb_153, paddingBottom: 15}}>
                                                                        <Avatar
                                                                            containerStyle={{}}
                                                                            rounded
                                                                            source={{ uri: NETWORK_INTERFACE_LINK_AVATAR + item.prfsnl.foto_del_perfil }}
                                                                        />

                                                                        <CustomText light={colors.black} dark={colors.white} style={{ fontSize: dimensions.FontSize(18), fontWeight: '200', marginTop: 10 }}>
                                                                            {item.prfsnl.nombre} {item.prfsnl.apellidos}  
                                                                        </CustomText>
                                                                    </View>

                                                                    <View style={{marginTop: dimensions.Height(5), alignSelf: 'center'}}>
                                                                        <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(18), fontWeight: '200', marginBottom: 20}}>Cuentanos cual ha sido el problema con el servicio y te ayudaremos a resolverlo.</CustomText>
                                                                    </View>
                                                                <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
                                                                <View style={{marginTop: 30}}>
                                                                    <TextInput 
                                                                        placeholderTextColor={colors.rgb_153} 
                                                                        color={colors.blue_main}
                                                                        multiline
                                                                        numberOfLines={5}  
                                                                        placeholder="Descripción"
                                                                        style={{ height: 150, paddingLeft: 20, paddingRight: 20, borderRadius: 5, marginBottom: 30,width: dimensions.Width(90), backgroundColor: 'transparent', fontSize: dimensions.FontSize(16), borderColor: colors.rgb_235, borderWidth: 0.5}}
                                                                        name='descripcionproblem'
                                                                        value={descripcionproblem}
                                                                        onChangeText={(text) => this.updateState('descripcionproblem', text)} 
                                                                     />
                                                                </View>
                                                                <View style={{alignSelf: 'center'}}>
                                                                <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
                                                                    {(ordenProceed) => (
                                                                    <View style={styles.buttonView1}>
                                                                        <Button light={colors.white} dark={colors.white} onPress={e => this.reportproblem(e, ordenProceed, item)} titleStyle={styles.buttonTitle} title="ENVIAR" />
                                                                    </View>
                                                                    )} 
                                                                    </Mutation>
                                                                </View>
                                                            </KeyboardAwareScrollView>
                                                                </View>
                                                            </ScrollView>
                                                        </View>}
                                                        />
                                                    </Modal>
                                                </View>
                                                <View style={styles.buttonView1}>
                                                    <Button light={colors.white} dark={colors.white} onPress={() => {this.setModalVisible1(true)}} titleStyle={styles.buttonTitle} title="REPORTAR UN PROBLEMA" />
                                                </View>
                                            </View>
                                        </View> : null
                                }
                        
                    
                </ScrollView>
            </View>
                }
                />
                )
            }
    render() {
        const { data } = this.state;
        if (data) {

            return this._renderItem(data);
        }
        return null;
    }
}
        
const styles = StyleSheet.create({
                container: {
                backgroundColor: "transparent",
                marginBottom: dimensions.Height(28)
                    },
    buttonView:{
                    backgroundColor:colors.light_sky,
                width:dimensions.Width(84),
                borderRadius:dimensions.Width(8),
              },
      buttonView1:{
                    backgroundColor:colors.ERROR,
                width:dimensions.Width(84),
                borderRadius:dimensions.Width(8),
              },
      buttonView2:{
                    backgroundColor:colors.orange,
                width:dimensions.Width(84),
                borderRadius:dimensions.Width(8),
              },
      buttonTitle:{
                    alignSelf:'center',
                paddingVertical:dimensions.Height(2),
                paddingHorizontal:dimensions.Width(5),
                color:colors.white,
                fontWeight:'bold',
                fontSize:dimensions.FontSize(17),
              }
        
        });
        
export default withApollo(OrdenDetails);