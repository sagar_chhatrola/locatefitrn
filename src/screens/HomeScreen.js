import React from 'react';
import {
  ScrollView,
  RefreshControl,
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  TouchableOpacity,
  TextInput
} from 'react-native';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { colors, dimensions } from './../theme';
import { CustomText } from './../components/CustomText';
import CardServices from '../components/CardServices';
import NavigationServices from '../services/navigationServices'
import navigationServices from '../services/navigationServices';
import navigation from 'react-native-popup-navigation/src/commonjs/components/navigation';


function wait(timeout) {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
}

export default function App() {
  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    wait(2000).then(() => setRefreshing(false));
  }, [refreshing]);

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.headers}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ alignItems: 'flex-start', marginLeft: 20 }}>
              <TouchableOpacity onPress={() => NavigationServices.openDrawer()}>
                <CustomText>
                  <AntdIcons name="menu-fold" size={25} color={colors.blue_main} />
                </CustomText>
              </TouchableOpacity>
            </View>
            <View style={{ marginLeft: 'auto', flexDirection: 'row' }}>
              <TouchableOpacity>
                <CustomText style={{ marginRight: 15 }}>
                  <AntdIcons name="bells" size={25} color={colors.blue_main} />
                </CustomText>
                <View style={styles.notify} />
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> NavigationServices.navigate('Favourites')}>
                <CustomText style={{ marginRight: 20 }}>
                  <AntdIcons name="hearto" size={25} color={colors.blue_main} />
                </CustomText>
                <View style={styles.notify} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ marginTop: dimensions.Height(4) }}>
            <TextInput
              color={colors.rgb_153}
              inlineImageLeft='search_icon'
              keyboardAppearance='dark'
              returnKeyType='search'
              placeholder='¿Que estas buscando?'
              clearButtonMode='always'
              backgroundColor={colors.white}
              placeholderTextColor={colors.rgb_153}
              style={{ height: 40, marginLeft: 10, marginRight: 10, borderRadius: 50, paddingLeft: 20, paddingRight: 20, color: colors.blue_main, shadowColor: colors.rgb_153,
                shadowOffset: { width: 0, height: 1 },
                shadowRadius: 10,
                shadowOpacity: 0.4,
                elevation: 5,
               }}
            />
          </View>
        </SafeAreaView>
        <ScrollView
        contentContainerStyle={styles.scrollView}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        >
        <CardServices  />
      </ScrollView>

      </View>
   
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
  },
  headers: {
    height: dimensions.Height(18),
    backgroundColor: colors.white,
  },
  formView: {
    marginHorizontal: dimensions.Width(8),
    flex: 1,
    marginTop: dimensions.Height(4)
  },
  rememberMeView: {
    marginTop: dimensions.Height(2),
    flexDirection: 'row'
  },
  rememberText: {
    fontSize: dimensions.FontSize(18)
  },
  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center'
  },
  buttonView: {
    backgroundColor: colors.white,
    width: dimensions.Width(54),
    borderRadius: dimensions.Width(1),
    shadowColor: colors.white,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 10,
    shadowOpacity: 0.5,
    elevation: 2,
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(2),
    color: colors.black,
    fontWeight: '200',
    fontSize: dimensions.FontSize(17),
  },
  notify: {
    backgroundColor: colors.ERROR,
    borderRadius: 4,
    height: dimensions.Height(1),
    width: dimensions.Height(1),
    position: 'absolute',
    right: 15,
  },

  categoryTitle: {
    height: "100%",
    width: dimensions.Width(95),
    paddingHorizontal: 10,
    backgroundColor: "rgba(0, 0, 0, 0.2)",
    justifyContent: "center",
    alignItems: "center"
  },

  imageBlock: {
    overflow: "hidden",
    borderRadius: 4
  },
})
