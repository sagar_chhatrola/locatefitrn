import React, { Component } from 'react';
import { View, Image, ImageBackground, StyleSheet, Alert, Linking, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import { colors, dimensions } from './../theme';
import { CustomTextInput } from './../components/CustomTextInput';
import { CustomText } from './../components/CustomText';
import { Button } from './../components/Button';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { user, image } from './../constants';
import { ViewCuston } from '../components/CustonView';
import {Logo} from '../constants/logo';


const NUEVO_USUARIO = gql`
  mutation crearUsuario($input: UsuarioInput) {
    crearUsuario(input: $input) {
      success
      message
      data {
        usuario
        password
        email
        nombre
        apellidos
        telefono
        tipo
      }
    }
  }
`

class RegisterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      usuario: '',
      password: '',
      email: '',
      nombre: '',
      apellidos: '',
      telefono:'',
      secureTextEntry: true
    };
  }

  UpdateState = (name, value) => {
    this.setState({
      [name]: value
    });
    console.log(value)
  };

  async handleSubmit(e, crearUsuario) {
    const input = this.state
    const {
      usuario,
      password,
      email,
      nombre,
      apellidos,
      telefono
    } = this.state;
    if (password == '', usuario == '', email == '', nombre == '', apellidos == '', telefono=='') {
      Alert.alert(
        'Error con el registro',
        'Todos los campos son obligatorio para el registro de usuario',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
      )
      return null
    }
    delete input.secureTextEntry
    await crearUsuario({ variables: { input } }) .then(async ({ data: res }) => {
      if (res && res.crearUsuario && res.crearUsuario.success) {
        console.log(res.crearUsuario.message);
        this.props.navigation.navigate('ConfirmEmail');
        this.UpdateState('usuario', "", 'password', "", 'email', "", 'nombre', "", 'apellidos', "",'telefono',"")
      } else if (res && res.crearUsuario && !res.crearUsuario.success)
        console.log(res.crearUsuario.message);
      Alert.alert(
        'Registro de usuario',
        (res.crearUsuario.message),
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
      ),
      this.UpdateState('usuario', "", 'password', "", 'email', "", 'nombre', "", 'apellidos', "",'telefono',"")
    });
  }

  render() {
    let { usuario, password, email, nombre, apellidos, secureTextEntry, telefono } = this.state
    return (
      <View style={styles.container}>
        <Mutation mutation={NUEVO_USUARIO}>
          {(crearUsuario) => {
            return (
              <ViewCuston light={colors.white} dark={colors.black} containers={
              <KeyboardAwareScrollView keyboardShouldPersistTaps="always" showsVerticalScrollIndicator={false} >
                <ImageBackground source={image.img_map} resizeMode="contain" style={{ justifyContent: 'center', alignItems: 'center', height: dimensions.Height(20) }}>
                <Logo />
                </ImageBackground>
                <View style={styles.formView}>
                  <CustomTextInput  placeholderTextColor={colors.rgb_153} containerStyle={{ marginTop: dimensions.Height(3.5)}}  left={image.ic_email} value={nombre} onChangeText={(nombre) => this.setState({ nombre })} name='nombre' keyboardType="default" placeholder="Nombres" />
                  <CustomTextInput  placeholderTextColor={colors.rgb_153} containerStyle={{ marginTop: dimensions.Height(3.5)}}  left={image.ic_email} value={apellidos} onChangeText={(apellidos) => this.setState({ apellidos })} name='apellidos' keyboardType="default" placeholder="Apellidos" />
                  <CustomTextInput  placeholderTextColor={colors.rgb_153} containerStyle={{ marginTop: dimensions.Height(3.5)}}  left={image.ic_email} value={usuario} onChangeText={(usuario) => this.setState({ usuario })} name='usuario' keyboardType="default" placeholder="Nombre de usuario" />
                  <CustomTextInput  placeholderTextColor={colors.rgb_153} containerStyle={{ marginTop: dimensions.Height(3.5)}}  left={image.ic_email} value={email} onChangeText={(email) => this.setState({ email })} name='email' keyboardType="email-address" placeholder="Correo electrónico" />
                  <CustomTextInput  placeholderTextColor={colors.rgb_153} onPress={() => this.setState({ secureTextEntry: !secureTextEntry })} left={image.ic_eye} containerStyle={{ marginTop: dimensions.Height(3.5), color: colors.black }} value={password} onChangeText={(password) => this.setState({ password })} name='password' secureTextEntry={secureTextEntry} placeholder="Contraseña" />
                  <CustomTextInput  keyboardType="numeric" placeholderTextColor={colors.rgb_153} left={image.ic_eye} containerStyle={{ marginTop: dimensions.Height(3.5)}} value={telefono} onChangeText={(telefono) => this.setState({ telefono })} name='phone'  placeholder="Número móvil" />
                  <CustomText style={{marginTop:dimensions.Height(3),fontSize:dimensions.FontSize(14),color:colors.slight_black,alignSelf:'center', textAlign: 'center',}}>
                      Continuar implica que has leído y aceptado los términos de condiciones y uso. 
                  </CustomText>
                  <View style={styles.signupButtonContainer}>
                    <Button light={colors.white} dark={colors.white} containerStyle={styles.buttonView}
                      onPress={e => this.handleSubmit(e, crearUsuario)} title="Regístrarme"
                      titleStyle={styles.buttonTitle} />
                  </View>
                  <CustomText style={{marginTop:dimensions.Height(3),fontSize:dimensions.FontSize(14),color:colors.slight_black,alignSelf:'center', marginBottom: dimensions.Height(10)}}>
                    ¿Ya tienes una cuenta? <CustomText light={colors.blue_main} dark={colors.white}  onPress={()=>this.props.navigation.navigate('Login')} style={{fontSize:dimensions.FontSize(14),fontWeight:'500',}}>Iniciar sesión</CustomText>
                  </CustomText>
                </View>
              </KeyboardAwareScrollView>}
              />
            );
          }}
        </Mutation>
      </View>
    );
  }
}

export default connect(
  null,
  {}
)(RegisterScreen);

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    height: dimensions.Height(100)
  },
  formView: {
    marginHorizontal: dimensions.Width(8),
    flex: 1,
    marginTop: dimensions.Height(1)
  },
  rememberMeView: {
    marginTop: dimensions.Height(2),
    flexDirection: 'row'
  },
  rememberText: {
    fontSize: dimensions.FontSize(18)
  },
  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center'
  },
  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(17),
  }
})