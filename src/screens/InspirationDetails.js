import React from 'react';
import { Text, StyleSheet, View, Image, Modal, ScrollView, TouchableOpacity, Share, Alert, SafeAreaView } from 'react-native';
import { colors, dimensions } from '../theme';
import Headerparallax from './../components/HeaderParallaxInspiration';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/Feather';
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo';
import 'moment/locale/es';
import moment from "moment";
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import { CustomText } from '../components/CustomText';



const CREAR_USUARIO_FAVORITE_INSPIRACION = gql`
  mutation crearUsuarioFavoritoInspiration($InspirationID: ID!) {
    crearUsuarioFavoritoInspiration(InspirationID: $InspirationID) {
      success
      message
    }
  }
`;

class InspirationDeatils extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            data: null,
            UserID: ''
        }
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    async componentDidMount() {
        const { navigation } = this.props;
        const data = await navigation.getParam('data');
        const id = await AsyncStorage.getItem('id');
        this.setState({
            data,
            UserID: id
        })
    }

    onShare = async () => {
        const { navigation } = this.props;
        const data = await navigation.getParam('data');
        try {
            const result = await Share.share({
                title: 'Locatefit',
                message:
                    (data.title),
            }); 

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };

    anadirFavorito = (crearUsuarioFavoritoInspiration, InspirationID) => {
        crearUsuarioFavoritoInspiration({ variables: { InspirationID } }).then(async ({ data: res }) => {
            if (res && res.crearUsuarioFavoritoInspiration && res.crearUsuarioFavoritoInspiration.success) {
                Toast.showWithGravity('Inspiración añadida a la lista de deseos', Toast.LONG, Toast.TOP)
                console.log('Producto añadido con exito')
                this.setState(prevState => ({
                    data: { ...prevState.data, anadidoFavoritoinspiration: true }
                }))
            }
            else if (res && res.crearUsuarioFavoritoInspiration && !res.crearUsuarioFavoritoInspiration.success)
            Alert.alert(
                'Upps debes iniciar sesión',
                (res.crearUsuarioFavoritoInspiration.message),
                [
                    {
                      text: 'Iniciar sesión', onPress: () => this.props.navigation.navigate('Login'),
                    },
                    { 
                      text: 'Regístrarme', onPress: () => this.props.navigation.navigate('Register') 
                    },
                ],
                { cancelable: false },
            )
            return null

        })
        .catch(error => {
            console.log(`error : ${error}`);
          })
    };



    _renderItem(item) {
        const { navigation } = this.props;
        return (
            <Headerparallax
                image1={item.image1}
                name1={<CustomText light={colors.black} dark={colors.white} numberOfLines={1} style={styles.stickySectionText}>{item.title}</CustomText>}
                back={<AntdIcons name="close" style={{ alignSelf: 'center', marginTop: 5, marginLeft: 2 }} size={24} color={colors.green_main} onPress={() => navigation.goBack(null)} />}
                component={<View>
                    <View style={{flexDirection: 'row'}}>
                    <CustomText light={colors.black} dark={colors.white} numberOfLines={3} style={{ width: dimensions.Width(70), fontSize: dimensions.FontSize(20), marginLeft: 10, fontWeight: 'bold', marginTop: 10}}>
                        {item.title}
                      </CustomText>
                    <View style={{ marginLeft: 'auto', marginTop: 10 }}>
                                {item.anadidoFavoritoinspiration ?
                                    <View>
                                        <AntdIcons name="heart" size={25} color={colors.ERROR} />
                                    </View>
                                    :
                                    <View>
                                        <Mutation mutation={CREAR_USUARIO_FAVORITE_INSPIRACION}>
                                            {(crearUsuarioFavoritoInspiration) => {
                                                return (
                                                    <TouchableOpacity onPress={() => this.anadirFavorito(crearUsuarioFavoritoInspiration, item.id)}>
                                                        <AntdIcons name="hearto" size={25} color={colors.rgb_153} />
                                                    </TouchableOpacity>
                                                )
                                            }}
                                        </Mutation>
                                    </View>
                                }
                            </View>
                        <TouchableOpacity style={{marginLeft: 10}} onPress={()=> this.onShare()}>
                            <MaterialCommunityIcons style={{marginTop: 10, marginLeft: 'auto', marginRight: 10 }} size={24} name="share" color={colors.rgb_153} />
                      </TouchableOpacity> 
                    </View>
                    <Text numberOfLines={1} style={{ width: dimensions.Width(70), fontSize: dimensions.FontSize(14), marginLeft: 10, fontWeight: '200', color: colors.rgb_153, marginTop: 30}}>
                    {moment(Number(item.created_at)).format('LL')}
                  </Text>
                    <View style={{paddingHorizontal: dimensions.Width(4), marginTop: dimensions.Height(3)}}>
                        <Text style={{textAlign: 'justify', fontSize: dimensions.FontSize(16), fontWeight: '300', color: colors.rgb_153}}>{item.description}</Text>
                    </View>

                    <View style={{marginTop: dimensions.Height(3), marginBottom: dimensions.Height(10)}}>
                    <Image source={{ uri: item.image2 }} style={{ width: dimensions.Width(100), height: 250, justifyContent: 'center', alignSelf: 'center' }} />
                    </View>
                </View>
                }
            />
        )
    }

    render() {
        const { data } = this.state;
        if (data) {
            return this._renderItem(data);
        }
        return null;
    }
}

const styles = StyleSheet.create({
    stickySectionText: {
        width: dimensions.Width(70)
    }

});


export default InspirationDeatils;