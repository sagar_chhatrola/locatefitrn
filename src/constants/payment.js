import React from 'react'
import {View, Image} from 'react-native'
import { image } from './../constants/image';
import { DynamicValue, useDynamicValue } from 'react-native-dark-mode'
const lightLogo = image.Paypal
const darkLogo = image.Paypallg

const logoUri = new DynamicValue(lightLogo, darkLogo)

export const PaymentPaypal = (props)=>{
    const source = useDynamicValue(logoUri)
    return <Image source={source} style={{ width: 90, height: 25 }} />
}