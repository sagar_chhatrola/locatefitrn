import React from 'react'
import {View, Image} from 'react-native'
import { image } from './../constants/image';
import { DynamicValue, useDynamicValue } from 'react-native-dark-mode'
const lightLogo = image.logo_main
const darkLogo = image.logo_main1

const logoUri = new DynamicValue(lightLogo, darkLogo)

export const Logo = (props)=>{
    const source = useDynamicValue(logoUri)
    return <Image source={source} style={{ width: 210, height: 38, alignSelf: 'center' }} />
}