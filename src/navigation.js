import React, { Component } from "react";
import {
  createSwitchNavigator,
  createAppContainer,
  
} from "react-navigation";
import { fromLeft, fromRight, fromBottom, zoomOut, zoomIn, fadeIn, fadeOut, fromTop, flipY } from 'react-navigation-transitions';
import { Dimensions } from 'react-native'
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import LandingScreen from "./screens/LandingScreen";
import RegisterScreen from "./screens/RegisterScreen";
import LoginScreen from "./screens/LoginScreen";
import ProfileScreen from "./screens/ProfileScreen";
import ForgotScreen from "./screens/ForgotPassword";
import ConfirmEmail from './screens/ConfirmEmail';
import ProductDetails from './screens/ProductDetails';
import { dimensions  } from "./theme/dimensions";
import DrawerScreen from "./screens/DrawerScreen";
import { Navigator } from 'react-native-popup-navigation'
import ProfilePublic from './screens/Profile';
import Favourites from './screens/Favourites';
import AllProducts from './screens/Allproducts';
import Search from './screens/Search';
import Coments from './screens/Coments';
import Header from './components/Header';
import navigation from './services/NavigationService';
import Address from './screens/Address';
import Mypayment from './screens/Mypayment';
import MyAccount from './screens/Myaccount';
import Orders from './screens/Orders';
import OrdenDetails from './screens/OrdenDetails';
import Pedido from './screens/Pedidos';
import PedidoDetails from './screens/PedidoDetails';
import Notification from './screens/Notification';
import Paymentpage from './screens/Paymentpage';
import Chatscreen from './screens/chatscreen';
import Message from './screens/messageScreen';
import Paymentype from '../stripe-and-paypal/scenes/CardFormScreen';
// import { store, persistor } from "./store/store";
import Configuration from './screens/configuration';
import Myprofile from './screens/Muyprofile';
import Myadds from './screens/myAdds';
import Inspiration from './screens/inspiration';
import InspirationDetails from './screens/InspirationDetails';
import MayInspiration from './screens/myInspiration';
import MessagechatList from './screens/messagechatlist';

const { height } = Dimensions.get('window')


const AuthDrawerNavigator = createDrawerNavigator({
  Profile: {
    screen: ProfileScreen,
  },
},{
  drawerWidth:dimensions.Width(75),
  contentComponent: DrawerScreen,
  drawerPosition: "left"
});

const handleCustomTransition = ({ scenes }) => {
  const prevScene = scenes[scenes.length - 2];
  const nextScene = scenes[scenes.length - 1];
 
  // Custom transitions go there

  if (prevScene
    && prevScene.route.routeName === 'Landing'
    && nextScene.route.routeName === 'Home') {
    return fromBottom(500);
    }

    if (prevScene
      && prevScene.route.routeName === 'Login'
      && nextScene.route.routeName === 'Home') {
      return fromBottom(500);
      }

  if (prevScene
    && prevScene.route.routeName === 'Home'
    && nextScene.route.routeName === 'ProductDetails') {
    return fromBottom(500);
    }

    if (prevScene
      && prevScene.route.routeName === 'Home'
      && nextScene.route.routeName === 'AllProducts') {
      return fromBottom(500);
      }

      if (prevScene
        && prevScene.route.routeName === 'Home'
        && nextScene.route.routeName === 'Search') {
        return fadeOut(500);
        }

    if (prevScene
        && prevScene.route.routeName === 'AllProducts'
        && nextScene.route.routeName === 'Search') {
        return fadeOut(500);
        }

    if (prevScene
      && prevScene.route.routeName === 'AllProducts'
      && nextScene.route.routeName === 'ProductDetails') {
      return fromBottom(500);
      }

      if (prevScene
        && prevScene.route.routeName === 'Search'
        && nextScene.route.routeName === 'AllProducts') {
        return fromBottom(500);
        }

      if (prevScene
        && prevScene.route.routeName === 'Search'
        && nextScene.route.routeName === 'ProductDetails') {
        return fromBottom(500);
        }
    if (prevScene
          && prevScene.route.routeName === 'ProductDetails'
          && nextScene.route.routeName === 'Paymentpage') {
          return fromBottom(500);
          }

  if (prevScene
          && prevScene.route.routeName === 'ProductDetails'
          && nextScene.route.routeName === 'ProfilePublic') {
          return fromBottom(500);
          }

  if (prevScene
          && prevScene.route.routeName === 'ProductDetails'
          && nextScene.route.routeName === 'Message') {
          return fromBottom(500);
          }

   if (prevScene
          && prevScene.route.routeName === 'Favourites'
          && nextScene.route.routeName === 'ProductDetails') {
          return fromBottom(500);
          }       
  if (prevScene
          && prevScene.route.routeName === 'Home'
          && nextScene.route.routeName === 'Message') {
          return fromBottom(500);
          }  
   if (prevScene
          && prevScene.route.routeName === 'AllProducts'
          && nextScene.route.routeName === 'Message') {
          return fromBottom(500);
          }
  if (prevScene
          && prevScene.route.routeName === 'Search'
          && nextScene.route.routeName === 'Message') {
          return fromBottom(500);
          }                       
   if (prevScene
          && prevScene.route.routeName === 'Orders'
          && nextScene.route.routeName === 'OrdenDetails') {
          return fromBottom(500);
          }

    if (prevScene
          && prevScene.route.routeName === 'Pedido'
          && nextScene.route.routeName === 'PedidoDetails') {
          return fromBottom(500);
          }
if (prevScene
          && prevScene.route.routeName === 'Home'
          && nextScene.route.routeName === 'Myprofile') {
          return fromBottom(500);
          }
  if (prevScene
          && prevScene.route.routeName === 'Myprofile'
          && nextScene.route.routeName === 'MyAccount') {
          return fromBottom(500);
          }
          
  if (prevScene
          && prevScene.route.routeName === 'Myprofile'
          && nextScene.route.routeName === 'Chatscreen') {
          return fromBottom(500);
          } 
  if (prevScene
          && prevScene.route.routeName === 'Myprofile'
          && nextScene.route.routeName === 'Address') {
          return fromBottom(500);
          }
 if (prevScene
          && prevScene.route.routeName === 'Myprofile'
          && nextScene.route.routeName === 'Mypayment') {
          return fromBottom(500);
          }
 if (prevScene
          && prevScene.route.routeName === 'Myprofile'
          && nextScene.route.routeName === 'Coments') {
          return fromBottom(500);
          }  
  if (prevScene
          && prevScene.route.routeName === 'Myprofile'
          && nextScene.route.routeName === 'Pedido') {
          return fromBottom(500);
          }                  
  if (prevScene
          && prevScene.route.routeName === 'Myprofile'
          && nextScene.route.routeName === 'Orders') {
          return fromBottom(500);
          }        
  if (prevScene
          && prevScene.route.routeName === 'Myprofile'
          && nextScene.route.routeName === 'Configuration') {
          return fromBottom(500);
          }  
  if (prevScene
          && prevScene.route.routeName === 'Myprofile'
          && nextScene.route.routeName === 'Myadds') {
          return fromBottom(500);
          }  
  if (prevScene
          && prevScene.route.routeName === 'Myprofile'
          && nextScene.route.routeName === 'MayInspiration') {
          return fromBottom(500);
          }  
  if (prevScene
          && prevScene.route.routeName === 'Myadds'
          && nextScene.route.routeName === 'ProductDetails') {
          return fromBottom(500);
          } 
  if (prevScene
          && prevScene.route.routeName === 'Home'
          && nextScene.route.routeName === 'Inspiration') {
          return fromBottom(500);
          } 
if (prevScene
          && prevScene.route.routeName === 'Inspiration'
          && nextScene.route.routeName === 'InspirationDetails') {
          return fromBottom(500);
          } 
  if (prevScene
          && prevScene.route.routeName === 'MayInspiration'
          && nextScene.route.routeName === 'InspirationDetails') {
          return fromBottom(500);
          } 
          
  return fromRight(500);
}

const AuthDrawer = createAppContainer(AuthDrawerNavigator);
const AuthAppNavigator = createStackNavigator(
  {

    Home :{
      screen: ProfileScreen,
      navigationOptions: {
        header: null
      },
    },

    Myprofile :{
      screen: Myprofile,
      navigationOptions: {
        header: null
      },
    },

    Favourites :{
      screen: Favourites,
      navigationOptions: {
        header: null
      },
    },

    Coments:{
      screen: Coments,
      navigationOptions: {
        header: null
      },
    },

    Mypayment:{
      screen: Mypayment,
      navigationOptions: {
        header: null
    }
  },
    
    Address:{
      screen: Address,
      navigationOptions: {
        header: null
      },
    },

    MyAccount:{
      screen: MyAccount,
      navigationOptions: {
        header: null
      },
    },

    Orders:{
      screen: Orders,
      navigationOptions: {
        header: null
      },
    },

    Pedido:{
      screen: Pedido,
      navigationOptions: {
        header: null
      },
    },

    OrdenDetails:{
      screen: OrdenDetails,
      navigationOptions: {
        header: null
      },
    },

    PedidoDetails:{
      screen: PedidoDetails,
      navigationOptions: {
        header: null
      },
    },

    ProductDetails :{
      screen: ProductDetails,
      navigationOptions: {
        header: null
      },
    },

    ProfilePublic :{
      screen: ProfilePublic,
      navigationOptions: {
        header: null
      },
    },

    AllProducts:{
      screen: AllProducts,
      navigationOptions: {
        header: null
      },
    },

    Search:{
      screen: Search,
      navigationOptions: {
        header: null
      },
    },

    Notification:{
      screen: Notification,
      navigationOptions: {
        header: null
      },
    },

    Paymentpage:{
      screen: Paymentpage,
      navigationOptions: {
        header: null
      },
    },
    
    Paymentype :{
      screen: Paymentype,
      navigationOptions: {
        header: null
      },
    },

    Configuration :{
      screen: Configuration,
      navigationOptions: {
        header: null
      },
    },

    Chatscreen :{
      screen: Chatscreen,
      navigationOptions: {
        header: null
      },
    },

    Message :{
      screen: Message,
      navigationOptions: {
        header: null
      },
    },

    Myadds :{
      screen: Myadds,
      navigationOptions: {
        header: null
      },
    },

    Inspiration :{
      screen: Inspiration,
      navigationOptions: {
        header: null
      },
    },
    InspirationDetails :{
      screen: InspirationDetails,
      navigationOptions: {
        header: null
      },
    },

    MayInspiration :{
      screen: MayInspiration,
      navigationOptions: {
        header: null
      },
    },

    MessagechatList :{
      screen: MessagechatList,
      navigationOptions: {
        header: null
      },
    },
  },
  
  {

    transitionConfig: (nav) => handleCustomTransition(nav),
     initialRouteName: "Home",  
    
  }
);






const UnAuthAppNavigator = createStackNavigator(
  
    {
  
      Login :{
        screen: LoginScreen,
        navigationOptions: {
          header: null
        },
      },
      Register :{
        screen: RegisterScreen,
        navigationOptions: {
          header: null
        },
      },
      Home :{
        screen: ProfileScreen,
        navigationOptions: {
          header: null
        },
      },

      Landing :{
        screen: LandingScreen,
        navigationOptions: {
          header: null
        },
      },
      Forgot :{
        screen: ForgotScreen,
        navigationOptions: {
          header: null
        },
      },

      ConfirmEmail :{
        screen: ConfirmEmail,
        navigationOptions: {
          header: null
        },
      },

      ProductDetails :{
        screen: ProductDetails,
        navigationOptions: {
          header: null
        },
      },

      ProfilePublic :{
        screen: ProfilePublic,
        navigationOptions: {
          header: null
        },
      },

      AllProducts:{
        screen: AllProducts,
        navigationOptions: {
          header: null
        },
      },

      Search:{
        screen: Search,
        navigationOptions: {
          header: null
        },
      },

      Inspiration:{
        screen: Inspiration,
        navigationOptions: {
          header: null
        },
      },

      InspirationDetails :{
        screen: InspirationDetails,
        navigationOptions: {
          header: null
        },
      },

    },
    {
       transitionConfig: (nav) => handleCustomTransition(nav),
       initialRouteName: "Landing"
    }
  );

export const UnAuthSwitch = createSwitchNavigator({
    UnAuth: UnAuthAppNavigator,
    Auth: AuthAppNavigator,
})

export const UnAuthContainer = createAppContainer(UnAuthSwitch);


export const AuthSwitch = createSwitchNavigator({
  Auth: AuthAppNavigator,
  UnAuth: UnAuthAppNavigator,
})

export const AuthContainer = createAppContainer(AuthSwitch);