import React from 'react';
import { Text, StyleSheet, View, Image, ActivityIndicator, FlatList, TouchableOpacity, RefreshControl, Share} from 'react-native';
import { colors, dimensions } from '../theme';
import { image } from '../constants/image'
import AntdIcons from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/Feather';
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import { CustomText } from '../components/CustomText';

const USUARIO_FAVORITO_INSPIRATION = gql`
    query{
        getUsuarioFavoritoInspiracion{
          success
          message
          list{
            id
            InspiracionID
            inspiracion{
              id
              title
              description
              image
              image1
              image2
              created_at
              anadidoFavoritoinspiration
            }
          }
        }
      }
      `;

const ELIMINAR_USUARIO_FAVORITE_INSPIRACION = gql`
  mutation eliminarUsuarioFavoritoInspiracion($id: ID!) {
    eliminarUsuarioFavoritoInspiracion(id: $id) {
      success
      message
    }
  }
`;

export default class CardServices extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  refetch = null

  _onRefresh() {
    this.setState({refreshing: true});
    fetchData().then(() => {
      this.setState({refreshing: false});
    });
  }

  onShare = async () => {
    try {
        const result = await Share.share({
            title: 'Locatefit',
            message:
                'Locatefit | Los mejores profeisonales',

        });

        if (result.action === Share.sharedAction) {
            if (result.activityType) {
                // shared with activity type of result.activityType
            } else {
                // shared
            }
        } else if (result.action === Share.dismissedAction) {
            // dismissed
        }
    } catch (error) {
        alert(error.message);
    }
};


  _renderItem(item, refetch) {
    const { navigation } = this.props;
    return (
      <View style={{ marginTop: dimensions.Height(2) }}>
      <TouchableOpacity style={styles.card} onPress={() => navigation.navigate('InspirationDetails', { data: item.inspiracion})}>
          <Image source={{ uri: item.inspiracion.image }} style={{ width: dimensions.Width(100), height: 250, justifyContent: 'center', alignSelf: 'center' }} />
          <View style={{borderBottomColor: colors.rgb_235, borderBottomWidth: 0.5, paddingBottom: 10}}>
          <View style={{flexDirection: 'row'}}>
            <CustomText light={colors.blue_main} dark={colors.white} numberOfLines={1} style={{ width: dimensions.Width(70), fontSize: dimensions.FontSize(16), marginLeft: 10, fontWeight: '300', marginTop: 10}}>
              {item.inspiracion.title}
            </CustomText> 
            <View style={{ marginLeft: 'auto', marginTop: 10 }}> 
            <Mutation mutation={ELIMINAR_USUARIO_FAVORITE_INSPIRACION} variables={{id: item.id}}>
                {(
                  eliminarUsuarioFavoritoInspiracion, data
                ) => {
                  return (
              <TouchableOpacity onPress={() => {
                eliminarUsuarioFavoritoInspiracion({ variables: { id: item.id} }).then(() => {
                  Toast.showWithGravity('Inspiración eliminada correctamente', Toast.LONG, Toast.TOP)
                  refetch()
                  if (
                    data &&
                    data.eliminarUsuarioFavoritoInspiracion &&
                    data.eliminarUsuarioFavoritoInspiracion.success
                    
                  ) {
                    console.log(data.eliminarUsuarioFavoritoInspiracion.message);
                  }  else if (
                    data &&
                    data.eliminarUsuarioFavoritoInspiracion &&
                    !data.eliminarUsuarioFavoritoInspiracion.success
                  )
                    console.log(data.eliminarUsuarioFavoritoInspiracion.message);
                })
                }
                }>
                <AntdIcons name="delete" size={25} color={colors.ERROR} />
              </TouchableOpacity>
              );
                }}
              </Mutation>
            </View>
          <TouchableOpacity style={{marginLeft: 10}} onPress={()=> this.onShare()}>
            <MaterialCommunityIcons style={{marginTop: 10, marginLeft: 'auto', marginRight: 10 }} size={24} name="share" color={colors.black} />
            </TouchableOpacity>     
          </View>
          <Text numberOfLines={1} style={{ width: dimensions.Width(70), fontSize: dimensions.FontSize(14), marginLeft: 10, fontWeight: '200', color: colors.rgb_153}}>
          {moment(Number(item.inspiracion.created_at)).format('LL')}
        </Text>
        </View>
      </TouchableOpacity>
  </View>
    )
  }

  render() {
    return (
      <Query query={USUARIO_FAVORITO_INSPIRATION}>
        {(response, error, loading, refetch) => {
          this.refetch = refetch;
          if (loading) {
            return <ActivityIndicator size="large" color={colos.green_main} />
          }
          if (error) {
            return console.log('Response Error-------', error);
          }
          if (response) {
            console.log(response)
            return <FlatList
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh.bind(this)}
              />}
              keyExtractor={item => item.id}
              data={response && response.data ? response.data.getUsuarioFavoritoInspiracion.list : ''}
              renderItem={({ item }) => this._renderItem(item, response.refetch)}
              ListEmptyComponent={
                <View style={{ alignSelf: 'center', padding: 20}}>
                  <Image source={image.Vacia} style={{width: dimensions.Width(34), alignSelf: 'center', height: dimensions.Height(13)}} />
                  <CustomText light={colors.blue_main} dark={colors.white} style={{textAlign: 'center', fontSize: dimensions.FontSize(20),fontWeight: '200'}}>Aún no has guardado ninguna inspiración</CustomText>
              </View>
              }
            />;
          }
        }}
      </Query>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    paddingHorizontal: dimensions.Width(4)
},
})
