import React from 'react';
import { TextInput, StyleSheet,View,Text } from 'react-native';
import { dimensions, colors } from './../theme';
import { CustomText } from './CustomText';

export const PlainTextInput = (props) => (
    
      
      <TextInput {...props} style={[props.style,styles.textInputStyle]} />
    
)

const styles = StyleSheet.create({
    textInputStyle:{
        paddingVertical:0,
       
    },
    hashTrix:{
        color:colors.red,
        marginVertical:dimensions.Height(1)
    },
    text:{
        color: colors.black,
        fontSize: dimensions.FontSize(14),
        justifyContent: 'center',
        textAlignVertical: 'center',
        alignContent: 'center',
        marginVertical: dimensions.Height(2),
    }
});


