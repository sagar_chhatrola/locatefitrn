import React from 'react';
import { Text } from 'react-native';
import { DynamicStyleSheet, DynamicValue, useDynamicStyleSheet } from 'react-native-dark-mode'
import { colors, dimensions } from './../theme';
import AntdIcons from 'react-native-vector-icons/AntDesign';

export const IconsD = (props) => {
  const dynamicStyles = new DynamicStyleSheet({
    colores: {
      color: new DynamicValue(props.light, props.dark),
    }
  })
  const styles = useDynamicStyleSheet(dynamicStyles)
  return(
    <AntdIcons name={props.name} size={props.size} style={styles.colores} />
    )
  };