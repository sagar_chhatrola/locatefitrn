import React from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity, Dimensions, } from 'react-native';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import LinearGradient from 'react-native-linear-gradient';
import { DynamicStyleSheet, DynamicValue, useDynamicStyleSheet } from 'react-native-dark-mode'
import { colors, dimensions } from './../theme';


export default Headersparallax = props => {
  const styles = useDynamicStyleSheet(dynamicStyles)
  return (
    <ParallaxScrollView
    showsVerticalScrollIndicator={false}
    headerBackgroundColor="white"
    stickyHeaderHeight={ STICKY_HEADER_HEIGHT }
    parallaxHeaderHeight={ PARALLAX_HEADER_HEIGHT }
    backgroundSpeed={10}

    renderBackground={() => (
      
      <View key="background">
      <LinearGradient colors={['#000000', '#000000', '#000000']} style={{ position: 'absolute',
                                                                          top: 0,
                                                                          width: window.width,
                                                                          height: PARALLAX_HEADER_HEIGHT
                                                                          }}>
       
    </LinearGradient>
      </View>
    )}

    renderForeground={() => (
      <View key="parallax-header" style={ styles.parallaxHeader }>
        {props.avatar}
        <View style={ styles.sectionSpeakerText }>
          {props.name}
        </View>
        <Text style={ styles.sectionTitleText }>
          {props.descripcion}
        </Text>
      </View>
    )}
    renderFixedHeader={() => (
      <View key="fixed-header" style={styles.fixedSection}>
        <View style={styles.fixedSectionText}>
          {props.back}
        </View>
      </View>
    )}


    renderStickyHeader={() => (
      <View key="sticky-header" style={styles.stickySection}>
    <View style={styles.stickySectionText}>{props.name1}</View>
      </View>
    )}
    >
      <View style={styles.container}>
        {props.component}
      </View>
    </ParallaxScrollView>
  );
}



const window = Dimensions.get('window');

const AVATAR_SIZE = 90;
const ROW_HEIGHT = 60;
const PARALLAX_HEADER_HEIGHT = 280;
const STICKY_HEADER_HEIGHT = 90;


const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.white, colors.black),
},
imagenbg:{
  width: window.width,
  height: PARALLAX_HEADER_HEIGHT
},
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    width: '100%',
  },
  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: '100%',
    backgroundColor: new DynamicValue(colors.white, colors.black),
    justifyContent: 'flex-end',
    
  },
  stickySectionText: {
    color: '#95ca3e',
    fontSize: 20,
    margin: 10,
    marginTop: 15
  },
  fixedSection: {
    color: '#95ca3e',
    position: 'absolute',
    bottom: 10,
    right: 10,
    flexDirection: 'row',
  },
  fixedSectionText: {
    color: '#95ca3e',
    fontSize: 16,
    marginRight: 0,
    width: 35,
    height: 35,
    borderRadius: 25,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },

  fixedSectionText1: {
    color: '#95ca3e',
    fontSize: 16,
    marginRight: 15,
    width: 35,
    height: 35,
    borderRadius: 25,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  parallaxHeader: {
    alignItems: 'center',
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    paddingTop: 70,
    
    
  },
  avatar: {
    marginBottom: 10,
    borderRadius: AVATAR_SIZE / 2,
    borderWidth: 2,
    borderColor: '#d6d7da',
    width: AVATAR_SIZE, 
    height: AVATAR_SIZE
  },
  sectionSpeakerText: {
    color: 'white',
    fontSize: 24,
    paddingVertical: 5
  },
  sectionTitleText: {
    color: 'white',
    fontSize: 14,
    textAlign: 'center',
    paddingVertical: 5,
    paddingHorizontal: dimensions.Width(4)
  },
  row: {
    overflow: 'hidden',
    paddingHorizontal: 10,
    height: ROW_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    borderColor: '#ccc',
    borderBottomWidth: 1,
    justifyContent: 'center'
  },
  rowText: {
    fontSize: 20
  },

  header1:{
    width: '100%',
    height: 80,
    marginTop: 'auto'
},


});