import React from 'react';
import { Text, StyleSheet, View, Image, ActivityIndicator, FlatList, TouchableOpacity, RefreshControl } from 'react-native';
import { colors, dimensions } from '../theme';
import { image } from '../constants/image'
import AntdIcons from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/Feather';
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { NETWORK_INTERFACE_LINK_AVATAR } from '../constants/config';
import Toast from 'react-native-simple-toast';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';
import { CustomText } from '../components/CustomText';

const USUARIO_FAVORITO_PRODUCTS = gql`
    query {
          getUsuarioFavoritoProductos {
            success
            message
            list {
              id
              productoId
              producto {
                  id
                  city
                  category_id
                  currency
                  description
                  domingo
                  domingo_from
                  domingo_to
                  jueves
                  jueves_from
                  jueves_to
                  lunes
                  lunes_from
                  lunes_to
                  martes
                  martes_from
                  martes_to
                  miercoles
                  miercoles_from
                  miercoles_to
                  number
                  sabado
                  sabado_from
                  sabado_to
                  time
                  title
                  visitas
                  viernes
                  viernes_from
                  viernes_to
                  anadidoFavorito
                  fileList
                  created_by
                  category {
                    id
                    title
                  }
                  ordenes{
                    id
                       }
                  professionalRating {
                    id
                    coment
                    rate
                    updated_at
                    customer {
                      foto_del_perfil
                      nombre
                      apellidos
                      profesion
                      ciudad
                    }
                  }
                  creator {
                    id
                    usuario
                    Verified
                    email
                    nombre
                    apellidos
                    UserID
                    ciudad
                    telefono
                    tipo
                    foto_del_perfil
                    fotos_tu_dni
                    profesion
                    descripcion
                    fecha_de_nacimiento
                    notificacion
                    grado
                    estudios
                    formularios_de_impuesto
                    fb_enlazar
                    twitter_enlazar
                    instagram_enlazar
                    youtube_enlazar
                    propia_web_enlazar
                  }
                }
                }
              }
            }
          `;

const ELIMINAR_USUARIO_FAVORITE_PRODUCTO = gql`
mutation eliminarUsuarioFavoritoProducto($id: ID!) {
  eliminarUsuarioFavoritoProducto(id: $id) {
    success
    message
  }
}
`;

export default class CardServices extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  refetch = null

  _onRefresh() {
    this.setState({refreshing: true});
    fetchData().then(() => {
      this.setState({refreshing: false});
    });
  }


  _renderItem(item, refetch) {
    let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
    item.producto.professionalRating.forEach(start => {
      if (start.rate == 1) rating['1'] += 1;
      else if (start.rate == 2) rating['2'] += 1;
      else if (start.rate == 3) rating['3'] += 1;
      else if (start.rate == 4) rating['4'] += 1;
      else if (start.rate == 5) rating['5'] += 1;
    });
    const ar = (5 * rating['5'] + 4 * rating['4'] + 3 * rating['3'] + 2 * rating['2'] + 1 * rating['1']) / item.producto.professionalRating.length;
    let averageRating = 0;
    if (item.producto.professionalRating.length) {
      averageRating = ar.toFixed(1);
    }
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetails', { data: item.producto })}>
        <View style={styles.card}>
          <View style={{ width: dimensions.Width(60), alignSelf: 'center', padding: 5 }}>
            <View style={{ flexDirection: 'row' }}>
              <CustomText light={colors.black} dark={colors.white} numberOfLines={1} style={{ fontSize: dimensions.FontSize(15), fontWeight: '500', width: dimensions.Width(50) }}>{item.producto.title}</CustomText>
              <Mutation mutation={ELIMINAR_USUARIO_FAVORITE_PRODUCTO} variables={{id: item.id}}>
                {(
                  eliminarUsuarioFavoritoProducto, data
                ) => {
                  return (
                    <TouchableOpacity onPress={() => {
                      eliminarUsuarioFavoritoProducto({ variables: { id: item.id } }).then(() => {
                        Toast.showWithGravity('Producto eliminado con éxito a al lista de deseos', Toast.LONG, Toast.TOP)
                        refetch()
                        if (
                          data &&
                          data.eliminarUsuarioFavoritoProducto &&
                          data.eliminarUsuarioFavoritoProducto.success
                        ) {
                          console.log(data.eliminarUsuarioFavoritoProducto.message);
                        }  else if (
                          data &&
                          data.eliminarUsuarioFavoritoProducto &&
                          !data.eliminarUsuarioFavoritoProducto.success
                        )
                          console.log(data.eliminarUsuarioFavoritoProducto.message);
                      })
                      
                    }
                    }>
                      <AntdIcons name="delete" size={24} color={colors.ERROR} style={{ marginLeft: 'auto' }} />
                    </TouchableOpacity>
                  );
                }}
              </Mutation>

            </View>
            <CustomText light={colors.rgb_153} dark={colors.light_white} numberOfLines={3} style={{fontWeight: '200' }}>{item.producto.description}</CustomText>
            <View style={{ flexDirection: 'row', marginTop: 10 }}>
              <CustomText light={colors.light_blue} dark={colors.light_white} style={{ color: colors.green_main }}>{item.producto.number}€<CustomText light={colors.blue_main} dark={colors.light_white}>{item.producto.currency}</CustomText></CustomText>
              <CustomText light={colors.blue_main} dark={colors.light_white} style={{ marginLeft: 20, fontSize: dimensions.FontSize(12) }}><AntdIcons name="staro" size={16} color={colors.orange} /> ({averageRating})</CustomText>
              <CustomText light={colors.blue_main} dark={colors.light_white} style={{ marginLeft: 20, fontWeight: '300', fontSize: dimensions.FontSize(12)}}><MaterialCommunityIcons name="map-pin" size={16} color={colors.green_main} /> {item.producto.city}</CustomText>
            </View>
          </View>
          {item.producto.fileList.length ?
            <View style={{ marginLeft: 'auto', alignSelf: 'center' }}>
              {item.producto.fileList.length > 0 ?
                <Image source={{ uri: item.producto.fileList.length > 0 ? NETWORK_INTERFACE_LINK_AVATAR + item.producto.fileList[0] : "" }} style={styles.img} /> : null}
            </View>
            : null
            }
            {(item.producto.fileList.length === 0 || !item.producto.fileList) ?
              <View style={{ marginLeft: 'auto', alignSelf: 'center' }}>
                <Image source={image.No_img} style={styles.img} />
              </View>
              : null 
              }
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <Query query={USUARIO_FAVORITO_PRODUCTS}>
        {(response, error, loading, refetch) => {
          this.refetch = refetch;
          if (loading) {
            return <ActivityIndicator size="large" color={colos.green_main} />
          }
          if (error) {
            return console.log('Response Error-------', error);
          }
          if (response) {
            return <FlatList
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh.bind(this)}
              />}
              keyExtractor={item => item.id}
              data={response && response.data ? response.data.getUsuarioFavoritoProductos.list : ''}
              renderItem={({ item }) => this._renderItem(item, response.refetch)}
              ListEmptyComponent={
                <View style={{ alignSelf: 'center', padding: 20}}>
                  <Image source={image.Vacia} style={{width: dimensions.Width(34), alignSelf: 'center', height: dimensions.Height(13)}} />
                  <Text style={{textAlign: 'center', fontSize: dimensions.FontSize(20), color: colors.blue_main, fontWeight: '200'}}>Aún no tienes servicios en la lista de deseos</Text>
              </View>
              }
            />;
          }
        }}
      </Query>
    )
  }
}


const styles = StyleSheet.create({
  card: {
    width: dimensions.Width(90),
    height: dimensions.Height(16),
    borderRadius: 10,
    backgroundColor: 'transparent',
    marginBottom: dimensions.Width(8),
    flexDirection: 'row',
    alignSelf: 'center'
  },
  img: {
    width: dimensions.Width(22),
    height: dimensions.Height(10),
    backgroundColor: colors.white,
    borderRadius: 7,
    marginLeft: 'auto',
  }
})
