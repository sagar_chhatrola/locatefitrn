import React from 'react';
import { StyleSheet, View, TouchableOpacity, SafeAreaView} from 'react-native';
import { colors, dimensions } from '../theme';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { CustomText } from '../components/CustomText';
import AsyncStorage from '@react-native-community/async-storage';
import { Badge } from '@ant-design/react-native';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';


const USUARIO_FAVORITO_PRODUCTS = gql`
    query {
          getUsuarioFavoritoProductos {
            success
            message
            list {
              id
              productoId
              producto {
                  id
                  city
                  category_id
                  currency
                  description
                  domingo
                  domingo_from
                  domingo_to
                  jueves
                  jueves_from
                  jueves_to
                  lunes
                  lunes_from
                  lunes_to
                  martes
                  martes_from
                  martes_to
                  miercoles
                  miercoles_from
                  miercoles_to
                  number
                  sabado
                  sabado_from
                  sabado_to
                  time
                  title
                  visitas
                  viernes
                  viernes_from
                  viernes_to
                  anadidoFavorito
                  fileList
                  created_by
                  category {
                    id
                    title
                  }
                  ordenes{
                    id
                       }
                  professionalRating {
                    id
                    coment
                    rate
                    updated_at
                    customer {
                      foto_del_perfil
                      nombre
                      apellidos
                      profesion
                      ciudad
                    }
                  }
                  creator {
                    id
                    usuario
                    email
                    nombre
                    apellidos
                    ciudad
                    telefono
                    tipo
                    foto_del_perfil
                    fotos_tu_dni
                    profesion
                    descripcion
                    fecha_de_nacimiento
                    notificacion
                    grado
                    estudios
                    formularios_de_impuesto
                    fb_enlazar
                    twitter_enlazar
                    instagram_enlazar
                    youtube_enlazar
                    propia_web_enlazar
                  }
                }
                }
              }
            }
          `;

const GET_NOTIFICATIONS = gql`
query getNotifications($userId: ID!) {
  getNotifications(userId: $userId) {
      success
      message
      notifications{
        _id
        user{
          id
          nombre
          apellidos
          foto_del_perfil
        }
        profesional{
          id
          nombre
          apellidos
          foto_del_perfil
        }
        cliente{
          id
          nombre
          apellidos
          foto_del_perfil
        }
        orden{
          id
        }
        type
        read
        createdAt
      }
  }
}
`;



class Header extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
        user: ''
    }; 
  }

  async componentDidMount() {
    const user = await AsyncStorage.getItem('id');

    if(user) {
      this.setState({ user: user })
    } else{
      return null
    }
    
  }

    render(){
        const { navigation }= this.props;
        return(
          <ViewCuston light='transparent' dark={colors.black} containers={
            <SafeAreaView style={styles.headers}>
            <View style={{ flexDirection: 'row', marginTop: 20 }}>
            <View style={{ alignItems: 'flex-start', marginLeft: 0 }}>
              <TouchableOpacity onPress={() => navigation.goBack(null)}>
                <CustomText>
                  <IconsD name="arrowleft" size={25} light={colors.blue_main} dark={colors.white} />
                </CustomText>
              </TouchableOpacity>
            </View>
            <View style={{ marginLeft: 'auto', flexDirection: 'row' }}>
            <Query query={GET_NOTIFICATIONS} variables={{userId: this.state.user}}>
              {(response, error, loading, refetch) => {
                this.refetch = refetch;
                if (loading) {
                  return null
                }
                if (error) {
                  return console.log('Response Error-------', error);
                }
                if (response) {
                  const datoscouter = response && response.data && response.data.getNotifications ? response.data.getNotifications.notifications.length : ''
              return(
                  <Badge text={datoscouter} overflowCount={9} style={{ marginRight: 15}}>
                    <TouchableOpacity onPress={()=> navigation.navigate('Notification')}>
                      <CustomText>
                          <IconsD name="bells" size={25} light={colors.blue_main} dark={colors.white} />
                      </CustomText>
                    </TouchableOpacity>
                  </Badge>
                  )
                    }
                  }}
                </Query>
              <Query query={USUARIO_FAVORITO_PRODUCTS}>
              {(response, error, loading, refetch) => {
                this.refetch = refetch;
                if (loading) {
                  return null
                }
                if (error) {
                  return console.log('Response Error-------', error);
                }
                if (response) {
                  const datos = response && response.data ? response.data.getUsuarioFavoritoProductos.list.length: ''
              return(
                  <Badge text={datos} overflowCount={9} style={{ marginRight: 0}}>
                    <TouchableOpacity onPress={()=> navigation.navigate('Favourites')}>
                      <CustomText>
                        <IconsD name="hearto" size={25} light={colors.blue_main} dark={colors.white} />
                      </CustomText>
                    </TouchableOpacity>
                  </Badge>
                      )
                    }
                  }}
                </Query>
            </View>
            </View>
            </SafeAreaView>
          }/>
        )
    }
}

const styles = StyleSheet.create({
    headers: {
      height: dimensions.Height(13),
      backgroundColor: 'transparent',
    },
    
});

export default Header;



