import React from 'react'
import {View} from 'react-native'
import { DynamicStyleSheet, DynamicValue, useDynamicStyleSheet } from 'react-native-dark-mode'
import { colors, dimensions } from './../theme';

export const ViewCuston = (props)=>{
    const dynamicStyles = new DynamicStyleSheet({
        fondo: {
            backgroundColor: new DynamicValue(props.light, props.dark),
      }
    })
    
    const styles = useDynamicStyleSheet(dynamicStyles)
    return <View style={[styles.fondo, props.style]}>
                {props.containers}
           </View>
}