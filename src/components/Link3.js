'use strict';
import { Button } from './Button';
var React = require('react');
var PropTypes = require('prop-types');
var ReactNative = require('react-native');
var { Linking, View, StyleSheet} = ReactNative;
import { colors, dimensions } from '../theme';



export default class OpenURLButton extends React.Component {
    static propTypes = {
        url: PropTypes.string,
    };

    handleClick = () => {
        Linking.canOpenURL(this.props.url).then(supported => {
            if (supported) {
                Linking.openURL(this.props.url);
            } else {
                console.log("Don't know how to open URI: " + this.props.url);
            }
        });
    };

    render() {
        return (
            <View style={styles.buttonView}>
                <Button onPress={this.handleClick} light={this.props.light} dark={this.props.dark} title={this.props.text} titleStyle={styles.buttonTitle} />
            </View>

        );
    }
}

const styles = StyleSheet.create({
      buttonTitle:{
        fontSize: dimensions.FontSize(16),
        fontWeight: '300'
      },
      buttonView:{
        backgroundColor: 'transparent',
      },

});
