'use strict';
var React = require('react');
var PropTypes = require('prop-types');
var ReactNative = require('react-native');
var { Linking, View} = ReactNative;
import { SocialIcon } from 'react-native-elements'

export default class OpenURLButton extends React.Component {
    static propTypes = {
        url: PropTypes.string,
    };

    handleClick = () => {
        Linking.canOpenURL(this.props.url).then(supported => {
            if (supported) {
                Linking.openURL(this.props.url);
            } else {
                console.log("Don't know how to open URI: " + this.props.url);
            }
        });
    };

    render() {
        return (
            <View style={{ marginTop: 15, marginBottom: 15 }}>
                <SocialIcon
                    onPress={this.handleClick}
                    type={this.props.type}
                /> 
            </View>

        );
    }
}