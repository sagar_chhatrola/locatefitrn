import React, { Component } from 'react';
import { View, Image, ImageBackground, StyleSheet, Alert, Linking, TouchableOpacity,RefreshControl, ActivityIndicator, FlatList} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { colors, dimensions } from './../theme';
import { CustomText } from './../components/CustomText';
import { Button } from './../components/Button';
import { user, image } from './../constants';
import { ViewCuston } from '../components/CustonView';
import { IconsD } from '../components/Icons';

import { Query } from 'react-apollo';
import gql from 'graphql-tag';

const categories = gql`
    query {
      getCategories {
        id
        title
        image
        description
      }
    }
`


class Category extends Component {

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  _onRefresh() {
    this.setState({refreshing: true});
    fetchData().then(() => {
      this.setState({refreshing: false});
    });
  }

  renderItem = (item) => {
           const { navigation } = this.props;
          return (
            <View style={{marginTop: 20}}>
            <ViewCuston light={colors.white} dark={colors.back_dark} style={styles.card} containers={
              <KeyboardAwareScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
                <TouchableOpacity  onPress={() => navigation.navigate('AllProducts', {data: item})}>
                    <View>
                        <CustomText light={colors.blue_main} dark={colors.white} style={{fontSize:dimensions.FontSize(16), fontWeight: '300', paddingHorizontal: 10}}>{item.title}</CustomText>
                    </View>
                </TouchableOpacity>
                </KeyboardAwareScrollView>
                }
              />
            </View>
            )
          }

    render() {
      return (
        <Query query={categories}>
        {(response, error, loading) => {
          if (loading) {
            return (<CustomText>
              Loading.....
            </CustomText>)
          }
          if (error) {
            return console.log('Response Error-------', error);
          }
          if (response) {
            return (<FlatList
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh.bind(this)}
                />}
              data={response && response.data ? response.data.getCategories : ''}
              horizontal={true}
              keyExtractor={(item) => item.id}
              showsHorizontalScrollIndicator={false}
              renderItem={({ item }) => this.renderItem(item)}
            />);
          }
        }}
      </Query>
            
            
      )}
    }

    export default connect(
        null,
        {  }
      )(Category);

      const styles = StyleSheet.create({
                card: {
                  padding: 10,
                  justifyContent: 'center',
                  alignSelf:'center',
                  shadowColor: colors.rgb_153,
                  shadowOffset: { width: 0, height: 1 },
                  shadowRadius: 8,
                  shadowOpacity: 0.1,
                  elevation: 1,
                  flexDirection: 'row',
                  margin: 5,
                  borderRadius: 30,
                }
      })