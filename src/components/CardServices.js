import React from 'react';
import { Text, StyleSheet, View, Image, ActivityIndicator, FlatList, TouchableOpacity } from 'react-native';
import { colors, dimensions } from '../theme';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { image } from '../constants/image'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { NETWORK_INTERFACE_LINK_AVATAR } from '../constants/config';
import AsyncStorage from '@react-native-community/async-storage';
import { CustomText } from '../components/CustomText';

const PROFESSIONAL_ORDENES_QUERY = gql`
  query getOrdenesByProfessional($profesional: ID!, $dateRange: DateRangeInput) {
    getOrdenesByProfessional(profesional: $profesional, dateRange: $dateRange) {
      success
      message
      list {
        id
        cupon
        nota
        aceptaTerminos
        endDate
        time
        producto
        cantidad
        cliente
        profesional
        estado
        progreso
        status
        created_at
        prfsnl{
          id
        }
        customer{
          id
        }
        product {
          id
          city
          category_id
          currency
          description
          domingo
          domingo_from
          domingo_to
          jueves
          jueves_from
          jueves_to
          lunes
          lunes_from
          lunes_to
          martes
          martes_from
          martes_to
          miercoles
          miercoles_from
          miercoles_to
          number
          sabado
          sabado_from
          sabado_to
          time
          title
          visitas
          viernes
          viernes_from
          viernes_to
          anadidoFavorito
          fileList
          created_by
        professionalRating {
          id
          coment
          rate
          updated_at
          customer {
            foto_del_perfil
            nombre
            apellidos
            profesion
            ciudad
            }
          }
        }
        client {
          nombre
          calle
          ciudad
          provincia
          codigopostal
          telefono
        }
        pagoPaypal {
          idPago
          idPagador
          fechaCreacion
          emailPagador
          nombrePagador
          apellidoPagador
          estadoPago
          moneda
          montoPagado
        }
      }
    }
  }
`;

export default class CardServices extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      ordenes: [],
      dataLoading: true,
      profesional: null

    }
  }

  _renderItem({ item }) {
    let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
    item.product.professionalRating.forEach(start => {
      if (start.rate == 1) rating['1'] += 1;
      else if (start.rate == 2) rating['2'] += 1;
      else if (start.rate == 3) rating['3'] += 1;
      else if (start.rate == 4) rating['4'] += 1;
      else if (start.rate == 5) rating['5'] += 1;
    });

    const ar = (5 * rating['5'] + 4 * rating['4'] + 3 * rating['3'] + 2 * rating['2'] + 1 * rating['1']) / item.product.professionalRating.length;
      let averageRating = 0;
      if(item.product.professionalRating.length){
        averageRating = ar.toFixed(1);
      }
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('OrdenDetails', { data: item })}>
        <View style={styles.card}>
          <View style={{ width: dimensions.Width(60), alignSelf: 'center', padding: 5 }}>
          <CustomText light={colors.black} dark={colors.white} numberOfLines={1} style={{ fontSize: dimensions.FontSize(15), fontWeight: '500' }}>{item.product.title}</CustomText>
          <CustomText light={colors.rgb_153} dark={colors.light_white} numberOfLines={3} style={{ marginTop: 10, fontWeight: '200' }}>{item.product.description}</CustomText>
          <View style={{ flexDirection: 'row', marginTop: 10 }}>
          <CustomText light={colors.blue_main} dark={colors.light_white}>{item.product.number}€<CustomText light={colors.blue_main} dark={colors.light_white}>{item.product.currency}</CustomText></CustomText>
          <CustomText light={colors.blue_main} dark={colors.light_white} style={{ marginLeft: 20, fontSize: dimensions.FontSize(12) }}><AntdIcons name="staro" size={16} color={colors.orange} /> ({averageRating})</CustomText>
            <Text style={{ marginLeft: 20, fontWeight: '300', fontSize: dimensions.FontSize(12), color: colors.green_main }}><MaterialCommunityIcons name="progress-check" size={16} color={colors.green_main} /> {item.estado}</Text>
          </View>
          </View>
          {item.product.fileList.length ?
          <View style={{ marginLeft: 'auto', alignSelf: 'center' }}>
            {item.product.fileList.length > 0 ?
              <Image source={{ uri: item.product.fileList.length > 0 ? NETWORK_INTERFACE_LINK_AVATAR + item.product.fileList[0] : "" }} style={styles.img} /> : null}
          </View>
          : null
          }
          {(item.product.fileList.length === 0 || !item.product.fileList) ?
            <View style={{ marginLeft: 'auto', alignSelf: 'center' }}>
              <Image source={image.No_img} style={styles.img} />
            </View>
            : null 
            }
        </View>
      </TouchableOpacity>
    )
  }

  formatResult = data => {
    if (data && data.getOrdenesByProfessional && data.getOrdenesByProfessional.success) {
      this.setState({
        dataLoading: false,
        ordenes: data.getOrdenesByProfessional.list
      });
      console.log(data.getOrdenesByProfessional.list)
    }
  }

  componentDidMount = async () => {
    const profesional = await AsyncStorage.getItem('id');
    this.setState({ profesional })
  }

  render() {
    const { profesional } = this.state;
    return (
      <Query query={PROFESSIONAL_ORDENES_QUERY} variables={{ profesional }} onCompleted={this.formatResult}>
        {(response, error, loading, refetch) => {
          this.refetch = refetch;
          if (loading) return 'Loading...'
          if (error) {
            return console.log('Response Error-------', error);
          }
          if (response) {
            return <FlatList
            data={this.state.ordenes}
            showsHorizontalScrollIndicator={false}
            renderItem={(item) => this._renderItem(item, response.refetch)}
            keyExtractor={item => item.id}
            ListEmptyComponent={
              <View style={{ alignSelf: 'center', padding: 20}}>
                <Image source={image.Vacia} style={{width: dimensions.Width(34), alignSelf: 'center', height: dimensions.Height(13)}} />
                <CustomText light={colors.blue_main} dark={colors.light_white} style={{textAlign: 'center', fontSize: dimensions.FontSize(20), fontWeight: '200'}}>Aún no tienes ordenes para procesar</CustomText>
            </View>
            }
          />;
          }
        }}
      </Query>
    )
  }
}


const styles = StyleSheet.create({
  card: {
    width: dimensions.Width(90),
    height: dimensions.Height(16),
    borderLeftColor: colors.ERROR,
    backgroundColor: 'transparent',
    marginBottom: dimensions.Height(5),
    flexDirection: 'row',
    alignSelf: 'center'
  },
  img: {
    width: dimensions.Width(22),
    height: dimensions.Height(10),
    backgroundColor: colors.white,
    borderRadius: 7,
    marginLeft: 'auto',
  }
})
