import React, { Component } from 'react';
import { View, Image, ImageBackground, StyleSheet, Alert, Linking, TouchableOpacity,RefreshControl, ActivityIndicator, FlatList} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { colors, dimensions } from './../theme';
import { CustomText } from './../components/CustomText';
import { Button } from './../components/Button';
import { user, image } from './../constants';

import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { ViewCuston } from './CustonView';

const categories = gql`
    query {
      getCategories {
        id
        title
        image
        description
      }
    }
`


class Category extends Component {

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  _onRefresh() {
    this.setState({refreshing: true});
    fetchData().then(() => {
      this.setState({refreshing: false});
    });
  }

  renderItem = (item) => {
           const { navigation } = this.props;
          return (
             
              <KeyboardAwareScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
              <ViewCuston light={colors.light_white} dark={colors.back_dark} style={{
                  height: 100,
                  width: 220,
                  padding: 10,
                  justifyContent: 'center',
                  alignSelf:'center',
                  flexDirection: 'row',
                  margin: 10,
                  borderRadius: 10,
                  borderRightWidth: 5,
                  borderRightColor: colors.green_main,
                }} containers={
                <TouchableOpacity style={styles.card} onPress={() => navigation.navigate('AllProducts', {data: item})}>
                    <Image source={{uri: item.image}} style={{width: 50, height: 45, justifyContent: 'center',alignSelf:'center', marginLeft: 10}} />
                    <View>
                        <CustomText light={colors.blue_main} dark={colors.white} numberOfLines={1} style={{fontSize:dimensions.FontSize(14), fontWeight: 'bold', marginTop: 10, paddingHorizontal: 20, width: 150}}>{item.title}</CustomText>

                        <CustomText light={colors.rgb_153} dark={colors.light_grey} numberOfLines={2} style={{marginTop:dimensions.Height(1),fontSize:dimensions.FontSize(12),alignSelf:'flex-start', paddingHorizontal: 20, textAlign: 'left', width: 160}}>{item.description}</CustomText>
                    </View>
                </TouchableOpacity>
                }/>
                </KeyboardAwareScrollView>
              
            )
          }

    render() {
      return (
        <Query query={categories}>
        {(response, error, loading) => {
          if (loading) {
            return (<CustomText>
              Loading.....
            </CustomText>)
          }
          if (error) {
            return console.log('Response Error-------', error);
          }
          if (response) {
            return (<FlatList
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh.bind(this)}
                />}
              data={response && response.data ? response.data.getCategories : ''}
              horizontal={true}
              keyExtractor={(item) => item.id}
              showsHorizontalScrollIndicator={false}
              renderItem={({ item }) => this.renderItem(item)}
            />);
          }
        }}
      </Query>
            
            
      )}
    }

    export default connect(
        null,
        {  }
      )(Category);

      const styles = StyleSheet.create({
                card: {
                  height: 100,
                  width: 220,
                  padding: 10,
                  justifyContent: 'center',
                  alignSelf:'center',
                  flexDirection: 'row',
                  margin: 10,
                  borderRadius: 10,
                  borderRightWidth: 5,
                  borderRightColor: colors.green_main,
                }
      })