import React from 'react'
import {TouchableOpacity,Text} from 'react-native'
import { CustomText } from '../components/CustomText'
import { DynamicStyleSheet, DynamicValue, useDynamicStyleSheet } from 'react-native-dark-mode'

export const Button = (props)=>{

    const dynamicStyles = new DynamicStyleSheet({
        colord: {
            color: new DynamicValue(props.dark, props.light),
      }
    })
    const styles = useDynamicStyleSheet(dynamicStyles)
    return <TouchableOpacity style={props.containerStyle} onPress={()=>props.onPress()}>
                <CustomText  style={[props.titleStyle, styles.colord]}>{props.title}</CustomText>
           </TouchableOpacity>
}


