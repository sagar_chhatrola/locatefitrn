//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Leudy Martes on 12/02/2020.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
