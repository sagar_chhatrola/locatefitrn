import React, { PureComponent } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, Modal, SafeAreaView, TouchableHighlight, ScrollView} from 'react-native'
import { WebView } from 'react-native-webview';
import stripe from 'tipsi-stripe'
import { colors, dimensions } from '../../src/theme';
import { image } from '../../src/constants';
import AntdIcons from 'react-native-vector-icons/AntDesign';
import { Button } from '../../src/components/Button';
import navigation from 'react-native-popup-navigation/src/commonjs/components/navigation';
import _get from 'lodash.get';
import { withApollo } from 'react-apollo';
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { NETWORK_INTERFACE_LINK } from '../../src/constants/config';
import axios from "axios";
import AsyncStorage from '@react-native-community/async-storage';
import Header from './../../src/components/Header';
import { ViewCuston } from '../../src/components/CustonView';
import { IconsD } from '../../src/components/Icons';
import { CustomText } from '../../src/components/CustomText';
import {PaymentPaypal} from '../../src/constants/payment';
import {PaymentVisa} from '../../src/constants/paymentvisa';


const GET_FULL_ORDEN = gql`
  query getFullOrden($id: ID!) {
    getFullOrden(id: $id) {
      id
      producto {
        id
        anadidoFavorito
        city
        currency
        created_by
        description
        fileList
        number
        time
        title
      }
      cupon {
        id
        descuento
        tipo
        clave
      }
      direccion {
        id
        calle
        ciudad
        codigopostal
        nombre
        telefono
        provincia
        usuario_id
      }
      cliente {
        id
        nombre
        apellidos
        notificacion
        telefono
        foto_del_perfil
        fotos_tu_dni
        fecha_de_nacimiento
        ciudad
        descripcion
        email
        grado
        usuario
        tipo
      }
      cantidad
      profesional {
        id
        nombre
        apellidos
        notificacion
        telefono
        foto_del_perfil
        fotos_tu_dni
        fecha_de_nacimiento
        ciudad
        descripcion
        email
        grado
        usuario
        tipo
      }
      nota
      aceptaTerminos
    }
  }  
`;
  
stripe.setOptions({
  publishableKey: 'pk_live_l2imKnAkvWpjXopC3A7Z1NnV002pWA8LAK'
})

class CardFormScreen extends PureComponent {
  static title = 'Pagar'
constructor(props){
  super(props);
   this.state = {
    loading: false,
    token: null,
    modalVisible: false,
    showModal: false,
    status: "Pending",
    complete: false,
    ordenID: '',  
    orden: '', 
    precio: 1,
    ordendata: ''
  }
  this.submit = this.submit.bind(this);
}
  
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
}

 componentDidMount = async () =>{
  const { navigation } = this.props;
  const id = await AsyncStorage.getItem('id');
  const data = navigation.getParam('data');
  this.setState({
      ordenID: data.data.crearModificarOrden.id, 
      orden: data.data.crearModificarOrden.direccion,
      ordendata: data.data.crearModificarOrden
  })
  console.log(this.state.ordendata)
}

async submit(ev) {
   
  this.props.client
    .query({
        query: GET_FULL_ORDEN,
        variables: { id: this.state.ordenID }
    })
    .then(async (results)=> {
      const { loading, error, data } = results
      let cantidad = null;
      let descuento = null;
      let precioUnidad = null;
      let total = 0;
      let tipoDescuento = null;
      let descuentoFinal = 0;
      if (data) {
        cantidad = _get(data, 'getFullOrden.cantidad', null);
        tipoDescuento = _get(data, 'getFullOrden.cupon.tipo', null);
        descuento = _get(data, 'getFullOrden.cupon.descuento', null);
        precioUnidad = _get(data, 'getFullOrden.producto.number', null);
        total = cantidad * precioUnidad;
        switch (tipoDescuento) {
          case 'porcentaje':
            descuentoFinal = total * (descuento / 100);
            break;
          case 'dinero':
            descuentoFinal = descuento;
            break;
        }
        total = total - descuentoFinal;
        console.log('calculos stripe', {
          cantidad,
          tipoDescuento,
          precioUnidad,
          descuento,
          descuentoFinal,
          total
        });
      }
      console.log("total",total,this.state.token.tokenId)
      let response = await fetch(NETWORK_INTERFACE_LINK + "/stripe/chargeToken", {
        method: "POST",
        headers:{"Content-Type": 'application/json',},
        mode: 'cors',
        cache: 'default',
        body: JSON.stringify({stripeToken:this.state.token.tokenId,orderId:this.state.ordenID,amount:(total.toFixed(2)*100)})
      });  
      console.log(response)
      if(response.ok){
        setTimeout(() => {
          console.log('pago caturado')
        }, 1000);
      }
    })
    .catch(error => {
      alert(`error : ${error}`);
    })
}
  
handleResponse = data => {
  if (data.title === "success") {
    this.setState({ showModal: false, status: "Complete" });
} else if (data.title === "cancel") {
    this.setState({ showModal: false, status: "Cancelled" });
} 
  this.props.client
    .query({
        query: GET_FULL_ORDEN,
        variables: { id: this.state.ordenID }
    })
    .then(async (results)=> {
      const { loading, error, data } = results
      let cantidad = null;
      let descuento = null;
      let precioUnidad = null;
      let total = 0;
      let tipoDescuento = null;
      let descuentoFinal = 0;
      if (data) {
        cantidad = _get(data, 'getFullOrden.cantidad', null);
        tipoDescuento = _get(data, 'getFullOrden.cupon.tipo', null);
        descuento = _get(data, 'getFullOrden.cupon.descuento', null);
        precioUnidad = _get(data, 'getFullOrden.producto.number', null);
        total = cantidad * precioUnidad;
        switch (tipoDescuento) {
          case 'porcentaje':
            descuentoFinal = total * (descuento / 100);
            break;
          case 'dinero':
            descuentoFinal = descuento;
            break;
        }
        total = total - descuentoFinal;
        console.log('calculos paypal', {
          cantidad,
          tipoDescuento,
          precioUnidad,
          descuento,
          descuentoFinal,
          total
        });
        this.setState({
          precio: total
        })
        console.log('precio paypal', this.state.precio)
}})
};

  handleCardPayPress = async () => {
    try {
      this.setState({ loading: true, token: null })
      const token = await stripe.paymentRequestWithCardForm({
        // Only iOS support this options
        smsAutofillDisabled: true,
        requiredBillingAddressFields: 'full',
        prefilledInformation: {
          billingAddress: {
            name: this.state.orden.nombre,
            line1: this.state.orden.calle,
            line2: '',
            city: this.state.orden.ciudad,
            state: this.state.orden.provincia,
            country: 'ES',
            postalCode: this.state.orden.codigopostal,
            email: '',
          },
        },
      })
      this.submit()
      this.setState({ loading: false, token })
    } catch (error) {
      this.setState({ loading: false })
    }
  }

  render() {
    const { loading, token } = this.state
    const { navigation } = this.props;
    return (
      <ViewCuston light={colors.white} dark={colors.black} containers={
      <View style={styles.container}>
      <View style={{paddingHorizontal: dimensions.Width(4)}}>
      <Header navigation={navigation} />
      </View>
        {!token && this.state.status === "Pending"?
          <View>
            <Text style={styles.header}>
              Seleccionar metódo de pago    <AntdIcons name="infocirlce" style={{ paddingTop: 10, marginLeft: 'auto', alignSelf: 'center' }} size={18} color={colors.rgb_153} onPress={()=> this.setModalVisible(true)}/>
            </Text>
            <View style={{ marginTop: 22 }}>
              <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.modalVisible}>
                <ViewCuston light={colors.white} dark={colors.black} containers={
                  <View>
                <SafeAreaView style={styles.headers}>
                  <View style={{ flexDirection: 'row', marginTop: 20 }}>
                    <View style={{ alignItems: 'flex-start', marginLeft: 10 }}>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <TouchableOpacity onPress={() => { this.setModalVisible(!this.state.modalVisible) }}>
                        <Text style={{ marginRight: 20 }}>
                          <AntdIcons name="close" size={25} color={colors.green_main} />
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </SafeAreaView>
                <View style={{marginTop: dimensions.Height(0), paddingVertical: dimensions.Width(4) }}>
                <Text style={{ marginTop: dimensions.Height(1), fontSize: dimensions.FontSize(16), color: colors.slight_black, padding: 10, textAlign: 'center' }}>Ayuda</Text>
                <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{marginRight: 20, marginBottom: dimensions.Height(30)}}>
                  <View style={{flexDirection: 'row', padding: 20, margin: 10}}>
                    <AntdIcons name="creditcard" style={{alignSelf: 'center' }} size={30} color={colors.rgb_153}/>
                    <View style={{marginLeft: 20, alignSelf: 'center'}}>
                    <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(18),  marginBottom: 10}}>
                    Métodos de pago
                    </CustomText>
                    <Text style={{fontSize: dimensions.FontSize(14), color: colors.slight_black, fontWeight: '200'}}>
                    Aceptamos las principales tarjetas de crédito y débito, incluida Visa, Mastercard y American Express.
                    </Text>
                    </View>
                  </View>

                  <View style={{flexDirection: 'row', padding: 20, margin: 10}}>
                    <AntdIcons name="lock1" style={{alignSelf: 'center' }} size={30} color={colors.rgb_153}/>
                    <View style={{marginLeft: 20, alignSelf: 'center'}}>
                    <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(18),  marginBottom: 10}}>
                    ¿Qué pasa si el profesional no cumple con la orden?
                    </CustomText>
                    <Text style={{fontSize: dimensions.FontSize(14), color: colors.slight_black, fontWeight: '200'}}>
                    Retenemos tu dinero hasta 7 días para garantizar la sastifación del servicio.
                    </Text>
                    </View>
                  </View>

                  <View style={{flexDirection: 'row', padding: 20, margin: 10}}>
                    <AntdIcons name="Safety" style={{alignSelf: 'center' }} size={30} color={colors.rgb_153}/>
                    <View style={{marginLeft: 20, alignSelf: 'center'}}>
                    <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(18), marginBottom: 10}}>
                    Seguridad
                    </CustomText>
                    <Text style={{fontSize: dimensions.FontSize(14), color: colors.slight_black, fontWeight: '200'}}>
                    Utilizamos las última tecnología segura SSL.
                    </Text>
                    </View>
                  </View>

                  <View style={{flexDirection: 'row', padding: 20, margin: 10}}>
                    <AntdIcons name="customerservice" style={{alignSelf: 'center' }} size={30} color={colors.rgb_153}/>
                    <View style={{marginLeft: 20, alignSelf: 'center'}}>
                    <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(18),  marginBottom: 10}}>
                    Estamos aquí para ayudarte
                    </CustomText>
                    <Text style={{fontSize: dimensions.FontSize(14), color: colors.slight_black, fontWeight: '200'}}>
                    Si tienes mas dudas o preguntas puede contactarnos en cualquier momento info@locatefit.es
                    </Text>
                    </View>
                  </View>

                  <View style={{flexDirection: 'row', padding: 20, margin: 10}}>
                    <AntdIcons name="idcard" style={{alignSelf: 'center' }} size={30} color={colors.rgb_153}/>
                    <View style={{marginLeft: 20, alignSelf: 'center'}}>
                    <CustomText light={colors.black} dark={colors.white} style={{fontSize: dimensions.FontSize(18), marginBottom: 10}}>
                    ¿Cuándo llega mi profesional?
                    </CustomText>
                    <Text style={{fontSize: dimensions.FontSize(14), color: colors.slight_black, fontWeight: '200'}}>
                      No te preocupes el profesional llegara a la fecha y hora seleccionada, "Sujeto a aprobación del profesional."
                    </Text>
                    </View>
                  </View>
                </View>
                </ScrollView>
                </View>
                </View>}
                />
              </Modal>
            </View>
            <View style={{ marginTop: 20 }}>
              <TouchableOpacity onPress={this.handleCardPayPress} style={styles.tarjeta}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ borderWidth: 0.5, padding: 8, borderColor: colors.rgb_153, borderRadius: 8, backgroundColor: 'transparent' }}>
                  <PaymentVisa />
                  </View>
                  <Text style={{ alignSelf: 'center', marginLeft: 20, fontSize: dimensions.FontSize(16), fontWeight: '300', color: colors.rgb_102 }}>
                    Pagar con Tarjeta
              </Text>
                  <AntdIcons name="right" style={{ marginTop: 5, marginLeft: 'auto', alignSelf: 'center' }} size={18} color={colors.rgb_153} />
                </View>
              </TouchableOpacity>
              <Modal
                    animationType="slide"
                    visible={this.state.showModal}
                    onRequestClose={() => this.setState({ showModal: false })}
                >
                <SafeAreaView style={styles.headers}>
                  <View style={{ flexDirection: 'row', marginTop: 20 }}>
                    <View style={{ alignItems: 'flex-start', marginLeft: 10 }}>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <TouchableOpacity onPress={() => this.setState({ showModal: false })}>
                        <Text style={{ marginRight: 20 }}>
                          <AntdIcons name="close" size={25} color={colors.green_main} />
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </SafeAreaView>
                    <WebView
                        source={{ uri: NETWORK_INTERFACE_LINK + `/paypal?price=${this.state.precio}&order=${this.state.ordenID}`}} 
                        onNavigationStateChange={data =>
                            this.handleResponse(data)
                        } 
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        injectedJavaScript={`document.f1.submit()`}
                    />
                </Modal>
              <TouchableOpacity onPress={() => this.setState({ showModal: true })} style={styles.tarjeta}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ borderWidth: 0.5, padding: 8, borderColor: colors.rgb_153, borderRadius: 8, backgroundColor: 'transparent' }}>
                    <PaymentPaypal />
                  </View>
                  <Text style={{ alignSelf: 'center', marginLeft: 20, fontSize: dimensions.FontSize(16), fontWeight: '300', color: colors.rgb_102 }}>
                    Pagar con Paypal
              </Text>
                  <AntdIcons name="right" style={{ marginTop: 5, marginLeft: 'auto', alignSelf: 'center' }} size={18} color={colors.rgb_153} />
                </View>
              </TouchableOpacity>
            </View>
          </View> : null}
        <View
          style={styles.token}>
          {token ?
            <View style={{ alignSelf: 'center' }}>
              <AntdIcons name="checkcircle" style={{ marginTop: 5, alignSelf: 'center', marginBottom: 10 }} size={80} color={colors.green_main} />
              <CustomText dark={colors.white} light={colors.rgb_153} style={styles.instruction}>
                ¡Profesional contratado con éxito en unos minutos te estará tocando el telefonillo!
              </CustomText>
              <View style={styles.signupButtonContainer}>
                <Button dark={colors.white} light={colors.white} containerStyle={styles.buttonView}
                  onPress={() => this.props.navigation.navigate('Pedido')} title="Ir a mis pedidos"
                  titleStyle={styles.buttonTitle} />
              </View>
            </View> : null
          }
        </View>
        <View
        style={styles.token}>
        {this.state.status === "Complete" ?
          <View style={{ alignSelf: 'center' }}>
            <AntdIcons name="checkcircle" style={{ marginTop: 5, alignSelf: 'center', marginBottom: 10 }} size={80} color={colors.green_main} />
            <CustomText dark={colors.white} light={colors.rgb_153} style={styles.instruction}>
              ¡Profesional contratado con éxito en unos minutos te estará tocando el telefonillo!
            </CustomText>
            <View style={styles.signupButtonContainer}>
              <Button dark={colors.white} light={colors.white} containerStyle={styles.buttonView}
                onPress={() => this.props.navigation.navigate('Pedido')} title="Ir a mis pedidos"
                titleStyle={styles.buttonTitle} />
            </View>
          </View> : null
        }
      </View>
        <View
          style={styles.token}>
          {this.state.status === "Cancelled" ?
            <View style={{ alignSelf: 'center' }}>
              <AntdIcons name="closecircle" style={{ marginTop: 5, alignSelf: 'center', marginBottom: 10 }} size={80} color={colors.ERROR} />
              <CustomText dark={colors.white} light={colors.rgb_153} style={styles.instruction}>
                ¡Ubo un problema con tu pago verifica e intenta de nuevo!
              </CustomText>
              <View style={styles.signupButtonContainer}>
                <Button dark={colors.white} light={colors.white} containerStyle={styles.buttonView}
                  onPress={() => this.props.navigation.navigate('Pedido')} title="Ir a mis pedidos"
                  titleStyle={styles.buttonTitle} />
              </View>
            </View> : null
          }
        </View>
      </View>}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: dimensions.Height(100)
  },
  header: {
    fontSize: 16,
    textAlign: 'center',
    color: colors.rgb_153,
  },

  tarjeta: {
    padding: 20,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.rgb_235,
  },

  instruction: {
    textAlign: 'center',
    marginBottom: 5,
  },
  token: {
    padding: 20,
    textAlign: 'center',
  },
  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(17),
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(10)
  }
})


export default withApollo(CardFormScreen);
