// react-native.config.js
module.exports = {
    dependencies: {
      'react-native-nested-scroll-view': {
        platforms: {
          android: null, // disable Android platform, other platforms will still autolink
        },
      },
      'react-native-twitter-signin':{
        platforms: {
          android: null, // disable Android platform, other platforms will still autolink
        },
      }
    },
  };